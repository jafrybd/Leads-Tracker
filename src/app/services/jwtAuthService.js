import axios from "axios";
import localStorageService from "./localStorageService";
import history from '../../history'
import Swal from 'sweetalert2'
import myApi from '../auth/api'



class JwtAuthService {
  // You need to send http request with email and passsword to your server in this method
  // Your server will return user object & a Token
  // User should have role property
  // You can define roles in app/auth/authRoles.js
  loginWithEmailAndPassword = (email, password) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(this.user);
      }, 1000);
    }).then(data => {
      // Login successful
      // Save token
      this.setSession(data.token);
      // Set user
      this.setUser(data);
      return data;
    });
  };

  loginPhonePass = (phone, password) => {

    return new Promise((resolve, reject) => {

      axios.post(myApi + "/intraco/lpg/authentication/login", { phone, password }
      ).then(res => {

        if (res.data.error) {
          if (res.data.message) {
            if (!res.data.error) {
            Swal.fire({
              icon: "success",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          } else {
            Swal.fire({
              icon: "error",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          }
          }

          return reject('Error')
        }

        // Initalizes path variable to use be dynamically set later
        let path;
        // Switches on User type
        switch (res.data.data.user_type) {
          case 2:
            this.user = {
              "userId": res.data.data.profile.id,
              "role": "ADMIN",
              "displayName": res.data.data.profile.name,
              "phone": res.data.data.profile.phone_number,
              "email": res.data.data.profile.email,
              "photoURL": '/profile/' + res.data.data.profile.image,
              "age": 24,
              "token": res.data.data.token
            }

            path = "/dashboard/analytics";

            break;

          case 3:
            this.user = {
              "userId": res.data.data.profile.id,
              "role": 'SUB-ADMIN',
              "displayName": res.data.data.profile.name,
              "phone": res.data.data.profile.phone_number,
              "email": res.data.data.profile.email,
              "photoURL": '/profile/' + res.data.data.profile.image,
              "age": 24,
              "token": res.data.data.token
            }

            path = "/dashboard/subAdmin"

            break;

          case 4:
            this.user = {
              "userId": res.data.data.profile.id,
              "role": 'SALES-PERSON',
              "displayName": res.data.data.profile.name,
              "phone": res.data.data.profile.phone_number,
              "email": res.data.data.profile.email,
              "photoURL": '/profile/' + res.data.data.profile.image,
              "age": 24,
              "token": res.data.data.token
            }

            path = "/dashboard/sales"

            break;
          default:
            path = "/"
        }

        /*

        Resolve the promise with an object that can be destructured when called
        from login

        */


        // resolve({
        //   user: this.user,
        //   path
        // })

        setTimeout(() => {
          resolve({ user: this.user, path });
        }, 1000);
      })
    })

  }


  loginPin = (pin) => {

    return new Promise((resolve, reject) => {

      axios.post(myApi + "/mdSir/login", { pin }
      ).then(res => {
        //console.log(res)
        if (res.data.error) {
          if (res.data.message) {
            if (!res.data.error) {
            Swal.fire({
              icon: "success",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          } else {
            Swal.fire({
              icon: "error",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          }
          }

          return reject('Error')
        }

        // Initalizes path variable to use be dynamically set later
        let path;
        //console.log(res.data.data)
        // Switches on User type
        if (res.data.data) {

          this.user = {
            "userId": res.data.data.profile.id,
            // "department_id" : res.data.data.department_id,
            "role": 'Managing Director',
            // "role": res.data.data.user_type,
            "displayName": res.data.data.profile.name,
            "phone": res.data.data.profile.phone_number,
            "email": res.data.data.profile.email,
            "photoURL": '/profile/' + res.data.data.profile.image,
            "age": 24,
            "token": res.data.data.token
          }

          path = "/dashboard/md";

        }
     

        setTimeout(() => {
          resolve({ user: this.user, path });
        }, 1000);

      })
    })


  }


  // You need to send http requst with existing token to your server to check token is valid
  // This method is being used when user logged in & app is reloaded
  loginWithToken = () => {
    return new Promise((resolve, reject) => {

      // Checks for the data in local storage
      let token = localStorage.getItem('jwt_token');
      let user = localStorage.getItem('auth_user');

      if (!token || !user) {
        reject()
      }

      // Sets the token for future API calls
      /*
      ***************
      DOES NOT VERIFY
      ***************
      */
      this.setSession(token);

      let path;

      const userJson = JSON.parse(user);

      switch (userJson.role) {
        case "ADMIN":
          path = "/dashboard/analytics"
          break
        case "SUB-ADMIN":
          path = "/dashboard/subAdmin"
          break
        case "SALES-PERSON":
          path = "/dashboard/sales"
          break
        case "Managing Director":
          path = "/dashboard/md"
          break
        default:
          path = "/"
      }

      resolve({
        user,
        path
      })
    });
  };




  logout = () => {
    this.setSession(null);
    this.removeUser();
  }

  // Set token to all http request header, so you don't need to attach everytime
  setSession = token => {
    if (token) {
      localStorage.setItem("jwt_token", token);
      axios.defaults.headers.common["Authorization"] = "Bearer " + token;
    } else {
      localStorage.removeItem("jwt_token");
      delete axios.defaults.headers.common["Authorization"];
    }
  };

  // Save user to localstorage
  setUser = (user) => {
    localStorageService.setItem("auth_user", user);
  }
  // Remove user from localstorage
  removeUser = () => {
    localStorage.removeItem("auth_user");
  }
}

export default new JwtAuthService();
