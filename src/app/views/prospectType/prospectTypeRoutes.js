/* eslint-disable no-dupe-keys */
import React from "react";
import { authRoles } from "../../auth/authRoles";

const ProspectTypeRoutes = [
  {
    path: "/prospectType/create",
    component: React.lazy(() => import("./createProspectType")),
    auth: authRoles.admin,
    auth: authRoles.editor
  },
  {
    path: "/prospectType/manage",
    component: React.lazy(() => import("./manageProspectType")),
    auth: authRoles.admin,
    auth: authRoles.editor
  },
  {
    path: "/prospectType/edit",
    component: React.lazy(() => import("./sa")),
    auth: authRoles.admin,
    auth: authRoles.editor
  }
];

export default ProspectTypeRoutes;
