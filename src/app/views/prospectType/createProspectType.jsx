import React, { Component } from "react";
import { Breadcrumb } from "matx";
import { Card } from "@material-ui/core";
import CreateForm from "./CreateForm";

class CreateProspectType extends Component {
  render() {
    return (
      <div className="m-sm-30">
        <div  className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: "Prospect Type", path: "/prospectType/create" },
              { name: "Create Prospect Type" }
            ]}
          />
        </div>
        <Card className="px-6 pt-2 pb-4"><CreateForm /></Card>
      </div>
    );
  }
}

export default CreateProspectType;
