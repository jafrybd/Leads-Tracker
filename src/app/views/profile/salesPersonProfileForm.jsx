/* eslint-disable no-unused-vars */
import React, { Component } from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import localStorageService from "../../services/localStorageService"
import axios from 'axios'
import myApi from '../../auth/api'
import Swal from 'sweetalert2'

import {
  Button,
  Icon,
  Grid,
  Radio,
  RadioGroup,
  FormControlLabel,
  Checkbox
} from "@material-ui/core";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from "@material-ui/pickers";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import themeOptions from "app/MatxLayout/MatxTheme/themeOptions";

class SalesPersonProfileForm extends Component {


  state = {
    oldPass: "",
    name: "",
    email: "",
    date: new Date(),
    creditCard: "",
    phone: "",
    password: "",
    confirmPassword: "",
    gender: "",
    agreement: ""
  };

  componentDidMount() {
    // custom rule will have name 'isPasswordMatch'
    ValidatorForm.addValidationRule("isPasswordMatch", value => {
      if (value !== this.state.password) {
        return false;
      }
      return true;
    });
    let { user } = this.props
  }

  componentWillUnmount() {
    // remove rule when it is not needed
    ValidatorForm.removeValidationRule("isPasswordMatch");
  }

  handleSubmit = event => {
    //console.log(this.state.phone)
    let baseURL;
    axios.post(myApi + '/intraco/lpg/Admin/superAdmin/profile/update',
      { 'id': JSON.parse(localStorage.getItem("auth_user")).userId, 'name': this.state.name, 'email': this.state.email },
      {
        headers: {

          'x-access-token': localStorage.getItem("jwt_token")
        },
      }
    )
      .then(res => {
        if (!res.data.error) {
            Swal.fire({
              icon: "success",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          } else {
            Swal.fire({
              icon: "error",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          }
        this.setState({
          name: '',
          email: ''
        });
      })
  }

  handleChange = event => {
    event.persist();
    this.setState({ [event.target.name]: event.target.value });
  };

  handleDateChange = date => {
    // //console.log(date);

    this.setState({ date });
  };

  render() {
    let { user } = this.props
    let {
      oldPass,
      name,
      creditCard,
      phone,
      password,
      confirmPassword,
      gender,
      date,
      email
    } = this.state;
    return (
      <div>
        <ValidatorForm
          ref="form"
          onSubmit={this.handleSubmit}
          onError={errors => null}
        >
          <Grid container spacing={6}>
            <Grid item lg={6} md={6} sm={12} xs={12}>

              <TextValidator
                autoComplete='off'
                className="mb-4 w-full"
                label='Full Name'
                onChange={this.handleChange}
                type="text"
                name="name"
                value={name}
                validators={["required"]}
                errorMessages={["this field is required"]}
              />
              <TextValidator
                autoComplete='off'
                className="mb-4 w-full"
                label="Email"
                onChange={this.handleChange}
                type="email"
                name="email"
                value={email}
                // validators={["required", "isemail"]}
                errorMessages={["this field is required", "email is not valid"]}
              />

            </Grid>
          </Grid>
          <Button color="primary" variant="contained" type="submit">
            <Icon>send</Icon>
            <span className="pl-2 capitalize">Update</span>
          </Button>
        </ValidatorForm>
      </div>
    );
  }
}

export default SalesPersonProfileForm;
