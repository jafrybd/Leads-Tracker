import React, { Component, Fragment } from "react";
import { Grid, Card } from "@material-ui/core";

// import DoughnutChart from "../charts/echarts/Doughnut";

// import ModifiedAreaChart from "../dashboard/shared/ModifiedAreaChart";
// import StatCards from "../dashboard/shared/StatCards";
// import TableCard from "../dashboard/shared/TableCard";
// import RowCards from "../dashboard/shared/RowCards";
// import StatCards2 from "../dashboard/shared/StatCards2";
import UpgradeCard from "../dashboard/shared/UpgradeCard";
// import Campaigns from "../dashboard/shared/Campaigns";
import { withStyles } from "@material-ui/styles";
import localStorageService from "app/services/localStorageService";
import Axios from "axios";
import myApi from "app/auth/api";

class AdminProfileForm extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      phone: "",
      email: "",
      photo: "",
      question: "",
    };
  }
  componentDidMount() {
    //getting profile data

    Axios.get(
      myApi +
        "/intraco/lpg/Admin/superAdmin/profile/view/" +
        parseInt(localStorageService.getItem("auth_user").userId),
      {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      }
    )
      .then((response) => {
        //console.log(response)

        this.setState({
          name: response.data.data.name,
          phone: response.data.data.phone_number,
          email: response.data.data.email,
          photo: response.data.data.image,
          // question : response.data.data.
        });

        //console.log(this.state)
      })

      .catch((err) => console.log(err));
  }

  render() {
    // let { theme } = this.props;

    // let { user } = this.props

    return (
      <Fragment>
        <div
          className="analytics m-sm-30 "
          style={{ marginTop: "35px!important" }}
        >
          <Grid container spacing={3}>
            <Grid item lg={8} md={8} sm={12} xs={12}>
              <table
                style={{
                  marginTop: "90px",
                  width: "60%",
                  height: "60%",
                  textAlign: "justify",
                }}
              >
                <tbody>
                  <tr>
                    <td
                      style={{
                        color: "#1a71b5",
                        fontWeight: "500",
                        width: "30%",
                      }}
                    >
                      Full Name
                    </td>
                    <td
                      style={{
                        color: "#1a71b5",
                        fontWeight: "500",
                        width: "30%",
                      }}
                    >
                      :
                    </td>
                    <td>{this.state.name}</td>
                  </tr>
                  <tr>
                    <td
                      style={{
                        color: "#1a71b5",
                        fontWeight: "500",
                        width: "30%",
                      }}
                    >
                      Email
                    </td>
                    <td
                      style={{
                        color: "#1a71b5",
                        fontWeight: "500",
                        width: "30%",
                      }}
                    >
                      :
                    </td>
                    <td>{this.state.email}</td>
                  </tr>
                  <tr>
                    <td
                      style={{
                        color: "#1a71b5",
                        fontWeight: "500",
                        width: "30%",
                      }}
                    >
                      Phone
                    </td>
                    <td
                      style={{
                        color: "#1a71b5",
                        fontWeight: "500",
                        width: "30%",
                      }}
                    >
                      :
                    </td>
                    <td>{this.state.phone}</td>
                  </tr>
                  <tr>
                    <td
                      style={{
                        color: "#1a71b5",
                        fontWeight: "500",
                        width: "30%",
                      }}
                    >
                      Position
                    </td>
                    <td
                      style={{
                        color: "#1a71b5",
                        fontWeight: "500",
                        width: "30%",
                      }}
                    >
                      :
                    </td>
                    <td>
                      <strong>
                        {localStorageService.getItem("auth_user").role}
                      </strong>
                    </td>
                  </tr>
                </tbody>
              </table>
            </Grid>

            <Grid item lg={4} md={4} sm={12} xs={12}>
              <Card className="px-6 py-4 mb-6">
                {localStorageService.getItem("auth_user") && (
                  <img
                    src={localStorageService.getItem("auth_user").photoURL}
                    style={{ height: "100%" }}
                    alt="pic"
                  />
                )}
              </Card>

              <UpgradeCard />
            </Grid>
          </Grid>
        </div>
      </Fragment>
    );
  }
}

export default withStyles({}, { withTheme: true })(AdminProfileForm);
