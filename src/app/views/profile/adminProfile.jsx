import React, { Component } from "react";
import { Breadcrumb } from "matx";
import { Card } from "@material-ui/core";
import AdminProfileForm from "./adminProfileForm";

// const a = JSON.parse(localStorage.getItem('auth_user')).displayName

class AdminProfile extends Component {
 
  render() {
    return (
      <div className="m-sm-30">
        <div  className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: "Profile", path: "/profile/admin" },
              { name: JSON.parse(localStorage.getItem('auth_user')).displayName }
            ]}
          />
        </div>
        <Card className="px-6 pt-2 pb-4"><AdminProfileForm /></Card>
      </div>
    );
  }
}

export default AdminProfile;
