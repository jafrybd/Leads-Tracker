import React, { Component } from "react";
import { Breadcrumb } from "matx";
import { Card } from "@material-ui/core";
import SalesPersonProfileForm from "./salesPersonProfileForm";

class SalesPersonProfile extends Component {
  render() {
    return (
      <div className="m-sm-30">
        <div  className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: "Profile", path: "/profile/sales_person" },
              { name: "SALES PERSON" }
            ]}
          />
        </div>
        <Card className="px-6 pt-2 pb-4"><SalesPersonProfileForm /></Card>
      </div>
    );
  }
}

export default SalesPersonProfile;
