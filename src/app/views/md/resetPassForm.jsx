import React, { Component } from "react";
import { Breadcrumb } from "matx";
import { Card } from "@material-ui/core";
import ResetPass from "./resetPass";

class ResetPassForm extends Component {
  render() {
    return (
      <div className="m-sm-30">
        <div  className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: "Managing Director", path: "/MD/reset" },
              { name: "Reset Password" }
            ]}
          />
        </div>
        <Card className="px-6 pt-2 pb-4"><ResetPass /></Card>
      </div>
    );
  }
}

export default ResetPassForm;
