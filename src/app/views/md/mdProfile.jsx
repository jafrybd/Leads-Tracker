import React, { Component } from "react";
import { Breadcrumb } from "matx";
import { Card } from "@material-ui/core";
import MDProfileForm from "./mdProfileForm";


class MDProfile extends Component {
 
  render() {
    return (
      <div className="m-sm-30">
        <div  className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: "Profile", path: "/profile/MD/view" },
            ]}
          />
        </div>
        <Card className="px-6 pt-2 pb-4"><MDProfileForm /></Card>
      </div>
    );
  }
}

export default MDProfile;
