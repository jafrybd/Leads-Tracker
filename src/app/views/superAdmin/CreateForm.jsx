import React, { Component } from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import localStorageService from "./../../services/localStorageService"
import axios from 'axios'
import myApi from '../../auth/api'
import {
  Button,
  Icon,
  Grid,

} from "@material-ui/core";

import "date-fns";

import Swal from 'sweetalert2'
class CreateForm extends Component {

  constructor() {
    super();
    this.state = {
      name: "",
      firstName: "",
      email: "",
      date: new Date(),
      creditCard: "",
      phone: "",
      password: "",
      confirmPassword: "",
      gender: "",
      agreement: ""
    };
  }

  componentDidMount() {

    ValidatorForm.addValidationRule("isPasswordMatch", value => {
      if (value !== this.state.password) {
        return false;
      }

      return true;
    });


  }

  componentWillUnmount() {
    // remove rule when it is not needed
    ValidatorForm.removeValidationRule("isPasswordMatch");
  }

  handleSubmit = event => {

    axios.post(myApi + '/intraco/lpg/admin/superAdmin/registration', {
      'name': this.state.name,
      'phone': this.state.phone,
      'email': this.state.email,
      'password': this.state.password,
    }, {

      "headers": {
        "x-access-token": localStorageService.getItem("auth_user").token
      }
    })
      .then(res => {

        if (res.data.message) {
          // if (res.data.error === true) {
        //   if (!res.data.error) {
        //     Swal.fire({
        //       icon: "success",
        //       title: res.data.message,
        //       showConfirmButton: false,
        //     });
        //   } else {
        //     Swal.fire({
        //       icon: "error",
        //       title: res.data.message,
        //       showConfirmButton: false,
        //     });
        //   }
        // }
        if (res.data.error === true) {
          Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: res.data.message,
            showConfirmButton: false,
            timer: 1500
          })
        }
        if (res.data.error !== true) {
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: res.data.message,
            showConfirmButton: false,
            timer: 1500
          })
        }
        if (res.data.error === false) {
          this.setState({
            name: "",
            firstName: "",
            email: "",
            date: new Date(),
            creditCard: "",
            phone: "",
            password: "",
            confirmPassword: "",
            gender: "",
            agreement: ""
          })
        }
      }

    })

  }

  handleChange = event => {
    event.persist();
    this.setState({ [event.target.name]: event.target.value });
  };

  handleDateChange = date => {
    this.setState({ date });
  };

  render() {
    let {
      name,

      phone,
      password,
      confirmPassword,

      email
    } = this.state;
    return (
      <div>
        <ValidatorForm
          ref="form"
          onSubmit={this.handleSubmit}
          onError={errors => null}
        >
          <Grid container spacing={6}>
            <Grid item lg={6} md={6} sm={12} xs={12}>


              <TextValidator
                autoComplete='off'
                className="mb-4 w-full"
                label="First Name"
                onChange={this.handleChange}
                type="text"
                name="name"
                value={name}
                validators={["required"]}
                errorMessages={["this field is required"]}
              />
              <TextValidator
                autoComplete='off'
                className="mb-4 w-full"
                label="Email"
                onChange={this.handleChange}
                type="email"
                name="email"
                value={email}
                errorMessages={["this field is required", "email is not valid"]}
              />


              <TextValidator
                autoComplete='off'
                className="mb-4 w-full"
                label="Phone Number"
                onChange={this.handleChange}
                type="text"
                name="phone"
                value={phone}
                validators={['matchRegexp:[0-9]$', "required"]}
                errorMessages={["this field is required"]}
              />

            </Grid>

            <Grid item lg={6} md={6} sm={12} xs={12}>

              <TextValidator
                autoComplete='off'
                className="mb-4 w-full"
                label="Password (Min Length : 6)"
                onChange={this.handleChange}
                name="password"
                type="password"
                value={password}

                validators={["required", "isPasswordMatch"]}
                errorMessages={["this field is required"]}
              />
              <TextValidator
                autoComplete='off'
                className="mb-4 w-full"
                label="Confirm Password"
                onChange={this.handleChange}
                name="confirmPassword"
                type="password"
                value={confirmPassword}

                validators={["required", "isPasswordMatch"]}
                errorMessages={[
                  "this field is required",
                  "password didn't match"
                ]}
              />

            </Grid>
          </Grid>
          <Button color="primary" variant="contained" type="submit">
            <Icon>send</Icon>
            <span className="pl-2 capitalize">Submit</span>
          </Button>
        </ValidatorForm>
      </div>
    );
  }
}

export default CreateForm;
