import React from "react";
import { Modal, Button } from 'react-bootstrap';
import myApi from '../../auth/api'

import {
  Card,
  Icon,
  IconButton,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody
} from "@material-ui/core";
import Swal from 'sweetalert2'
import axios from 'axios'
const ManageForm = () => {
  let [superAdminList, setSuperAdminList] = React.useState('');
 
  const fetchData = React.useCallback(() => {
    axios({
      "method": "GET",
      "url": myApi+"/intraco/lpg/admin/superAdmin/list",
      "headers": {

        'x-access-token': localStorage.getItem("jwt_token")
      }
    })
      .then((response) => {
  
        setSuperAdminList(response.data.data)
      })
      .catch((error) => {
        //console.log(error)
      })
  }, [])
  React.useEffect(() => {
    fetchData()
  }, [fetchData])


  const handleShow = (user) => {

    Swal.update(
      '<input type="text" value='+user.id+'>'+'<input type="text" value='+user.name+'>'
    )


  }
  return (

    <Card elevation={3} className="pt-5 mb-6">
      <div className="card-title px-6 mb-3">Super Admin List</div>
      <div className="overflow-auto">


        <Table className="product-table">
          <TableHead>
            <TableRow>
              <TableCell className="px-6" colSpan={4}>
                Name
              </TableCell>
              <TableCell className="px-0" colSpan={2}>
                Phone
              </TableCell>
              <TableCell className="px-0" colSpan={2}>
                Email
              </TableCell>
              <TableCell className="px-0" colSpan={1}>
                Action
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {superAdminList.length == 0
              ? 'Loading users...'
              : superAdminList.map(user => (
                <TableRow key={user.id}>
                  <TableCell className="px-0 capitalize" colSpan={4} align="left">
                    <div className="flex items-center">
                      <img
                        className="circular-image-small"
                        src={'/profile/' + user.image}
                        alt="user"
                      />
                      <p className="m-0 ml-8">{user.name}</p>
                    </div>
                  </TableCell>
                  <TableCell className="px-0 capitalize" align="left" colSpan={2}>


                    {user.phone_number}

                  </TableCell>

                  <TableCell className="px-0 capitalize" align="left" colSpan={2}>

                    {user.email}
                  </TableCell>
                  <TableCell className="px-0" colSpan={1}>
                    <IconButton onClick={() => handleShow(user)}>
                      <Icon color="primary">edit</Icon>
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>

      </div>
    </Card>
  );
};


export default ManageForm;
