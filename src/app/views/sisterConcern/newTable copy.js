import React, { useState } from "react";
import { Button } from "reactstrap";
import ModalForm from "./Modal";
import axios from "axios";
import myApi from "../../auth/api";
import {
  // Table,
  // TableHead,
  // TableBody,
  // TableRow,
  // td,
  TablePagination,
} from "@material-ui/core";
import Swal from "sweetalert2";
import ModalTable from "./ModalTable";
import ModalHistory from "./ModalHistory";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
// import { StickyTable } from "app/views/sticky";
import { Table } from 'react-bootstrap'
export default function DeactiveDataTable(props) {

  const [form, setValues] = useState({
    name: "",
    type: "",
    company: "",
    active_status: "",
  });
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
  };

  const onChange = (e) => {
    setValues({
      ...form,
      [e.target.name]: e.target.value,
    });
  };
  const deleteItem = (id, sister_concern, company, active_status) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You will be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, deactive it!",
      allowOutsideClick: false,
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .post(
            myApi + "/intraco/lpg/company_sister_concerns/deactive",
            { id: id },
            {
              headers: {
                "x-access-token": localStorage.getItem("jwt_token"),
              },
            }
          )
          .then((res) => {
            Swal.fire({
              icon: "success",
              sister_concern: "Deactivated",
              text: "Dectivated Successfully!",
            });
          })
          .then((item) => {
            props.updateState({
              id,
              sister_concern,
              company,
              active_status: 1,
            });
          })
          .catch((err) => console.log(err));
      }
    });
  };

  const activeItem = (id, sister_concern, company, active_status) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You will be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#1a71b5",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, active it!",
      allowOutsideClick: false,
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .post(
            myApi + "/intraco/lpg/company_sister_concerns/active",
            { id: id },
            {
              headers: {
                "x-access-token": localStorage.getItem("jwt_token"),
              },
            }
          )
          .then((res) => {
            Swal.fire({
              icon: "success",
              sister_concern: "Activated",
              text: "Activated Successfully!",
            });
          })
          .then((item) => {
            props.updateState({
              id,
              sister_concern,
              company,
              active_status: 0,
            });
          });
      }
    });
  };
  const useStyles = makeStyles({
    root: {
      width: "100%",
      overflowX: "auto",
      // tableLayout: 'initial'
    },
    table: {
      minWidth: '650px',
      // tableLayout: 'initial'
    },
  });
  const classes = useStyles();


  return (
    <>
      {/* <TableContainer
    //   component={Paper} style={{ boxShadow: '0px 2px 1px -1px rgba(255, 255, 255, 0.06),0px 1px 1px 0px rgba(255, 255, 255, 0.04),0px 1px 3px 0px rgba(255, 255, 255, 0.03)', backgroundColor: '#fafafa' }}>
      // <Table className={classes.table} aria-label="simple table" style={{ overflowX : 'auto', overflowY: 'scroll', width: '400' }}> */}
      <Table responsive>
        <thead>
          <tr>
            <th>
              SL.
            </th>
            <th >
              Company
            </th>
            <th >
              Sister Concern
            </th>
            <th >
              Status
            </th>
            <th >
              Details
            </th>
            <th  >
              History
            </th>
            <th  >
              Edit
            </th>
            <th >
              Action
            </th>
          </tr>
        </thead>
        <tbody>
          {props.items &&
            props.items
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((item, id) => (
                <tr key={id}>
                  <td>
                    {page * rowsPerPage + id + 1}
                  </td>
                  <td >
                    {item.company}
                  </td>
                  <td>
                    {item.sister_concern}
                  </td>
                  <td>
                    {item.active_status === 0 ? (
                      <font color="green">Activate</font>
                    ) : (
                      <font color="red">Deactivated</font>
                    )}
                  </td>

                  <td >
                    <ModalTable
                      style={{ maxWidth: "700px!important" }}
                      buttonLabel="View"
                      item={item}
                    />
                  </td>
                  <td>
                    <ModalHistory
                      style={{ maxWidth: "700px!important" }}
                      buttonLabel="View"
                      item={item}
                    />
                  </td>
                  {item.active_status === 0 ? (
                    <td>
                      <ModalForm
                        buttonLabel="Edit"
                        item={item}
                        updateState={props.updateState}
                      />
                    </td>
                  ) : (
                    <td>
                      <Button
                        color="btn btn-warning"
                        style={{ marginTop: "7px", margin: "auto", marginRight: '-7px' }}
                        disabled
                      >
                        Edit
                      </Button>
                    </td>
                  )}

                  {item.active_status === 0 ? (
                    <td>
                      <Button
                        color="danger"
                        style={{
                          backgroundColor: "#f41b35",
                          borderColor: "#f41b35",
                          // width: "100px",
                        }}
                        onClick={() =>
                          deleteItem(
                            item.id,
                            item.sister_concern,
                            item.company,
                            item.active_status
                          )
                        }
                      >
                        Deactive
                      </Button>
                    </td>
                  ) : (
                    <td>
                      <Button
                        name="active_status"
                        color="success"
                        style={{
                          backgroundColor: "#00aa33",
                          borderColor: "#00aa33",
                          // width: "100px",
                        }}
                        onClick={() =>
                          activeItem(
                            item.id,
                            item.sister_concern,
                            item.company,
                            item.active_status
                          )
                        }
                      >
                        Active
                      </Button>
                    </td>
                  )}
                </tr>
              ))}
        </tbody>
      </Table>

      <TablePagination
        style={{
          maxWidth: 400,
          overflowX: "hidden",
          padding: "0px!important",
          display: "contents",
        }}
        className="px-4"
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={props.items.length}
        rowsPerPage={rowsPerPage}
        page={page}
        backIconButtonProps={{
          "aria-label": "Previous Page",
        }}
        nextIconButtonProps={{
          "aria-label": "Next Page",
        }}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </>
    // </TableContainer>
    //  <Table striped bordered hover>
    //   <thead>
    //     <tr>
    //       <th>#</th>
    //       <th>First Name</th>
    //       <th>Last Name</th>
    //       <th>Username</th>
    //     </tr>
    //   </thead>
    //   <tbody>
    //     <tr>
    //       <td>1</td>
    //       <td>Mark</td>
    //       <td>Otto</td>
    //       <td>@mdo</td>
    //     </tr>
    //     <tr>
    //       <td>2</td>
    //       <td>Jacob</td>
    //       <td>Thornton</td>
    //       <td>@fat</td>
    //     </tr>
    //     <tr>
    //       <td>3</td>
    //       <td colSpan="2">Larry the Bird</td>
    //       <td>@twitter</td>
    //     </tr>
    //   </tbody>
    // </Table>
  );
};

