/* eslint-disable no-dupe-keys */
import React from "react";
import { authRoles } from "../../auth/authRoles";

const SisterConcernRoutes = [
  {
    path: "/sisterconcern/create",
    component: React.lazy(() => import("./createSisterConcern")),
    auth: authRoles.admin,
    auth: authRoles.editor,
  },

  {
    path: "/sisterconcern/edit",
    component: React.lazy(() => import("./sa")),
    auth: authRoles.admin,
    auth: authRoles.editor,
  },
];

export default SisterConcernRoutes;
