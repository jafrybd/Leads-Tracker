/* eslint-disable jsx-a11y/heading-has-content */
import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "reactstrap";
import DeactiveDataTable from "./newTable";
import axios from "axios";
import { Breadcrumb } from "matx";
import myApi from "../../auth/api";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import { Button, Icon, Grid } from "@material-ui/core";
import Select from "react-select";
import { ValidatorForm } from "react-material-ui-form-validator";
import localStorageService from "app/services/localStorageService";
import Swal from "sweetalert2";
import jsPDF from "jspdf";
import "jspdf-autotable";
import Moment from 'moment'

function Sa(props) {
  const [name, setNames] = useState("");
  const [selectedCompany, setSelectedCompany] = useState("");
  const [selectedSisterConcern, setSelectedSisterConcern] = useState("");
  const [selectedSisterConcernByCompany, setSelectedSisterConcernByCompany] = useState("");
  const [company, setCompany] = useState([]);
  const [sisterConcernList, setSisterConcernList] = useState([]);
  const [sisterConcernListByCompany, setSisterConcernListByCompany] = useState([]);
  const [items, setItems] = useState([]);
  const [form, setValues] = useState({
    clients_corporate: [],

    selectedClientIndividual: '',
    selectedClientCorporate: '',
    selectedClientType: '',
    client_type_array: [],
    client_type_arrayy: [],
    selectedVehicleIndividualNew: [],
    selectedDiscussionCorporateNew: [],
    prospectTypes: [],
    selectedProspectType: '',
    clients: [],
    selectedClient: "",
    sales: [],
    selectedSales: '',
    discussionTypes: [],
    selectedDiscussionType: "",
    CallTimeTypes: [],
    selectedCallTimeType: "",
    VehicleTypes: [],
    selectedVehicleType: "",
    Leads: [],
    selectedLead: "",

    columns: ['discussion_type', 'vehicle_type', 'sales_person', 'call_at', 'leads', 'call_date'],
    options: {
      filter: true,
      filterType: "dropdown",

    },
    tableData: [],
    startDate: null,
    endDate: null,
    endDateFormatted: null,
    startDateFormatted: null,
    isSubmitted: false
  });


  const getItems = () => {
    axios({
      method: "GET",
      url: myApi + "/intraco/lpg/company_sister_concerns/allList",
      headers: {
        "x-access-token": localStorage.getItem("jwt_token"),
      },
    })
      .then((response) => {
        console.log(response.data.data);
        setItems(Array.isArray(response.data.data) ? response.data.data : []);
      })
      .catch((error) => {
        console.log(error);
      });

    // getting clientInformation
    axios
      .get(myApi + "/intraco/lpg/company/allList", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        console.log(res);
        let companies = res.data.data.map((c) => {
          return {
            value: c.id,
            label: c.title,
          };
        });

        setCompany(
          [
            {
              value: 0,
              label: "Select All Company",
            },
          ].concat(companies)
        );
      })
      .catch((error) => {
        console.log(error);
      });

    // getting clientInformation
    axios
      .get(myApi + "/intraco/lpg/company_sister_concerns/allList", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        console.log(res);
        let companies = res.data.data.map((c) => {
          return {
            value: c.id,
            label: c.sister_concern,
          };
        });

        setSisterConcernList(
          [
            {
              value: 0,
              label: "Select All Sister Concern",
            },
          ].concat(companies)
        );
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const addItemToState = (item) => {
    setItems([...items, item]);
    setNames([...name, name]);
  };

  const updateState = (item) => {
    const itemIndex = items.findIndex((data) => data.id === item.id);
    const newArray = [
      ...items.slice(0, itemIndex),
      item,
      ...items.slice(itemIndex + 1),
    ];
    setItems(newArray);
  };

  const deleteItemFromState = (id) => {
    const updatedItems = items.filter((item) => item.id !== id);
    setItems(updatedItems);
  };

  useEffect(() => {
    getItems();
  }, []);

  const onSelectChangeCompany = (label) => {
    if (label !== null) {
      setSelectedCompany(label.label);
    }
    if (label === null) {
      setSelectedCompany("");
    }
    label !== null ?
      (  // getting sisterConcern

        axios
          .get(myApi + "/intraco/lpg/company_sister_concerns/" + label.value + "/allSisterConcernListForGlobalSearch", {
            headers: {
              "x-access-token": localStorageService.getItem("auth_user").token,
            },
          })
          .then((res) => {
            console.log(res.data.data);
            let companies = res.data.data.map((c) => {
              return {
                value: c.id,
                label: c.title,
              };
            });

            setSisterConcernListByCompany(
              [
                {
                  value: 0,
                  label: "Select All Sister Concern",
                },
              ].concat(companies)
            );
          })
          .catch((error) => {
            console.log(error);
          })
      )
      :
      // getting clientInformation
      axios
        .get(myApi + "/intraco/lpg/company_sister_concerns/allList", {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token,
          },
        })
        .then((res) => {
          console.log(res);
          let companies = res.data.data.map((c) => {
            return {
              value: c.id,
              label: c.sister_concern,
            };
          });

          setSisterConcernListByCompany(
            [
              {
                value: 0,
                label: "Select All Sister Concern",
              },
            ].concat(companies)
          );
        })
        .catch((error) => {
          console.log(error);
        });

  };

  const onSelectChangeSisterConcern = (label) => {
    if (label !== null) {
      setSelectedSisterConcern(label.label);
    }
    if (label === null) {
      setSelectedSisterConcern("");
    }


  };
  const onSelectChangeSisterConcernByCompany = (label) => {
    if (label !== null) {
      setSelectedSisterConcern(label.label);
    }
    if (label === null) {
      setSelectedSisterConcern("");
    }

  };

  const handleSubmit = () => {
    console.log(selectedSisterConcern, selectedCompany);
    if (
      (selectedCompany === "Select All Company" || selectedCompany === '') &&
      (selectedSisterConcern === "Select All Sister Concern" || selectedSisterConcern === '')
    ) {
      axios
        .get(
          myApi + "/intraco/lpg/company_sister_concerns/allList",

          {
            headers: {
              "x-access-token": localStorageService.getItem("auth_user").token, // override instance defaults
            },
          }
        )
        .then((res) => {
          if (res.data.data) {
            setItems(res.data.data);
          }
          if (res.data.count === 0) {
            Swal.fire({
              icon: "error",
              title: "No Data Found",
              showConfirmButton: false,
              timer: 1000,
            });
          }
        });
    } else if (
      selectedCompany === "Select All Company" &&
      selectedSisterConcern !== "Select All Sister Concern"
    ) {
      axios
        .post(
          myApi + "/intraco/lpg/company_sister_concerns/search",
          {
            sister_concern_title: selectedSisterConcern,
            company_title: "",
          },
          {
            headers: {
              "x-access-token": localStorageService.getItem("auth_user").token, // override instance defaults
            },
          }
        )
        .then((res) => {
          if (res.data.data) {
            setItems(res.data.data);
          }
          if (res.data.count === 0) {
            Swal.fire({
              icon: "error",
              title: "No Data Found",
              showConfirmButton: false,
              timer: 1000,
            });
          }
        });
    } else if (
      selectedCompany !== "Select All Company" &&
      selectedSisterConcern === "Select All Sister Concern"
    ) {
      axios
        .post(
          myApi + "/intraco/lpg/company_sister_concerns/search",
          {
            sister_concern_title: "",
            company_title: selectedCompany,
          },
          {
            headers: {
              "x-access-token": localStorageService.getItem("auth_user").token, // override instance defaults
            },
          }
        )
        .then((res) => {
          if (res.data.data) {
            setItems(res.data.data);
          }
          if (res.data.count === 0) {
            Swal.fire({
              icon: "error",
              title: "No Data Found",
              showConfirmButton: false,
              timer: 1000,
            });
          }
        });
    } else {
      axios
        .post(
          myApi + "/intraco/lpg/company_sister_concerns/search",
          {
            sister_concern_title: selectedSisterConcern,
            company_title: selectedCompany,
          },
          {
            headers: {
              "x-access-token": localStorageService.getItem("auth_user").token, // override instance defaults
            },
          }
        )
        .then((res) => {
          if (res.data.data) {
            setItems(res.data.data);
          }
          if (res.data.count === 0) {
            Swal.fire({
              icon: "error",
              title: "No Data Found",
              showConfirmButton: false,
              timer: 1000,
            });
          }
        });
    }
  };
  const exportPDF = () => {
    const locateValueById = (types, id) => {
      let item = types.find((it) => it.id === Number(id));
      return item;
    };
    const addFooters = doc => {


      const pageCount = doc.internal.getNumberOfPages()
      var footer = new Image();
      footer.src = '/assets/footerPdf.png';

      doc.setFont('helvetica', 'italic')
      doc.setFontSize(8)
      for (var i = 1; i <= pageCount; i++) {
        doc.setPage(i)

        doc.addImage(image, 'JPEG', pageWidth - 110, 0, 100, 100)
        doc.addImage(leftBar, 'JPEG', 0, 0, 16, 270)
        doc.addImage(footer, 'JPEG', 0, pageHeight - 60, pageWidth, 60)
      }
    }

    const unit = "pt";
    const size = "A4"; // Use A1, A2, A3 or A4
    const orientation = "portrait"; // portrait or landscape

    const marginLeft = 40;
    const doc = new jsPDF(orientation, unit, size);
    var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
    var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
    doc.setFontSize(15);

    var leftBar = new Image();
    leftBar.src = '/assets/leftBar.png';
    var image = new Image();
    image.src = '/assets/logoNew.png';
    const title = "Appointment Report";
    const headers = [["SL", "Company", "Sister Concern", "Sales Person"]];

    const data = items.map((ap, i) => [i + 1, ap.company, ap.sister_concern, ap.salesperson_info.name]);

    let content = {
      startY: 150,
      head: headers,
      body: data,
      margin: {
        bottom: 120,
        top: 150
      }
    };


    doc.setFont('helvetica', 'bold')
    doc.setFontSize(15)
    doc.text(title, marginLeft, 50);
    doc.setFontSize(10)
    const filterData = form.startDateFormatted && form.endDateFormatted && 'Date : ' + form.startDateFormatted + " > " + form.endDateFormatted
    form.startDateFormatted && form.endDateFormatted && doc.text(filterData, marginLeft, 70)


    doc.autoTable(content);
  
    doc.setTextColor(100);
    doc.setFontSize(10);

    addFooters(doc)

    doc.save("appointment_report(" + Moment(new Date()).format('YYYY-MM-DD') + ").pdf")
  }
  return (
    <div className="m-sm-30">
      <div className="mb-sm-30">
        <Container className="App">
          <Row>
            <Col>
              <Breadcrumb
                routeSegments={[
                  { name: "Sister Concern", path: "/sisterconcern/edit" },
                  { name: "Manage" },
                ]}
              />
              <h1 style={{ margin: "20px 0" }}></h1>
            </Col>
          </Row>

          <br></br>
          <Row>
            <Col>
              <div>
                <ValidatorForm onSubmit={handleSubmit}>
                  <Grid container spacing={6}>
                    <Grid
                      item
                      lg={6}
                      md={6}
                      sm={12}
                      xs={12}

                    >
                      <div style={{ margin: "10px", marginLeft: "0px" }}>
                        <font color="grey">
                          <b>Select Company</b>
                        </font>
                      </div>

                      {
                        <Select
                          style={{
                            width: "100%",
                            padding: "5px",
                            backgroundColor: "white",
                            color: "#444",
                            borderBottomColor: "#000000",
                            textAlign: "left",
                            marginTop: "40px",
                          }}
                          isClearable="true"
                          name="selectedCompany"
                          options={company}
                          className="basic-multi-select"
                          classNamePrefix="select"
                          onChange={onSelectChangeCompany}
                        />
                      }

                    </Grid>
                    <Grid
                      item
                      lg={6}
                      md={6}
                      sm={12}
                      xs={12}

                    >
                      <div style={{ margin: "10px", marginLeft: "0px" }}>
                        <font color="grey">
                          <b>Select Sister Concern</b>
                        </font>
                      </div>
                      {selectedCompany === '' ?
                        <Select
                          style={{
                            width: "100%",
                            padding: "5px",
                            backgroundColor: "white",
                            color: "#444",
                            borderBottomColor: "#000000",
                            textAlign: "left",
                            marginTop: "40px",
                          }}
                          isClearable="true"
                          name="selectedSisterConcern"
                          options={sisterConcernList}
                          className="basic-multi-select"
                          classNamePrefix="select"
                          onChange={onSelectChangeSisterConcern}
                        />
                        :
                        <Select
                          style={{
                            width: "100%",
                            padding: "5px",
                            backgroundColor: "white",
                            color: "#444",
                            borderBottomColor: "#000000",
                            textAlign: "left",
                            marginTop: "40px",
                          }}
                          isClearable="true"
                          name="selectedSisterConcern"
                          options={sisterConcernListByCompany}
                          className="basic-multi-select"
                          classNamePrefix="select"
                          onChange={onSelectChangeSisterConcernByCompany}
                        />
                      }
                    </Grid>
                  </Grid>
                  <Button
                    onSubmit={handleSubmit}
                    color="primary"
                    variant="contained"
                    type="submit"
                    style={{
                      height: "30px",
                      marginTop: "20px",
                      marginBottom: "20px",
                    }}
                  >
                    <Icon>send</Icon>
                    <span className="pl-2 capitalize">Search</span>
                  </Button>
                </ValidatorForm>
              </div>
            </Col>
          </Row>
          <Row>

            {items.length !== 0 &&
              <button className="btn btn-primary" style={{ marginLeft: '85%', marginTop: '-50px' }} onClick={() => exportPDF()}>Generate Report</button>
            }
          </Row>
          <Row>
            <Col>
              <DeactiveDataTable
                items={items}
                updateState={updateState}
                deleteItemFromState={deleteItemFromState}
              />
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
}

export default Sa;
