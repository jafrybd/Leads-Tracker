/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import axios from "axios";
import myApi from "../../auth/api";
import Swal from "sweetalert2";
import localStorageService from "app/services/localStorageService";
import Select from "react-select";

function AddEditForm(props) {
  const [item, setItem] = useState([]);
  const [selectedSalesPerson, setSelectedSalesPerson] = useState("");
  const [salespersonList, setSalespersonList] = useState([]);
  const [form, setValues] = useState({
    id: "",
    sister_concern: "",
    active_status: "",
    remove_reason: "",
    company: "",
    salesPerson_info: {
      id: "",
      name: "",
    },
    isSubmitted: "",
  });
  const onChange = (e) => {
    setValues({
      ...form,
      [e.target.name]: e.target.value,
    });
  };
  const onSalespersOnChange = (e) => {
    setValues({
      ...form,
      salesperson_info: {
        id: Number(e.target.value),
        name: e.target.name,
      },
    });
  };
  const submitFormAdd = (e) => {
    e.preventDefault();
  };

  const submitFormEdit = (e) => {
    e.preventDefault();
    let reqData = {
      id: form.id === 0 ? "" : form.id,
      title: form.sister_concern,
      sales_person_id: form.salesperson_info.id,
      remove_reason: form.remove_reason,
    };

    if (reqData.sales_person_id === "") {
      delete reqData.sales_person_id;
    }
    if (reqData.remove_reason === "") {
      delete reqData.remove_reason;
    }
    axios
      .post(
        myApi + "/intraco/lpg/company_sister_concerns/editSisterConcern",
        reqData,
        {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token,
          },
        }
      )
      .then((response) => {
        if (response.data.error === false) {
          Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
          props.updateState(form);
        } else {
          Swal.fire({
            icon: "error",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        }
        props.toggle();
      })
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    form.salesperson_info && form.salesperson_info.id
      ? setValues({
          isSubmitted: true,
        })
      : setValues({
          isSubmitted: false,
        });

    // getting salesperson
    axios
      .get(myApi + "/intraco/lpg/salesPerson/list", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        let salerFromApi = res.data.data.map((client) => {
          return {
            value: client.id,
            label: client.name,
          };
        });

        setSalespersonList(
          [
            {
              value: "",
              label: "Select if you want to change",
            },
          ].concat(salerFromApi)
        );
        console.log(salespersonList);
      })
      .catch((error) => {
        console.log(error);
      });

    if (props.item.id) {
      setValues({
        id: props.item.id,
        sister_concern: props.item.sister_concern,
        salesperson_info: {
          id: props.item.salesperson_info && props.item.salesperson_info.id,
          name: props.item.salesperson_info && props.item.salesperson_info.name,
        },
        company: props.item.company,
        active_status: props.item.active_status,
      });
    }
    console.log(props.item);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, false);

  const onSelectSalespersonChange = (value) => {
    if (value !== null) {
      setSelectedSalesPerson(value.value);
      setValues({
        salesperson_id: value.value,
      });
    }
    if (value === null) {
      setSelectedSalesPerson("");
    }
  };

  return (
    <Form onSubmit={props.item ? submitFormEdit : submitFormAdd}>
      <FormGroup>
        <Label for="name">Sister Concern</Label>
        <Input
          title="text"
          name="sister_concern"
          id="sister_concern"
          onChange={onChange}
          value={form.sister_concern === null ? "" : form.sister_concern}
          minLength="3"
          required
        />
      </FormGroup>

      <FormGroup>
        <Label for="name">Sales Person</Label>
        <select
          style={{
            width: "100%",
            padding: "5px",
            backgroundColor: "white",
            color: "#444",
            borderBottomColor: "#949494",
            textAlign: "left",
          }}
          className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
          name="salesperson_id"
          value={form.salesperson_info && form.salesperson_info.id}
          onChange={onSalespersOnChange}
        >
          {salespersonList.map((client) => (
            <option key={client.value} value={client.value}>
              {client.label}
            </option>
          ))}
        </select>
      </FormGroup>

      {props.item.salesperson_info && props.item.salesperson_info.id ? (
        <FormGroup>
          <Label for="name">Remove Reason</Label>
          <Input
            title="text"
            name="remove_reason"
            id="remove_reason"
            onChange={onChange}
            value={form.remove_reason === null ? "" : form.remove_reason}
            minLength="3"
            required
          />
        </FormGroup>
      ) : (
        ""
      )}

      <Button>Submit</Button>
    </Form>
  );
}

export default AddEditForm;
