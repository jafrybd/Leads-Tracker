/* eslint-disable no-dupe-keys */
import React from "react";
import { authRoles } from "../../auth/authRoles";

const ClientTypeRoutes = [
  {
    path: "/clientType/create",
    component: React.lazy(() => import("./createClientType")),
    auth: authRoles.admin,
    auth: authRoles.editor
  },
  {
    path: "/clientType/manage",
    component: React.lazy(() => import("./manageClientType")),
    auth: authRoles.admin,
    auth: authRoles.editor
  },
  {
    path: "/clientType/edit",
    component: React.lazy(() => import("./sa")),
    auth: authRoles.admin,
    auth: authRoles.editor
  }
];

export default ClientTypeRoutes;
