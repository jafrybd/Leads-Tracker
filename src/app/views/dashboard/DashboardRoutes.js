import React from "react";
import { authRoles } from "../../auth/authRoles";

const dashboardRoutes = [
  {
    path: "/dashboard/analytics",
    component: React.lazy(() => import("./Analytics")),
    auth: authRoles.admin
  },
  {
    path: "/dashboard/subAdmin",
    component: React.lazy(() => import("./subAdmin")),
    auth: authRoles.editor
  },
  {
    path: "/dashboard/sales",
    component: React.lazy(() => import("./salesPerson")),
    auth: authRoles.guest
  },
  {
    path: "/dashboard/md",
    component: React.lazy(() => import("./md")),
    auth: authRoles.md
  }
];

export default dashboardRoutes;
