/* eslint-disable no-unused-vars */
import React, { Component, Fragment } from "react";
import { Grid, Card } from "@material-ui/core";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import MDdashboard from './shared/MDdashboard'
import MDStatCard from "./shared/MDStatCard";
import { withStyles } from "@material-ui/styles";

class MD extends Component {
  state = {};

  render() {
    let { user } = this.props;

    let { theme } = this.props;

    return (
      <Fragment>

        <div className="analytics m-sm-30 " style={{ marginTop: '35px !important' }}>
          <Grid container spacing={3}>
            <Grid item lg={8} md={8} sm={12} xs={12}>
              <MDStatCard />

              <MDdashboard />
            </Grid>


            <Grid item lg={4} md={4} sm={12} xs={12}>
              <Card className="px-6 py-4 mb-6">
                <div className="card-title" style={{ textAlign: 'center' }}>{user.displayName}</div>
                <div className="card-subtitle" style={{ textAlign: 'center' }}>{user.role}</div>

                <br></br>
                <img src={user.photoURL} alt="user" style={{ height: '300px', display: 'block', margin: 'auto' }} />
              </Card>


            </Grid>
          </Grid>
        </div>
      </Fragment>
    );
  }
}
MD.propTypes = {

  logoutUser: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  logoutUser: PropTypes.func.isRequired,
  user: state.user,
});

export default withStyles({}, { withTheme: true })(
  withRouter(
    connect(mapStateToProps, {
    })(MD)
  )
);