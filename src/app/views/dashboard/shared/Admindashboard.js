import React, { useState, useEffect } from "react";
import myApi from "../../../auth/api";
import history from "../../../../history";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { logoutUser } from "app/redux/actions/UserActions";
import PropTypes from "prop-types";

import Moment from "moment";
import {
  Card,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TablePagination,
} from "@material-ui/core";
import Swal from "sweetalert2";
import axios from "axios";
import localStorageService from "app/services/localStorageService";
const AdminDashboard = (props) => {
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);
  const [clients, setClients] = useState([]);
  let [AdminDashboardList, setAdminDashboardList] = React.useState("");

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
  };

  const items =
    AdminDashboardList &&
    AdminDashboardList.salesPersonWiseAppintmentList &&
    AdminDashboardList.salesPersonWiseAppintmentList.map((item, i) => {
      return (
        <tr key={item.id}>
          <td>{i + 1}</td>
          <td>{item.sales_person_id}</td>
          <td>{item.name}</td>
          <td>{item.total_appointment}</td>
        </tr>
      );
    });

  const fetchData = React.useCallback(() => {
    axios({
      method: "GET",
      url: myApi + "/intraco/lpg/dashboard/data",
      headers: {
        "x-access-token":
          localStorage.getItem("jwt_token") &&
          localStorage.getItem("jwt_token"),
      },
    })
      .then((response) => {
        if (response.data.data) {
          setAdminDashboardList(response.data.data);
        }
        if (
          response.data.message &&
          response.data.message === "Timeout Login Fast"
        ) {
          props.logoutUser();
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  React.useEffect(() => {
    fetchData();
  }, [fetchData]);

  const handleCompleteTask = (id) => {

    axios
      .post(
        myApi + "/intraco/lpg/task/complete",
        {
          task_id: id,
        },
        {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token,
          },
        }
      )
      .then((response) => {

        Swal.fire("Task Complete Successfully !");
        history.push({
          pathname: "/",
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };
  return (
    <Card elevation={3} className="pt-5 mb-6">
      <div className="card-title px-6 mb-3">
        Sales Person Activity ({Moment(new Date()).format("YYYY-MM-DD")})
      </div>
      <div className="overflow-auto">
        <Table className="product-table">
          {AdminDashboardList && AdminDashboardList.salesPersonWiseAppintmentList && AdminDashboardList.salesPersonWiseAppintmentList.length === 0 ? (
            <div
              className="alert alert-info"
              role="alert"
              style={{ width: "100%", background: "white", border: "white" }}
            >
              No more Activities !
            </div>
          ) : (AdminDashboardList &&
            AdminDashboardList.salesPersonWiseAppintmentList.length !== 0 &&
            <TableHead>
              <TableRow>
                <TableCell className="px-6" colSpan={3} align="left">
                  Sales Person
                </TableCell>
                <TableCell className="px-0" colSpan={2} align="center">
                  Total Appointment
                </TableCell>
              </TableRow>
            </TableHead>
          )}
          {AdminDashboardList &&
            AdminDashboardList.salesPersonWiseAppintmentList.length !== 0 &&
            AdminDashboardList.salesPersonWiseAppintmentList
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((user) => (
                <TableBody>
                  <TableRow key={user.id}>
                    <TableCell align="center" className="px-6" colSpan={3}>
                      <div className="flex items-center">
                        <img
                          className="circular-image-small"
                          src="/profile/sales_person.jpg"
                          alt="task"
                        />
                        <p className="m-0 ml-8">{user.name}</p>
                      </div>
                    </TableCell>
                    <TableCell align="center" className="px-0" colSpan={2}>
                      {user.total_appointment}
                    </TableCell>
                  </TableRow>
                </TableBody>
              ))}
        </Table>
        {AdminDashboardList && AdminDashboardList.salesPersonWiseAppintmentList &&
          AdminDashboardList.salesPersonWiseAppintmentList.length > 10 &&
          <TablePagination
            className="px-4"
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={items && items.length}
            rowsPerPage={rowsPerPage}
            page={page}
            backIconButtonProps={{
              "aria-label": "Previous Page",
            }}
            nextIconButtonProps={{
              "aria-label": "Next Page",
            }}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />}
      </div>
    </Card>
  );
};

AdminDashboard.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.user,
  logoutUser: PropTypes.func.isRequired,
});

export default withRouter(
  connect(mapStateToProps, { logoutUser })(AdminDashboard)
);
