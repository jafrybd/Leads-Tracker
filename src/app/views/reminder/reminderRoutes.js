import { auth } from "firebase";
import React from "react";
import { authRoles } from "../../auth/authRoles";

const ReminderRoutes = [

  {
    path: "/reminder/manage",
    component: React.lazy(() => import("./FilterActiveSaler")),
    auth: authRoles.guest
  },
  {
    path: "/reminder/edit/:id",
    component: React.lazy(() => import("./edit/createClient")),
    auth: authRoles.guest
  },

];

export default ReminderRoutes;
