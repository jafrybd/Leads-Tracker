import React, { useState } from "react";
import { Button, Modal, ModalHeader, ModalBody } from "reactstrap";
import AppointmentView from "./appointmentView";

function TaskModal(props) {
  const [modal, setModal] = useState(false);

  const toggle = () => {
    setModal(!modal);
  };

  const closeBtn = (
    <button className="close" onClick={toggle}>
      &times;
    </button>
  );
  const label = props.buttonLabel;

  let button = "";
  let title = "";

  if (label === "Edit") {
    button = (
      <Button
        color="warning"
        onClick={toggle}
        style={{ float: "left", marginRight: "10px" }}
      >
        View
      </Button>
    );
    title = "Details";
  } else {
    button = (
      <Button
        color="btn btn-light"
        onClick={toggle}
        style={{ float: "left", marginRight: "10px" }}
      >
        <img
          src="/assets/viewClient.png"
          style={{ height: "32px" }}
          alt="view"
        />
      </Button>
    );
    title = "View Details";
  }

  return (
    <div style={{ margin: "auto", marginLeft: "33.5%" }}>
      {button}
            <Modal isOpen={modal} toggle={toggle} className={props.className} style={{maxWidth : '350px'}}>

        <ModalHeader toggle={toggle} close={closeBtn}>
          {title}
        </ModalHeader>
        <ModalBody style={{ width: "110%!important" }}>
          <AppointmentView
            viewState={props.viewState}
            toggle={toggle}
            item={props.item}
          />
        </ModalBody>
      </Modal>
    </div>
  );
}

export default TaskModal;
