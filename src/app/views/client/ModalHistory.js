import React, { useState } from "react";
import { Button, Modal, ModalHeader, ModalBody } from "reactstrap";
import CompanyDetails from "./sisterconcernHistory";

function ModalHistory(props) {
  const [modal, setModal] = useState(false);

  const toggle = () => {
    setModal(!modal);
  };

  const closeBtn = (
    <button className="close" onClick={toggle}>
      &times;
    </button>
  );
  const label = props.buttonLabel;

  let button = "";
  let title = "";

  if (label === "Edit") {
    button = (
      <Button
        color="warning"
        onClick={toggle}
        style={{ float: "left", marginRight: "10px" }}
      >
        {label}
      </Button>
    );
    title = "Edit Profile";
  } else {
    button = (
      <Button
        color="btn"
        onClick={toggle}
        style={{ float: "left", marginRight: "10px", marginTop: "5px" }}
      >
        <img
          src="/assets/viewClient.png"
          style={{ height: "48px" }}
          alt="view"
        />
      </Button>
    );
    title = "View Details";
  }

  return (
    <div style={{ margin: "auto", marginLeft: "33.5%" }}>
      {button}
      <Modal
        isOpen={modal}
        toggle={toggle}
        className={props.className}
        style={{ className: "historyMargin", margin: "15% auto" }}
      >
        <ModalHeader toggle={toggle} close={closeBtn}>
          {title}
        </ModalHeader>
        <ModalBody style={{ width: "110%!important" }}>
          <CompanyDetails
            viewState={props.viewState}
            toggle={toggle}
            item={props.item}
          />
        </ModalBody>
      </Modal>
    </div>
  );
}

export default ModalHistory;
