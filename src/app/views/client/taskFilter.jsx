/* eslint-disable no-unused-vars */
import React from "react";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import axios from "axios";
import myApi from '../../auth/api'
import localStorageService from 'app/services/localStorageService'
import DeactiveDataTable from "./deactiveDataTable";
import jsPDF from "jspdf";
import "jspdf-autotable";
import Moment from 'moment'
class TaskFilter extends React.Component {

  constructor(props) {
    super(props)
    const tableData = this.state.tableData

  }
  state = {
    clients: [],
    selectedClient: "",
    discussionTypes: [],
    selectedDiscussionType: "",
    CallTimeTypes: [],
    selectedCallTimeType: "",
    VehicleTypes: [],
    selectedVehicleType: "",
    Leads: [],
    selectedLead: "",

    columns: ['discussion_type', 'vehicle_type', 'sales_person', 'call_at', 'leads', 'call_date'],
    options: {
      filter: true,
      filterType: "dropdown",

    },
    tableData: [],
    SalesPersons: [],
    selectedSalesPerson: '',
    startDate: null,
    endDate: null,
    endDateFormatted: null,
    startDateFormatted: null,
    isSubmitted: false,
    client_type_array: [],
    client_type_arrayy: [],

  };

  componentDidMount() {
    axios({
      "method": "GET",
      "url": myApi + "/intraco/lpg/client/list",
      "headers": {
        'x-access-token': localStorageService.getItem('auth_user').token
      }
    })
      .then((response) => {
        this.setState({
          isSubmitted: true,
          tableData: response.data.data
        })

      })
      .catch((error) => {
        console.log(error)
      })

    // getting clientType
    axios.get(
      myApi + "/intraco/lpg/client_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(client => {
          return {
            value: client.id,
            label: client.type,
            id: client.id,
            type: client.type
          };
        });

        this.setState({
          client_type_array: [
            // {
              // value: 0,
              // label: 'Corporate Client',
              // id: 0,
              // type: 'Corporate Client',
            // }
          ]
            .concat(clientsFromApi)
        });
        this.setState({
          client_type_arrayy: [
            {
              value: 0,
              label: 'Corporate Client',
              id: 0,
              type: 'Corporate Client',
            }
          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });
    // getting clientInformation
    axios.get(
      myApi + "/intraco/lpg/client/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(client => {
          return {
            id: client.id,
            name: client.name
          };
        });

        this.setState({
          clients: [
            {
              id: '',
              name:
                "Select Client Type"
            }
          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting clientInformation
    axios.get(
      myApi + "/intraco/lpg/salesPerson/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(client => {
          return {
            id: client.id,
            name: client.name
          };
        });

        this.setState({
          SalesPersons: [
            {
              id: '',
              name:
                "Select Salesperson"
            }
          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });


  }
  handleChange = event => {
    event.persist();
    this.setState({ [event.target.name]: event.target.value });
  };

  hundleDateChange(startDate, endDate) {

    this.setState(() => ({

      endDate,
      startDate,
    }));
    if (startDate != null) {
      this.setState(() => ({
        startDateFormatted: startDate.format("YYYY-MM-DD"),
      }));
    }
    if (startDate == null) {
      this.setState(() => ({
        startDateFormatted: '',
      }));
    }
    if (endDate != null) {
      this.setState(() => ({
        endDateFormatted: endDate.format("YYYY-MM-DD"),
      }));
    }
    if (endDate == null) {
      this.setState(() => ({
        endDateFormatted: '',
      }));
    }
  }

  handleSubmit = () => {
    let reqData = {
      "client_id": this.state.selectedClient,
      "sales_person_id": this.state.selectedSalesPerson,
      "from_date": this.state.startDateFormatted,
      "to_date": this.state.endDateFormatted
    }
    if (reqData.to_date === '' || reqData.to_date === null || reqData.to_date === 'null') {
      delete reqData.to_date
    }
    if (reqData.from_date === '' || reqData.from_date === null || reqData.from_date === 'null') {
      delete reqData.from_date
    }
    axios.post(myApi + '/intraco/lpg/appointment/searchDeactive', reqData, {
      headers: {
        'x-access-token': localStorageService.getItem('auth_user').token // override instance defaults
      },
    })
      .then(res => {
        if (res.data.data) {
          this.setState({
            isSubmitted: true,
            tableData: Array.isArray(res.data.data) ? res.data.data : []

          })

        }
      })
  }


  exportPDF = () => {
    const locateValueById = (types, id) => {
      let item = types.find((it) => it.id === Number(id));
      return item;
    };
    const addFooters = doc => {


      const pageCount = doc.internal.getNumberOfPages()
      var footer = new Image();
      footer.src = '/assets/footerPdf.png';

      doc.setFont('helvetica', 'italic')
      doc.setFontSize(8)
      for (var i = 1; i <= pageCount; i++) {
        doc.setPage(i)

        doc.addImage(image, 'JPEG', pageWidth - 110, 0, 100, 100)
        doc.addImage(leftBar, 'JPEG', 0, 0, 16, 270)
        doc.addImage(footer, 'JPEG', 0, pageHeight - 60, pageWidth, 60)
      }
    }

    const unit = "pt";
    const size = "A4"; // Use A1, A2, A3 or A4
    const orientation = "portrait"; // portrait or landscape

    const marginLeft = 40;
    const doc = new jsPDF(orientation, unit, size);
    var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
    var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
    doc.setFontSize(15);

    var leftBar = new Image();
    leftBar.src = '/assets/leftBar.png';
    var image = new Image();
    image.src = '/assets/logoNew.png';
    const title = "Clients Report";
    const headers = [["SL", "Name", "Client Type", "Primary Number", "Email", "Location"]];

    const data = this.state.tableData.map((ap, i) => [i + 1, ap.name, locateValueById(this.state.client_type_arrayy, ap.client_type_id) &&
      locateValueById(this.state.client_type_arrayy, ap.client_type_id).type, ap.primary_phone_no,
    ap.email, ap.address]);


    let content = {
      startY: 150,
      head: headers,
      body: data,
      margin: {
        bottom: 120,
        top: 150
      }
    };


    doc.setFont('helvetica', 'bold')
    doc.setFontSize(15)
    doc.text(title, marginLeft, 50);
    doc.setFontSize(10)
    const filterData = this.state.startDateFormatted && this.state.endDateFormatted && 'Date : ' + this.state.startDateFormatted + " > " + this.state.endDateFormatted
    this.state.startDateFormatted && this.state.endDateFormatted && doc.text(filterData, marginLeft, 70)


    doc.autoTable(content);

    doc.setTextColor(100);
    doc.setFontSize(10);

    addFooters(doc)

    doc.save("clients_report(" + Moment(new Date()).format('YYYY-MM-DD') + ").pdf")
  }

  render() {
    return (
      <>
        {this.state.tableData.length !== 0 &&
          <button
            className="btn btn-primary"
            style={{ marginTop: '-60px', marginLeft: '85%' }}
            onClick={() => this.exportPDF()}>Generate Report</button>
        }

        {

          this.state.isSubmitted && this.state.tableData &&
          <DeactiveDataTable items={this.state.tableData} />

        }
      </>
    );
  }
}
export default TaskFilter;