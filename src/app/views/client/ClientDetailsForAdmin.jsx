/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Table } from 'reactstrap';
import axios from 'axios'
import myApi from '../../auth/api'
import history from '../../../history'
import Swal from 'sweetalert2'
import localStorageService from 'app/services/localStorageService';
import Moment from 'moment'
import {
  IconButton,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Icon,
  TablePagination
} from "@material-ui/core";
function ClientDetailsForAdmin(props) {
  const [item, setItem] = useState([])
  const [taskList, setTaskList] = useState([])
  const [prospectTypes, setProspectTypes] = useState([])

  const [form, setValues] = useState({
    id: 0,
    name: '',
    last: '',
    email: '',
    phone_number: '',
    location: '',
    hobby: '',
    image: '',

  })
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(+event.target.value);
  };

  const onChange = e => {
    setValues({
      ...form,
      [e.target.name]: e.target.value
    })
  }

  const submitFormAdd = e => {
    e.preventDefault()

  }

  const submitFormEdit = e => {
    e.preventDefault()

    axios.post(myApi + '/intraco/lpg/salesPerson/profile/update', {
      'id': form.id, 'name': form.name,
      'email': form.email, 'phone_number': form.phone_number
    }, {

      "headers": {
        'x-access-token': localStorage.getItem("jwt_token")
      }
    })

      .then(response => {
        //console.log(response.data)
        if (!response.data.error) {
          Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        } else {
          Swal.fire({
            icon: "error",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        }
        if (response.data.error === false) {
          props.updateState(form)
        }
        props.toggle()
      })

      .catch(err => console.log(err))
  }


  useEffect(() => {
    // getting prospectType
    axios.get(
      myApi + "/intraco/lpg/prospect_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(prospectType => {
          return {
            id: prospectType.id,
            type: prospectType.type
          };
        });

        setProspectTypes([
          {
            id: '',
            type:
              "Select if you want to change"
          }
        ].concat(
          clientsFromApi)
        )
      })
      .catch(error => {
        console.log(error);

      });

    if (props.item) {
      const { id, name, last, email, phone_number, location, hobby, image } = props.item
      setValues({ id, name, last, email, phone_number, location, hobby, image })


      axios.get(myApi + '/intraco/lpg/client/profile/view/1/' + props.item.id, {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }).then(response => {


        if (response.data.data.task.length) {
          setTaskList(response.data.data.task)
        }
      })

    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, false)

  const locateValueById = (types, id) => {
    let item = types.find(it => it.id === Number(id))
    return item
  }

  return (

    <div>
      <div className="modal-dialog" style={{ maxWidth: '95%' }}>
         <div className="modal-content" style={{marginTop : '-35px'}}>
          <div className="modal-header">
            <button type="button" className="close" data-dismiss="modal" aria-hidden="true" />
            {taskList.length !== 0 && <h4 className="modal-title" id="myModalLabel">More About <font color='green'>{props.item.name}</font></h4>}
          </div>

          {taskList.length === 0 &&
            <div className="modal-body">
              <div className="alert alert-primary" role="alert">
                Sorry, There is no task history for <font color='green'>{props.item.name}</font>
              </div>
            </div>}

          {taskList.length !== 0 &&
            <div className="modal-body">
              <Table responsive hover>

                <TableHead>
                  <TableRow>
                    <TableCell className="px-0">#</TableCell>
                    <TableCell className="px-0">Sales Person</TableCell>
                    <TableCell className="px-0">Discussion</TableCell>
                    <TableCell className="px-0">Vehicle</TableCell>
                    <TableCell className="px-0">Date</TableCell>
                    <TableCell className="px-0">Time</TableCell>
                    <TableCell className="px-0">Leads</TableCell>
                    <TableCell className="px-0">Prospect</TableCell>
                    <TableCell className="px-0">Status</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>

                  {taskList
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((item, id) => (
                      <TableRow key={id}>
                        <TableCell align="left">
                         {page * rowsPerPage + id + 1}
                        </TableCell>
                        <TableCell className="px-0 capitalize" align="left">
                          {item.sales_person}
                        </TableCell>
                        <TableCell className="px-0 capitalize" align="left">
                          {item.discussion_type.map(e => e.type)}
                        </TableCell>
                        <TableCell className="px-0 capitalize" align="left">
                          {item.vehicle_type.map(function (item, index) {
                            return (
                              <span key={`demo_snap_${index}`}>
                                {(index ? ", " : "") + item.type}
                              </span>
                            );
                          })}
                        </TableCell>
                        <TableCell className="px-0 capitalize" align="left">
                          {Moment(item.call_date).format('YYYY-MM-DD')}
                        </TableCell>
                        <TableCell className="px-0 capitalize" align="left">
                          {Moment(item.call_time, 'hh:mm').format('LT')}
                        </TableCell>
                        <TableCell className="px-0 capitalize">
                          {item.leads}
                        </TableCell>
                        <TableCell className="px-0 capitalize">
                          {item.prospect_type_id === 0
                            ? "Default"
                            : locateValueById(prospectTypes, item.prospect_type_id)
                              ? locateValueById(prospectTypes, item.prospect_type_id).type
                              : item.prospect_type}
                        </TableCell>
                        <TableCell className="px-0 capitalize">
                          {item.status && item.status === "complete" ? (
                            <small className="border-radius-4 bg-secondary text-white px-2 py-2px ">
                              Completed
                            </small>
                          ) : (
                              <small className="border-radius-4 bg-primary text-white px-2 py-2px ">
                                Incomplete
                              </small>
                            )}
                        </TableCell>
                      </TableRow>
                    ))}
                </TableBody>
              </Table>
              {taskList.length !== 0 && <TablePagination
                className="px-4"
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={taskList && taskList.length}
                rowsPerPage={rowsPerPage}
                page={page}
                backIconButtonProps={{
                  "aria-label": "Previous Page"
                }}
                nextIconButtonProps={{
                  "aria-label": "Next Page"
                }}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
              />}
            </div>
          }

        </div>

      </div>
    </div>
  )
}

export default ClientDetailsForAdmin
