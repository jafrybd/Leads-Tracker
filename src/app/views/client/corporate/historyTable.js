import React, { useState } from "react";
import { Button, Modal, ModalHeader, ModalBody } from "reactstrap";
import ClientDetails from "./historyDetails";

function ModalOther(props) {
  const [modal, setModal] = useState(false);

  const toggle = () => {
    setModal(!modal);
  };

  const closeBtn = (
    <button className="close" onClick={toggle}>
      &times;
    </button>
  );
  const label = props.buttonLabel;

  let button = "";
  let title = "";

  if (label === "Edit") {
    button = (
      <Button
        color="warning"
        onClick={toggle}
        style={{ float: "left", marginRight: "10px" }}
      >
        {label}
      </Button>
    );
    title = "Edit Profile";
  } else {
    button = (
      <Button
        color="btn btn-light"
        onClick={toggle}
        style={{ float: "left", marginLeft: "-5px" }}
      >
        <img src="/assets/view.png" />
      </Button>
    );
    title = "View Details";
  }

  return (
    <div style={{ margin: "auto", marginLeft: "40%" }}>
      {button}
      <Modal
        isOpen={modal}
        toggle={toggle}
        className="taskModal"
        style={{ className: "historyMargin" }}
      >
        <ModalHeader toggle={toggle} close={closeBtn}>
          {title}
        </ModalHeader>
        <ModalBody style={{ width: "110%!important" }}>
          <ClientDetails
            viewState={props.viewState}
            toggle={toggle}
            item={props.item}
          />
        </ModalBody>
      </Modal>
    </div>
  );
}

export default ModalOther;
