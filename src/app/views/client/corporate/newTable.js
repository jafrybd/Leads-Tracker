import React, { useState } from "react";
import { Button } from "reactstrap";
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TablePagination,
} from "@material-ui/core";
import history from "../../../../history";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
const DeactiveDataTable = (props) => {
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
  };

  return (
    <>
      <TableContainer
        width="100%"
        component={Paper}
        style={{
          // width : 650,
          boxShadow:
            "0px 2px 1px -1px rgba(255, 255, 255, 0.06),0px 1px 1px 0px rgba(255, 255, 255, 0.04),0px 1px 3px 0px rgba(255, 255, 255, 0.03)",
          backgroundColor: "#fafafa",
        }}
      >
        <Table className="whitespace-pre">
          <TableHead>
            <TableRow>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "10%" }}
              >
                SL.
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "30%" }}
              >
                Company Title
              </TableCell>

              <TableCell
                align="right"
                className="px-0"
                style={{ width: "60%", right: "15px", position: "relative" }}
              >
                Details
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {props.items &&
              props.items
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((item, id) => (
                  <TableRow key={id}>
                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      style={{ width: "10%" }}
                    >
                      {page * rowsPerPage + id + 1}
                    </TableCell>
                    <TableCell
                      className="px-0 capitalize"
                      align="center"
                      style={{ width: "70%" }}
                    >
                      {item.title}
                    </TableCell>

                    <TableCell
                      className="px-0 capitalize"
                      align="right"
                      style={{ width: "20%" }}
                    >
                      <Button
                        style={{
                          backgroundColor: "green",
                          borderColor: "green",
                        }}
                        onClick={() =>
                          history.push({
                            pathname: "/corporate/" + item.company_id,
                            state: item,
                          })
                        }
                        item={item}
                        color="btn btn-warning"
                        style={{ marginTop: "7px", marginLeft: "auto" }}
                      >
                        View
                      </Button>
                    </TableCell>
                  </TableRow>
                ))}
          </TableBody>
        </Table>
      </TableContainer>

      <TablePagination
        style={{
          maxWidth: 400,
          overflowX: "hidden",
          padding: "0px!important",
          display: "contents",
        }}
        className="px-4"
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={props.items && props.items.length}
        rowsPerPage={rowsPerPage}
        page={page}
        backIconButtonProps={{
          "aria-label": "Previous Page",
        }}
        nextIconButtonProps={{
          "aria-label": "Next Page",
        }}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </>
  );
};

export default DeactiveDataTable;
