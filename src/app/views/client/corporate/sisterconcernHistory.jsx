import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Table } from 'reactstrap';
import axios from 'axios'
import myApi from '../../../auth/api'

import localStorageService from 'app/services/localStorageService';
import moment from 'moment'
import {
  IconButton,

  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Icon,
  TablePagination
} from "@material-ui/core";
function CompanyDetails(props) {

  const [CompanyDetails, setCompanyDetails] = useState([])


  const [form, setValues] = useState({
    id: 0,
    company_id: '',
    company_sister_concern_id: '',
    sales_person_id: '',
    sister_concern_title: '',
    // location: '',
    // hobby: '',
    // image: '',

  })
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(+event.target.value);
  };

  const onChange = e => {
    setValues({
      ...form,
      [e.target.name]: e.target.value
    })
  }

  const submitFormAdd = e => {
    e.preventDefault()

  }

  const submitFormEdit = e => {
    e.preventDefault()


  }


  useEffect(() => {


    if (props.item) {
      const { id, company_id, company_sister_concern_id, sales_person_id, sister_concern_title } = props.item
      setValues({ id, company_id, company_sister_concern_id, sales_person_id, sister_concern_title })


      axios.post(myApi + '/intraco/lpg/company_sister_concerns/' + props.company_id + '/searchSisterConcernBySalesPerson', {
        "title": props.sister_concern_title
      }, {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }).then(response => {
        console.log(response.data.data)

        if (response.data.data) {
          setCompanyDetails(response.data.data)
        }
      })

    }
  }, false)

  const locateValueById = (types, id) => {
    let item = types.find(it => it.id === Number(id))
    return item
  }

  return (

    <div>


      {CompanyDetails.length !== 0 &&
        <div className="modal-body">
          <Table responsive hover>

            <TableHead>
              <TableRow>
                <TableCell className="px-0" style={{ width: "5%", textAlign: 'center' }}>#</TableCell>
                <TableCell className="px-0" style={{ width: "20%", textAlign: 'center' }}>Sister Concern</TableCell>
                {/* <TableCell className="px-0" style={{ width: "25%", textAlign: 'center' }}>Start Date</TableCell>
                <TableCell className="px-0" style={{ width: "25%", textAlign: 'center' }}>End Date</TableCell> */}
                <TableCell className="px-0" style={{ width: "25%", textAlign: 'center' }}>Details</TableCell>

              </TableRow>
            </TableHead>
            <TableBody>

              {CompanyDetails
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((item, id) => (
                  <TableRow key={id}>
                    <TableCell className="px-0" style={{ width: "5%", textAlign: 'center' }}>
                     {page * rowsPerPage + id + 1}
                    </TableCell>
                    <TableCell className="px-0 capitalize" align="left" style={{ width: "20%", textAlign: 'center' }}>
                      {item.sister_concern_title}
                    </TableCell>
                    {/* <TableCell className="px-0 capitalize" align="left" style={{ width: "25%", textAlign: 'center' }}>
                      {moment(item.start_date).format('DD/MM/YYYY')}
                    </TableCell>
                    <TableCell className="px-0 capitalize" align="left" style={{ width: "25%", textAlign: 'center' }}>
                      {item.end_date === "0000-00-00 00:00:00" || item.end_date === null ? " " : moment(item.end_date).format('DD/MM/YYYY')}
                    </TableCell> */}
                    <TableCell className="px-0 capitalize" align="left" style={{ width: "25%", textAlign: 'center' }}>
                      {item.remove_reason}
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
          {CompanyDetails.length !== 0 && <TablePagination
            className="px-4"
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={CompanyDetails && CompanyDetails.length}
            rowsPerPage={rowsPerPage}
            page={page}
            backIconButtonProps={{
              "aria-label": "Previous Page"
            }}
            nextIconButtonProps={{
              "aria-label": "Next Page"
            }}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />}
        </div>
      }

    </div>

  )
}

export default CompanyDetails
