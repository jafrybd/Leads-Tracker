/* eslint-disable no-unused-vars */
import React from "react";
import { Button } from "reactstrap";
import axios from "axios";
import myApi from "../../../auth/api";
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TablePagination,
} from "@material-ui/core";
import Swal from "sweetalert2";
import ModalOther from "./historyTable";
import history from "../../../../history";

const Datatable = (props) => {
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
  };

  const deleteItem = (id, title, active_status) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You will be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, deactive it!",
      allowOutsideClick: false,
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .post(
            myApi + "/intraco/lpg/company/delete",
            { id: id },
            {
              headers: {
                "x-access-token": localStorage.getItem("jwt_token"),
              },
            }
          )
          .then((res) => {
            Swal.fire({
              icon: "success",
              title: "Activated",
              showConfirmButton: false,
              timer: 1000
            });
          })
          .then((item) => {
            props.updateState({
              id,
              title,
              active_status: 1,
            });
          })
          .catch((err) => console.log(err));
      }
    });
  };

  const activeItem = (id, title, active_status) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You will be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#1a71b5",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, active it!",
      allowOutsideClick: false,
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .post(
            myApi + "/intraco/lpg/company/active",
            { id: id },
            {
              headers: {
                "x-access-token": localStorage.getItem("jwt_token"),
              },
            }
          )
          .then((res) => {
            Swal.fire({
              icon: "success",
              title: "Activated",
              showConfirmButton: false,
              timer: 1000
            });
          })
          .then((item) => {
            props.updateState({
              id,
              title,
              active_status: 0,
            });
          });
      }
    });
  };

  const details = (id) => {};

  return (
    <div className="w-full overflow-auto">
      <Table className="whitespace-pre">
        <TableHead>
          <TableRow>
            <TableCell align="center" className="px-0" style={{ width: "10%" }}>
              SL.
            </TableCell>
            <TableCell align="center" className="px-0" style={{ width: "30%" }}>
              Title
            </TableCell>
            <TableCell align="center" className="px-0" style={{ width: "20%" }}>
              History
            </TableCell>

            <TableCell align="center" className="px-0" style={{ width: "40%" }}>
              Action
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.items &&
            props.items
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((item, id) => (
                <TableRow key={id}>
                  <TableCell
                    className="px-0 capitalize"
                    align="center"
                    style={{ width: "10%" }}
                  >
                   {page * rowsPerPage + id + 1}
                  </TableCell>
                  <TableCell
                    className="px-0 capitalize"
                    align="center"
                    style={{ width: "30%" }}
                  >
                    {item.sister_concern_title}
                  </TableCell>
                  <TableCell
                    className="px-0 capitalize"
                    align="center"
                    style={{ width: "20%" }}
                  >
                    <ModalOther
                      style={{ maxWidth: "700px!important" }}
                      buttonLabel="View"
                      item={item}
                    />
                  </TableCell>
                  <TableCell
                    className="px-0 capitalize"
                    align="center"
                    style={{ width: "40%" }}
                  >
                    <Button
                      onClick={() =>
                        history.push({
                          pathname:
                            "/client/member/add/" +
                            item.company_sister_concern_id,
                          state: item.company_sister_concern_id,
                        })
                      }
                      item={item}
                      color="btn btn-warning"
                      style={{
                        margin: "auto",
                        marginTop: "7px",
                        height: "37px",
                      }}
                    >
                      <span
                        class="material-icons MuiIcon-root MuiIcon-fontSizeMedium"
                        aria-hidden="true"
                        title="person_add"
                      >
                        person_add
                      </span>
                    </Button>{" "}
                    <Button
                      onClick={() =>
                        history.push({
                          pathname:
                            "/client/member/list/" +
                            item.company_sister_concern_id,
                          state: item.company_sister_concern_id,
                        })
                      }
                      item={item}
                      color="btn btn-warning"
                      style={{
                        margin: "auto",
                        marginTop: "7px",
                      }}
                    >
                      View Details
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
        </TableBody>
      </Table>

      <TablePagination
        style={{
          maxWidth: 400,
          overflowX: "hidden",
          padding: "0px!important",
          display: "contents",
        }}
        className="px-4"
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={props.items.length}
        rowsPerPage={rowsPerPage}
        page={page}
        backIconButtonProps={{
          "aria-label": "Previous Page",
        }}
        nextIconButtonProps={{
          "aria-label": "Next Page",
        }}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </div>
  );
};

export default Datatable;
