import React, { useEffect, useState, useCallback } from "react";
import { Button } from "reactstrap";
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TablePagination,
} from "@material-ui/core";
import ModalForm from "./Modal";
import axios from "axios";
import myApi from "../../auth/api";
import Swal from "sweetalert2";
import localStorageService from "app/services/localStorageService";
import ModalOther from "./ModalOther";

function DataTable(props) {
  const [clientTypes, setClientTypes] = useState([]);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
  };
  const resetPassword = (id) => {
    axios
      .post(
        myApi + "/intraco/lpg/salesPerson/profile/resetPassword",
        { id: id },
        {
          headers: {
            "x-access-token": localStorage.getItem("jwt_token"),
          },
        }
      )
      .then((response) => {
        Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
      })

      .catch((err) => console.log(err));
    // }
  };
  const deleteItem = (id) => {
    Swal.fire({
      title: "Are you sure?",
      // text: "You will be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
      allowOutsideClick: false,
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .post(
            myApi + "/intraco/lpg/client/profile/delete",
            { id: id },
            {
              headers: {
                "x-access-token": localStorageService.getItem("auth_user")
                  .token,
              },
            }
          )
          .then((response) => {})
          .then((item) => {
            props.deleteItemFromState(id);
            Swal.fire("Deleted Successfully !");
          })
          .catch((err) => console.log(err));
      }
    });
  };
  useEffect(
    useCallback(() => {
      // getting clientType
      axios
        .get(myApi + "/intraco/lpg/client_type/list", {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token,
          },
        })
        .then((res) => {
          let clientsFromApi = res.data.data.map((client) => {
            return {
              id: client.id,
              type: client.type,
            };
          });

          setClientTypes(
            [
              {
                id: "",
                type: "Select if you want to change",
              },
            ].concat(clientsFromApi)
          );
        })
        .catch((error) => {
          console.log(error);
        });
    }, [clientTypes]),
    []
  );

  const locateValueById = (types, id) => {
    let item = types.find((it) => it.id === Number(id));
    return item;
  };

  return (
    <div className="w-full overflow-auto">
      <Table className="whitespace-pre">
        <TableHead>
          <TableRow>
            <TableCell
              align="center"
              className="px-0"
              style={{ width: "30px" }}
            >
              SL.
            </TableCell>
            <TableCell align="center" className="px-0">
              Name
            </TableCell>
            <TableCell
              align="center"
              className="px-0"
              style={{ width: "300px" }}
            >
              Email
            </TableCell>
            <TableCell align="center" className="px-0">
              Address
            </TableCell>
            <TableCell align="center" className="px-0">
              Type
            </TableCell>
            <TableCell align="center" className="px-0">
              Task Details
            </TableCell>
            <TableCell align="center" className="px-0">
              Action
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.items &&
            props.items
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((item, id) => (
                <TableRow key={id}>
                  <TableCell
                    className="px-0 capitalize"
                    align="center"
                    style={{ width: "30px" }}
                  >
                   {page * rowsPerPage + id + 1}
                  </TableCell>
                  <TableCell className="px-0 capitalize" align="center">
                    {item.name}
                  </TableCell>
                  <TableCell
                    className="px-0 "
                    align="center"
                    style={{ width: "300px" }}
                  >
                    {item.email}
                  </TableCell>
                  <TableCell className="px-0 capitalize" align="center">
                    {item.address}
                  </TableCell>
                  <TableCell className="px-0 capitalize" align="center">
                    {locateValueById(clientTypes, item.client_type_id)
                      ? locateValueById(clientTypes, item.client_type_id).type
                      : item.clientType}
                  </TableCell>
                  <TableCell className="px-0 capitalize" align="center">
                    <ModalOther
                      style={{ maxWidth: "700px!important" }}
                      buttonLabel="View"
                      item={item}
                    />
                  </TableCell>
                  <TableCell className="px-0 capitalize" align="center">
                    <div style={{ width: "150px" }}>
                      <ModalForm
                        style={{ maxWidth: "700px!important" }}
                        buttonLabel="Edit"
                        item={item}
                        updateState={props.updateState}
                      />
                      <Button
                        color="danger"
                        onClick={() => deleteItem(item.id)}
                      >
                        Delete
                      </Button>
                    </div>
                  </TableCell>
                </TableRow>
              ))}
        </TableBody>
      </Table>

      <TablePagination
        style={{
          maxWidth: 400,
          overflowX: "hidden",
          padding: "0px!important",
          display: "contents",
        }}
        className="px-4"
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={props.items.length}
        rowsPerPage={rowsPerPage}
        page={page}
        backIconButtonProps={{
          "aria-label": "Previous Page",
        }}
        nextIconButtonProps={{
          "aria-label": "Next Page",
        }}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </div>
  );
}

export default DataTable;
