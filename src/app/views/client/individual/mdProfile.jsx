import React, { Component } from "react";
import { Breadcrumb } from "matx";
import { Card } from "@material-ui/core";
import ClientManage from "./clientManage";

// const a = JSON.parse(localStorage.getItem('auth_user')).displayName

class MDProfile extends Component {

  render() {
    return (
      <div className="m-sm-30">
        <div className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: "Task List", path: "/task/filter" },
              { name: "Client" },
              // { name: JSON.parse(localStorage.getItem('auth_user')).displayName }
            ]}
          />
        </div>
        <Card className="px-6 pt-2 pb-4"><ClientManage /></Card>
      </div>
    );
  }
}

export default MDProfile;
