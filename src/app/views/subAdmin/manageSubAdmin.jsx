import React, { Component } from "react";
import { Breadcrumb } from "matx";
import ManageForm from "./manageForm";
import { Card } from "@material-ui/core";

class ManageSuperAdmin extends Component {
  render() {
    return (
      <div className="m-sm-30">
        <div  className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: "Sub Admin", path: "/subAdmin/manage" },
              { name: "Manage Sub Admin" }
            ]}
          />
        </div>
        <Card className="px-6 pt-2 pb-4"><ManageForm /></Card>
      </div>
    );
  }
}

export default ManageSuperAdmin;
