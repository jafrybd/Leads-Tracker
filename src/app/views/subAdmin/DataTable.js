import React from "react";
import { Table, Button } from "reactstrap";
import ModalForm from "./Modal";
import axios from "axios";
import myApi from "../../auth/api";
import Swal from "sweetalert2";

function DataTable(props) {
  const resetPassword = (id) => {
    Swal.fire({
      title: "Are you sure?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, reset password!",
      allowOutsideClick: false,
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .post(
            myApi + "/intraco/lpg/admin/subAdmin/profile/resetPassword",
            { id: id },
            {
              headers: {
                "x-access-token": localStorage.getItem("jwt_token"),
              },
            }
          )
          .then((response) => {
            console.log(response.message);
            Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
          })

          .catch((err) => console.log(err));
      }
    });
  };

  const deleteItem = (id) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .post(
            myApi + "/intraco/lpg/admin/subAdmin/delete",
            { id: id },
            {
              headers: {
                "x-access-token": localStorage.getItem("jwt_token"),
              },
            }
          )
          .then((response) => {
            Swal.fire(response.data.message, "", "success");
          })
          .then((item) => {
            props.deleteItemFromState(id);
          })
          .catch((err) => console.log(err));
      }
    });
  };

  const items =
    props.items &&
    props.items.map((item, i) => {
      return (
        <tr key={item.id}>
          <th scope="row" style={{ textAlign: "center" }}>
            {i + 1}
          </th>
          <td style={{ textAlign: "center" }}>{item.name}</td>
          <td style={{ textAlign: "center" }}>{item.phone_number}</td>
          <td style={{ textAlign: "center" }}>{item.email}</td>
          <td style={{ textAlign: "center" }}>
            <div style={{ width: "120px", margin: "auto" }}>
              <Button
                className="btn btn-light"
                onClick={() => resetPassword(item.id)}
              >
                <img
                  src="/assets/reset-password.png"
                  style={{ height: "32px" }}
                  alt='pic'
                />
              </Button>
            </div>
          </td>
          <td style={{ textAlign: "center" }}>
            <div style={{ width: "150px", margin: "auto" }}>
              <ModalForm
                buttonLabel="Edit"
                item={item}
                updateState={props.updateState}
              />{" "}
              <Button color="danger" onClick={() => deleteItem(item.id)}>
                Delete
              </Button>
            </div>
          </td>
        </tr>
      );
    });

  return (
    <Table responsive hover>
      <thead>
        <tr>
          <th style={{ textAlign: "center" }}>ID</th>
          <th style={{ textAlign: "center" }}>Name</th>
          <th style={{ textAlign: "center" }}>Phone</th>
          <th style={{ textAlign: "center" }}>Email</th>
          <th style={{ textAlign: "center" }}>Reset Password</th>
          <th style={{ textAlign: "center" }}> Action</th>
        </tr>
      </thead>
      <tbody>{items}</tbody>
    </Table>
  );
}

export default DataTable;
