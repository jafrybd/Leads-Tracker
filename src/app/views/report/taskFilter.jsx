'use strict';

import React, { Fragment, useState } from "react";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import "react-datepicker/dist/react-datepicker.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from "axios";
import myApi from '../../auth/api'
import localStorageService from 'app/services/localStorageService'
import PaggingTable from "./paggingTable";
import moment from 'moment'
import Moment from 'moment'
import { DatePicker } from "@material-ui/pickers";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker
} from "@material-ui/pickers";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import {
  Button,
  Icon,
  Grid,
} from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import jsPDF from "jspdf";
import "jspdf-autotable";
import Swal from "sweetalert2";

class TaskFilter extends React.Component {

  constructor(props) {
    super(props)
    const tableData = []

    this.state = {
      tableData: [],
      selectedDate: moment(new Date()).format('YYYY-MM-DD'),
      selectedMonth: moment(new Date()).format('YYYY-MM-DD'),
      selectedYear: moment(new Date()).format('YYYY-MM-DD'),
      date: moment(new Date()).format('YYYY-MM-DD'),
      // prospectTypes: []

    };
  }

  state = {
    prospectTypes: [],
    clients: [],
    selectedClient: "",
    discussionTypes: [],
    selectedDiscussionType: "",
    CallTimeTypes: [],
    selectedCallTimeType: "",
    VehicleTypes: [],
    selectedVehicleType: "",
    Leads: [],
    selectedLead: "",

    columns: ['discussion_type', 'vehicle_type', 'sales_person', 'call_at', 'leads', 'call_date'],
    options: {
      filter: true,
      filterType: "dropdown",

    },
    tableData: [],
    SalesPersons: [],
    selectedSalesPerson: '',
    startDate: null,
    endDate: null,
    endDateFormatted: null,
    startDateFormatted: null,
    isSubmitted: false
  };


  componentDidMount() {
    this.state.selectedMonth && (
      this.setState({
        selectedMonth: Moment(new Date(), 'YYYY/MM/DD').format("MM"),
        selectedYear: Moment(new Date(), 'YYYY/MM/DD').format("YYYY"),
      })
    )
    // getting prospectType
    axios.get(
      myApi + "/intraco/lpg/prospect_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(prospectType => {
          return {
            id: prospectType.id,
            type: prospectType.type
          };
        });

        this.setState({
          prospectTypes: [
            {
              id: '',
              type:
                "Select if you want to change"
            }
          ].concat(
            clientsFromApi)
        })
      })
      .catch(error => {
        //console.log(error);

      });

    //getting data
    axios.post(myApi + "/mdSir/searchTask",
      {
        "month": moment(new Date(), 'YYYY/MM/DD').format("M"),
        "year": moment(new Date(), 'YYYY/MM/DD').format("YYYY")
      },
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    ).then((response) => {
      //console.log(response)
      this.setState({
        isSubmitted: true,
        tableData: response.data.data
      })


    })
      .catch((error) => {
        //console.log(error)
      })


    // getting clientInformation
    axios.get(
      myApi + "/intraco/lpg/client/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(client => {
          return {
            id: client.id,
            name: client.name
          };
        });

        this.setState({
          clients: [
            {
              id: '',
              name:
                "Select Client Type"
            }
          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        //console.log(error);
      });

    // getting clientInformation
    axios.get(
      myApi + "/intraco/lpg/salesPerson/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(client => {
          return {
            id: client.id,
            name: client.name
          };
        });

        this.setState({
          SalesPersons: [
            {
              id: '',
              name:
                "Select Salesperson"
            }
          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        //console.log(error);
      });


  }



  handleSubmit = () => {

    axios.post(myApi + "/mdSir/searchTask",
      {
        "month": this.state.selectedMonth ? this.state.selectedMonth : moment(new Date(), 'YYYY/MM/DD').format("MM"),
        "year": this.state.selectedYear ? this.state.selectedYear : moment(new Date(), 'YYYY/MM/DD').format("YYYY"),
      },
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    ).then((response) => {
      if (response.data.data.length === 0) {
        Swal.fire({
            icon: "error",
            title: "No Data Found",
            showConfirmButton: false,
          });
      }
      this.setState({
        isSubmitted: true,
        tableData: response.data.data
      })


    })
      .catch((error) => {
        //console.log(error)
      })
  }

  exportPDF = () => {
    const locateValueById = (types, id) => {
      let item = types.find(it => it.id === Number(id))
      return item
    }
    const addFooters = doc => {


      const pageCount = doc.internal.getNumberOfPages()
      var footer = new Image();
      footer.src = '/assets/footerPdf.png';

      doc.setFont('helvetica', 'italic')
      doc.setFontSize(8)
      for (var i = 1; i <= pageCount; i++) {
        doc.setPage(i)

        doc.addImage(image, 'JPEG', pageWidth - 110, 0, 100, 100)
        doc.addImage(leftBar, 'JPEG', 0, 0, 16, 270)
        doc.addImage(footer, 'JPEG', 0, pageHeight - 60, pageWidth, 60)
      }
    }

    const unit = "pt";
    const size = "A4"; // Use A1, A2, A3 or A4
    const orientation = "portrait"; // portrait or landscape

    const marginLeft = 40;
    const doc = new jsPDF(orientation, unit, size);
    var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
    var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
    doc.setFontSize(15);

    var leftBar = new Image();
    leftBar.src = '/assets/leftBar.png';
    var image = new Image();
    image.src = '/assets/logoNew.png';


    const title = "Task Report";
    const headers = [["SL", "Sales Person", "Client", "Discussion Type", "Vehicle Type",
      "Lead", "Prospect Type", "Status"]];


    const data = this.state.tableData.map((ap, i) => [i + 1, ap.sales_person, ap.client_info.map((e) => e.name || e.company_title + '_' + e.title),
    ap.discussion_type.map(function (item, index) {
      return (
        (index ? ", " : "") + item.type
      )
    }), ap.vehicle_type.map(function (item, index) {
      return (
        (index ? ", " : "") + item.type
      )
    }), ap.leads,
    ap.prospect_type_id === 0
      ? "Default"
      : locateValueById(this.state.prospectTypes, ap.prospect_type_id)
        ? locateValueById(this.state.prospectTypes, ap.prospect_type_id).type
        : ap.prospect_type,
    ap.status]);

    let content = {
      startY: 150,
      head: headers,
      body: data,
      margin: {
        bottom: 120,
        top: 150
      }
    };

    doc.setFont('helvetica', 'bold')
    doc.setFontSize(15)
    doc.text(title, marginLeft, 50);

    doc.setFontSize(12)

    const filterData = this.state.date && 'Filter By : ' + moment(this.state.date).format("MM/YYYY")
    doc.text(filterData, marginLeft, 70)
    doc.autoTable(content);

    //footer
    let str = "Your footer text";
    doc.setTextColor(100);
    doc.setFontSize(10);

    addFooters(doc)

    doc.save("task_report(" + Moment(new Date()).format('YYYY-MM-DD') + ").pdf")
  }

  handleChange(date) {
    this.setState({
      startDate: date
    })
  }



  handleDateChange = date => {

    date = Moment(date).format('YYYY-MM-DD')

    this.setState({ date });


    this.setState({

      selectedMonth: moment(date).format('MM'),
      selectedYear: moment(date).format('YYYY')
    });
  };



  render() {
    let { date } = this.state
    return (

      <>
        <ValidatorForm
          ref="form"
          onSubmit={this.handleSubmit}
        >
          <Grid container spacing={6} >
            <Grid item lg={6} md={6} sm={12} xs={12} style={{ maxWidth: '33%' }}>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>

                <KeyboardDatePicker
                  margin="normal"
                  id="mui-pickers-date"
                  label="Select month/year"
                  format="MM/yyyy"
                  views={"year", "month"}
                  openTo="month"
                  value={date}
                  onChange={this.handleDateChange}
                  KeyboardButtonProps={{
                    'aria-label': 'change date',
                  }}

                />
              </MuiPickersUtilsProvider>



            </Grid>


          </Grid>
          <Button onSubmit={this.handleSubmit} color="primary" variant="contained" type="submit"
            style={{ marginTop: '5px', height: '30px', backgroundColor: '#007bff' }}>
            <Icon>send</Icon>
            <span className="pl-2 capitalize">Search</span>
          </Button>


        </ValidatorForm>

        <br></br>
        {this.state.tableData.length !== 0 &&
          <button
            className="btn btn-primary"
            style={{ marginLeft: '83%', marginTop: '-75px' }}
            onClick={() => this.exportPDF()}>Generate Report</button>
        }
        <br></br>
        {

          this.state.isSubmitted && this.state.tableData &&
          <PaggingTable items={this.state.tableData} />

        }
      </>
    );
  }

}
export default TaskFilter;