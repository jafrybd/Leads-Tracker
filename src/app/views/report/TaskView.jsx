/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Table } from 'reactstrap';
import axios from 'axios'
import myApi from '../../auth/api'
import history from '../../../history'
import Swal from 'sweetalert2'
import localStorageService from 'app/services/localStorageService';
import Moment from 'moment'
import {
  IconButton,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Icon,
  TablePagination
} from "@material-ui/core";
import { FlexibleWidthXYPlot } from 'react-vis';
function TaskView(props) {
  const [item, setItem] = useState([])
  const [taskList, setTaskList] = useState([])
  const [prospectTypes, setProspectTypes] = useState([])

  const [form, setValues] = useState({
    id: 0,
    name: '',
    last: '',
    email: '',
    phone_number: '',
    location: '',
    hobby: '',
    image: '',

  })
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(+event.target.value);
  };

  const onChange = e => {
    setValues({
      ...form,
      [e.target.name]: e.target.value
    })
  }

  const submitFormAdd = e => {
    e.preventDefault()

  }

  const submitFormEdit = e => {
    e.preventDefault()

    axios.post(myApi + '/intraco/lpg/salesPerson/profile/update', {
      'id': form.id, 'name': form.name,
      'email': form.email, 'phone_number': form.phone_number
    }, {

      "headers": {
        'x-access-token': localStorage.getItem("jwt_token")
      }
    })

      .then(response => {
        //console.log(response.data)
        if (!response.data.error) {
          Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        } else {
          Swal.fire({
            icon: "error",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        }
        if (response.data.error === false) {
          props.updateState(form)
        }
        props.toggle()
      })

      .catch(err => console.log(err))
  }


  useEffect(() => {
    // getting prospectType
    axios.get(
      myApi + "/intraco/lpg/prospect_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(prospectType => {
          return {
            id: prospectType.id,
            type: prospectType.type
          };
        });

        setProspectTypes([
          {
            id: '',
            type:
              "Select if you want to change"
          }
        ].concat(
          clientsFromApi)
        )
      })
      .catch(error => {
        console.log(error);

      });

    if (props.item) {
      const { id, name, last, email, phone_number, location, hobby, image } = props.item
      setValues({ id, name, last, email, phone_number, location, hobby, image })


      axios.get(myApi + '/intraco/lpg/task/details/' + props.item.id, {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }).then(response => {


        if (response.data.data) {
          setTaskList(response.data.data)
        }
      })

    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, false)

  const locateValueById = (types, id) => {
    let item = types.find(it => it.id === Number(id))
    return item
  }

  return (
    <div>
      <div className="modal-dialog">
         <div className="modal-content" style={{marginTop : '-35px'}}>
          <div className="modal-body">
            <div style={{ display: FlexibleWidthXYPlot }}>
              <div>
                <span><strong>Date : </strong></span>
                <span className="label label-warning">{Moment(props.item.call_date).format('YYYY-MM-DD')}</span>

              </div>

              <div style={{
                textAlign: "right",
                marginTop: "-20px"
              }}>
                <span><strong>Call Time : </strong></span>
                <span className="label label-warning">{Moment(props.item.call_time, 'hh:mm').format('LT')}</span>

              </div>
            </div>

            <hr />
            <center>
              <span><strong>Client : </strong></span>
              <span className="label label-warning">{props.item.client_info.map((e) => e.name || e.company_title + '_' + e.title)}</span>

            </center>
            <hr />
            <center>
              <span><strong>Discussion Type : </strong></span>
              <span className="label label-warning">{props.item.discussion_type.map(function (item, index) {
                return (
                  <span key={`demo_snap_${index}`}>
                    {(index ? ", " : "") + item.type}
                  </span>
                );
              })}</span>

            </center>
            <hr />
            <center>
              <span><strong>Vehicle Type : </strong></span>
              <span className="label label-warning">{props.item.vehicle_type.map(function (item, index) {
                return (
                  <span key={`demo_snap_${index}`}>
                    {(index ? ", " : "") + item.type}
                  </span>
                );
              })}</span>

            </center>
            <hr />
            <center>
              <span><strong>prospect Type : </strong></span>
              <span className="label label-warning">{props.item.prospect_type_id === 0
                ? "Default"
                : locateValueById(prospectTypes, props.item.prospect_type_id)
                  ? locateValueById(prospectTypes, props.item.prospect_type_id).type
                  : props.item.prospect_type}</span>

            </center>
            <hr />
            <center>
              <span><strong>Lead : </strong></span>
              <span className="label label-warning">{props.item.leads}</span>

            </center>
            <hr />
            <center>
              <span><strong>Status : </strong></span>
              <span className="label label-warning"> {props.item.status && props.item.status === "complete" ? (
                <small className="border-radius-4 bg-secondary text-white px-2 py-2px ">
                  Completed
                </small>
              ) : (
                  <small className="border-radius-4 bg-primary text-white px-2 py-2px ">
                    Incomplete
                  </small>
                )}</span>

            </center>
          </div>

        </div>
      </div>
    </div>
  )
}

export default TaskView
