/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TablePagination
} from "@material-ui/core";

import Moment from 'moment'
import ModalTable from "./ModalTable";
import Swal from 'sweetalert2'
import localStorageService from 'app/services/localStorageService';
import axios from 'axios'
import myApi from '../../auth/api'
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";

export default function PaggingTable(props) {
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);
  const [clients, setClients] = useState([])

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(+event.target.value);
  };


  const items = props.items && props.items.map((item, i) => {
    return (
      <tr key={item.id}>
        <td>{i + 1}</td>
        <td>{item.sales_person}</td>
        <td>{item.discussion_type}</td>
        <td>{item.vehicle_type}</td>
        <td>{item.call_at}</td>
        <td>{Moment(item.call_date).format('YYYY-MM-DD')}</td>
        <td>{item.leads}</td>
      </tr>
    )
  })
  useEffect(() => {
    // getting clientInformation
    axios.get(
      myApi + "/intraco/lpg/client/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(client => {
          return {
            client_id: client.id,
            client_name: client.name
          };
        });

        setClients([
          {
            client_id: '',
            client_name:
              "Select Client"
          }
        ]
          .concat(clientsFromApi)
        );

      })
      .catch(error => {
        //console.log(error);
      });

  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, false)


  const useStyles = makeStyles({
    table: {
      minWidth: 650,
    },
  });
  const classes = useStyles();

  return (
    <TableContainer component={Paper} style={{ boxShadow: '0px 2px 1px -1px rgba(255, 255, 255, 0.06),0px 1px 1px 0px rgba(255, 255, 255, 0.04),0px 1px 3px 0px rgba(255, 255, 255, 0.03)', backgroundColor: '#fafafa' }}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell align="center" className="px-0" style={{ width: '5%' }}>SL.</TableCell>
            <TableCell align="center" className="px-0" style={{ width: '15%' }}>Sales Person</TableCell>
            <TableCell align="center" className="px-0" style={{ width: '15%' }}>Client</TableCell>
            <TableCell align="center" className="px-0" style={{ width: '15%' }}>Client Type</TableCell>
            <TableCell align="center" className="px-0" style={{ width: '15%' }}>Call Date</TableCell>
            <TableCell align="center" className="px-0" style={{ width: '10%' }}>Lead</TableCell>
            <TableCell align="center" className="px-0" style={{ width: '13%' }}>Status</TableCell>
            <TableCell align="center" className="px-0" style={{ width: '12%' }}>Details</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.items && props.items
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((item, id) => (
              <TableRow key={id}>
                <TableCell className="px-0 capitalize" align="center">
                  {page * rowsPerPage + id + 1}
                </TableCell>
                <TableCell className="px-0 capitalize" align="center">
                  {item.sales_person}
                </TableCell>
                <TableCell className="px-0 capitalize" align="center">
                  {item.client_info.map((e) => e.name || e.company_title + '_' + e.title)}
                </TableCell>
                <TableCell className="px-0 capitalize" align="center">
                  {item.client_type_id === 0 ? 'Corporate' : 'Individual'}
                </TableCell>
                <TableCell className="px-0 capitalize" align="center">
                  {Moment(item.call_date).format('YYYY-MM-DD')}
                </TableCell>
                <TableCell className="px-0 capitalize" align="center">
                  {item.leads}
                </TableCell>
                <TableCell className="px-0 capitalize" align="center">
                  {item.status && item.status === "complete" ? (
                    <small className="border-radius-4 bg-secondary text-white px-2 py-2px ">
                      Completed
                    </small>
                  ) : (
                      <small className="border-radius-4 bg-primary text-white px-2 py-2px ">
                        Incomplete
                      </small>
                    )}
                </TableCell>
                <TableCell className="px-0 capitalize" align="center">
                  <ModalTable
                    style={{ maxWidth: "700px!important" }}
                    buttonLabel="View"
                    item={item}
                  />
                </TableCell>

              </TableRow>
            ))}
        </TableBody>
      </Table>

      <TablePagination
        style={{
          maxWidth: 400,
          overflowX: "hidden",
          padding: "0px!important",
          display: "contents",
        }}
        className="px-4"
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={items && items.length}
        rowsPerPage={rowsPerPage}
        page={page}
        backIconButtonProps={{
          "aria-label": "Previous Page"
        }}
        nextIconButtonProps={{
          "aria-label": "Next Page"
        }}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </TableContainer>

  );
}
