import React, { Component } from "react";
import { Breadcrumb } from "matx";
import AppointmentFilter from "./appointmentFilter";

class Filter extends Component {
  render() {
    return (
      <div className="m-sm-30">
        <div  className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: "Report", path: "/report/appointment" },
              { name: "Appointment List" }
            ]}
          />
        </div>
        <AppointmentFilter />
      </div>
    );
  }
}

export default Filter;
