import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "reactstrap";
import DeactiveDataTable from "./newTable";
import axios from "axios";
import { Breadcrumb } from "matx";
import myApi from "../../auth/api";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import { Button, Icon, Grid } from "@material-ui/core";
import Select from "react-select";
import { ValidatorForm } from "react-material-ui-form-validator";
import localStorageService from "app/services/localStorageService";

function Sa(props) {
  const [name, setNames] = useState("");
  const [selectedCompany, setSelectedCompany] = useState("");
  const [selectedSisterConcern, setSelectedSisterConcern] = useState("");
  const [company, setCompany] = useState([]);
  const [sisterConcernList, setSisterConcernList] = useState([]);
  const [items, setItems] = useState([]);

  const getItems = () => {
    axios({
      method: "GET",
      url: myApi + "/intraco/lpg/company/allList",
      headers: {
        "x-access-token": localStorage.getItem("jwt_token"),
      },
    })
      .then((response) => {
        console.log(response.data.data);
        setItems(Array.isArray(response.data.data)?response.data.data:[]);
      })
      .catch((error) => {
        console.log(error);
      });

    // getting clientInformation
    axios
      .get(myApi + "/intraco/lpg/company/allList", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        console.log(res);
        let companies = res.data.data.map((c) => {
          return {
            value: c.id,
            label: c.title,
          };
        });

        setCompany(
          [
            {
              value: 0,
              label: "Select All Company",
            },
          ].concat(companies)
        );
      })
      .catch((error) => {
        console.log(error);
      });

    // getting clientInformation
    axios
      .get(myApi + "/intraco/lpg/company_sister_concerns/allList", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        console.log(res);
        let companies = res.data.data.map((c) => {
          return {
            value: c.id,
            label: c.sister_concern,
          };
        });

        setSisterConcernList(
          [
            {
              value: 0,
              label: "Select All Sister Concern",
            },
          ].concat(companies)
        );
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const addItemToState = (item) => {
    setItems([...items, item]);
    setNames([...name, name]);
  };

  const updateState = (item) => {
    const itemIndex = items.findIndex((data) => data.id === item.id);
    const newArray = [
      ...items.slice(0, itemIndex),
      item,
      ...items.slice(itemIndex + 1),
    ];
    setItems(newArray);
  };

  const deleteItemFromState = (id) => {
    const updatedItems = items.filter((item) => item.id !== id);
    setItems(updatedItems);
  };

  useEffect(() => {
    getItems();
  }, []);

  const onSelectChangeCompany = (label) => {
    if (label !== null) {
      setSelectedCompany(label.label);
    }
    if (label === null) {
      setSelectedCompany("");
    }
  };

  const onSelectChangeSisterConcern = (label) => {
    if (label !== null) {
      setSelectedSisterConcern(label.label);
    }
    if (label === null) {
      setSelectedSisterConcern("");
    }
  };

  const handleSubmit = () => {
    console.log(selectedSisterConcern, selectedCompany);
    if (selectedCompany === "Select All Company") {
      axios
        .get(
          myApi + "/intraco/lpg/company/allList",

          {
            headers: {
              "x-access-token": localStorageService.getItem("auth_user").token, // override instance defaults
            },
          }
        )
        .then((res) => {
          if (res.data.data) {
            setItems(res.data.data);
          }
        });
    } else {
      axios
        .post(
          myApi + "/intraco/lpg/company/search",
          {
            title: selectedCompany,
          },
          {
            headers: {
              "x-access-token": localStorageService.getItem("auth_user").token, // override instance defaults
            },
          }
        )
        .then((res) => {
          if (res.data.data) {
            setItems(res.data.data);
          }
        });
    }
  };

  return (
    <div className="m-sm-30">
      <div className="mb-sm-30">
        <Container className="App">
          <Row>
            <Col>
              <Breadcrumb
                routeSegments={[
                  { name: "Company", path: "/company/edit" },
                  { name: "Details" },
                ]}
              />
              <h1 style={{ margin: "20px 0" }}></h1>
            </Col>
          </Row>

          <br></br>
          <Row>
            <Col>
              <div>
                <ValidatorForm onSubmit={handleSubmit}>
                  <Grid container spacing={6}>
                    <Grid
                      item
                      lg={6}
                      md={6}
                      sm={12}
                      xs={12}
                      
                    >
                      {
                        <Select
                          style={{
                            width: "100%",
                            padding: "5px",
                            backgroundColor: "white",
                            color: "#444",
                            borderBottomColor: "#000000",
                            textAlign: "left",
                            marginTop: "40px",
                          }}
                          isClearable="true"
                          name="selectedCompany"
                          options={company}
                          className="basic-multi-select"
                          classNamePrefix="select"
                          onChange={onSelectChangeCompany}
                        />
                      }

                      <Button
                        onSubmit={handleSubmit}
                        color="primary"
                        variant="contained"
                        type="submit"
                        style={{ height: "30px", marginTop: "20px" }}
                      >
                        <Icon>send</Icon>
                        <span className="pl-2 capitalize">Search</span>
                      </Button>
                    </Grid>
                  </Grid>
                </ValidatorForm>
              </div>
            </Col>
          </Row>

          <Row>
            <Col>
              <DeactiveDataTable
                items={items}
                updateState={updateState}
                deleteItemFromState={deleteItemFromState}
              />
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
}
// }

export default Sa;
