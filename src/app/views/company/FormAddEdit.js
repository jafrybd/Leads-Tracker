import React, { useState, useEffect } from "react";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import axios from "axios";
import myApi from "../../auth/api";
import history from "../../../history";
import Swal from "sweetalert2";
import localStorageService from "app/services/localStorageService";
function AddEditForm(props) {
  const [item, setItem] = useState([]);
  const [form, setValues] = useState({
    id: 0,
    title: "",
    active_status: 0,
  });

  const onChange = (e) => {
    setValues({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const submitFormAdd = (e) => {
    e.preventDefault();
  };

  const submitFormEdit = (e) => {
    e.preventDefault();

    axios
      .post(
        myApi + "/intraco/lpg/company/update",
        {
          id: form.id,
          title: form.title,
        },
        {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token,
          },
        }
      )

      .then((response) => {
        Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        if (response.data.error === false) {
          // props.updateState(form);
         
          props.updateState(form)
        }
        props.toggle();
      })

      .catch((err) => console.log(err));
  };

  useEffect(() => {
    if (props.item) {
      const {
        id,
        title,
        last,
        email,
        phone_number,
        location,
        hobby,
        active_status
      } = props.item;
      setValues({ id, title, last, email, phone_number, location, hobby,active_status });
    }
  }, false);

  return (
    <Form onSubmit={props.item ? submitFormEdit : submitFormAdd}>
      <FormGroup>
        <Label for="name">title</Label>
        <Input
          title="text"
          name="title"
          id="title"
          onChange={onChange}
          value={form.title === null ? "" : form.title}
          minLength="3"
          required
        />
      </FormGroup>

      <Button>Submit</Button>
    </Form>
  );
}

export default AddEditForm;
