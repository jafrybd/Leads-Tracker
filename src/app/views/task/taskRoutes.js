/* eslint-disable no-dupe-keys */
import React from "react";
import { authRoles } from "../../auth/authRoles";

const TaskRoutes = [
  {
    path: "/task/create",
    component: React.lazy(() => import("./createTask")),
    auth: authRoles.guest,
  },
  {
    path: "/task/edit",
    component: React.lazy(() => import("./taskManage")),

    auth: authRoles.guest,
  },
  {
    path: "/task/update/:id",
    component: React.lazy(() => import("./update/updateClient")),
    auth: authRoles.guest,
  },
  {
    path: "/task/filter",
    component: React.lazy(() => import("./taskFilter")),
    auth: authRoles.guest,
  },
  {
    path: "/task/search",
    component: React.lazy(() => import("./FilterAdmin")),
    auth: authRoles.admin,
    auth: authRoles.editor,
  },
];

export default TaskRoutes;
