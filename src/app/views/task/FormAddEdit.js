import React, { useState, useEffect, useCallback } from "react";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";

import axios from "axios";
import myApi from "../../auth/api";
import Swal from "sweetalert2";
import localStorageService from "app/services/localStorageService";
import Moment from "moment";

function AddEditForm(props) {
  const [value, setValue] = useState(
    Moment(props.item.call_time, "HH:mm").format("HH:mm")
  );

  const [form, setValues] = useState({
    id: "",
    name: "",
    primary_phone_no: "",
    secondary_phone_no: "",
    email: "",
    address: "",

    discussion_type_id: "",

    call_time_type_id: "",

    vehicle_type_id: "",
    lead_id: "",
    prospect_type_id: "",
    client_id: "",
    discussion_type: "",
    vehicle_type: "",
    call_at: "",
    leads: "",
    call_time: null,

    validationError: "",
    date: "",
    call_date: null,
    time: null,
    newTime: Moment(props.item.call_time, "HH:mm").format("HH:mm"),
  });

  const onChange = (e) => {
    setValues({
      ...form,
      [e.target.name]: e.target.value,
    });
  };
  const handleDialogTimeChange = (newValue) => {
    setValue(Moment(props.item.call_time, "HH:mm").format("HH:mm"));
  };

  const submitFormAdd = (e) => {};

  const handleTimeChange = (newTime) => {
    setValues({
      time: newTime,
      newTime: newTime,
    });
  };

  const submitFormEdit = (e) => {
    e.preventDefault();

    let callDate = Moment(form.call_date).format("YYYY-MM-DD");
    let callDateProps = Moment(props.item.call_date).format("YYYY-MM-DD");
    axios
      .post(
        myApi + "/intraco/lpg/task/update",
        {
          id: form.id || props.item.id,
          client_id: form.client_id || props.item.client_id,
          discussion_type:
            Number(form.discussion_type_id) ||
            Number(props.item.discussion_type_id),

          call_time:
            form.newTime === null
              ? Moment(props.item.call_time, "HH:mm").format("HH:mm")
              : Moment(form.newTime, "HH:mm").format("HH:mm"),
          vehicle_type:
            Number(form.vehicle_type_id) || Number(props.item.vehicle_type_id),
          call_date: form.call_date === null ? callDateProps : callDate,
          lead: form.lead_id || props.item.lead_id,
          prospect_type: form.prospect_type_id || props.item.prospect_type_id,
        },
        {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token,
          },
        }
      )

      .then((res) => {
       Swal.fire({
            icon: "success",
            title: res.data.message,
            showConfirmButton: false,
            timer: 1000
          });
        if (res.data.error === false) {
          props.updateState({
            id: form.id || props.item.id,
            client_id: form.client_id || props.item.client_id,
            discussion_type_id:
              form.discussion_type_id || props.item.discussion_type_id,
            vehicle_type_id: form.vehicle_type_id || props.item.vehicle_type_id,
            call_date: form.call_date === null ? callDateProps : callDate,
            lead_id: form.lead_id || props.item.lead_id,
            prospect_type_id:
              form.prospect_type_id || props.item.prospect_type_id,
            call_time: form.newTime || props.item.call_time,
          });
        }
        props.toggle();
      })

      .catch((err) => console.log(err));
  };

  return (
    <Form onSubmit={props.item ? submitFormEdit : submitFormAdd}>
      <FormGroup>
        <Label for="discussion_type_id">Discussion Type</Label>
        <select
          style={{
            width: "100%",
            padding: "5px",
            backgroundColor: "white",
            color: "#444",
            borderBottomColor: "#949494",
            textAlign: "left",
            marginTop: "20px",
          }}
          className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
          name="discussion_type_id"
          value={form.discussion_type_id || props.item.discussion_type_id}
          onChange={onChange}
        >
          {props.discussionTypes.map((discussionType) => (
            <option key={discussionType.id} value={discussionType.id}>
              {discussionType.type}
            </option>
          ))}
        </select>
      </FormGroup>
      <FormGroup>
        <Label for="client_id">Client</Label>
        <select
          style={{
            width: "100%",
            padding: "5px",
            backgroundColor: "white",
            color: "#444",
            borderBottomColor: "#949494",
            textAlign: "left",
            marginTop: "20px",
          }}
          className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
          name="client_id"
          value={form.client_id || props.item.client_id}
          onChange={onChange}
        >
          {props.clientTypes.map((client) => (
            <option key={client.id} value={client.id}>
              {client.name}
            </option>
          ))}
        </select>
      </FormGroup>
      <FormGroup>
        <Label for="vehicle_type_id">Vehicle Type</Label>
        <select
          style={{
            width: "100%",
            padding: "5px",
            backgroundColor: "white",
            color: "#444",
            borderBottomColor: "#949494",
            textAlign: "left",
            marginTop: "20px",
          }}
          className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
          name="vehicle_type_id"
          value={form.vehicle_type_id || props.item.vehicle_type_id}
          onChange={onChange}
        >
          {props.vehicleTypes.map((vehicleType) => (
            <option key={vehicleType.id} value={vehicleType.id}>
              {vehicleType.type}
            </option>
          ))}
        </select>
      </FormGroup>
      <FormGroup>
        <Label for="call_time_type_id">Call Time</Label>

        <div>
          <Input
            type="text"
            name="newTime"
            id="call_time"
            onChange={onChange}
            value={form.newTime}
            required
          />
        </div>
      </FormGroup>
      <FormGroup>
        <Label for="discussion_type_id">Prospect type</Label>
        <select
          style={{
            width: "100%",
            padding: "5px",
            backgroundColor: "white",
            color: "#444",
            borderBottomColor: "#949494",
            textAlign: "left",
            marginTop: "20px",
          }}
          className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
          name="prospect_type_id"
          value={form.prospect_type_id || props.item.prospect_type_id}
          onChange={onChange}
        >
          {props.prospectTypes.map((prospectType) => (
            <option key={prospectType.id} value={prospectType.id}>
              {prospectType.type}
            </option>
          ))}
        </select>
      </FormGroup>
      <FormGroup>
        <Label for="discussion_type_id">Lead</Label>
        <select
          style={{
            width: "100%",
            padding: "5px",
            backgroundColor: "white",
            color: "#444",
            borderBottomColor: "#949494",
            textAlign: "left",
            marginTop: "20px",
          }}
          className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
          name="lead_id"
          value={form.lead_id || props.item.lead_id}
          onChange={onChange}
        >
          {props.lead_id.map((lead) => (
            <option key={lead.id} value={lead.id}>
              {lead.name}
            </option>
          ))}
        </select>
      </FormGroup>

      <FormGroup>
        <Label for="address">Date</Label>

        <Input
          type="date"
          name="call_date"
          id="call_date"
          onChange={onChange}
          value={
            form.call_date === null
              ? Moment(props.item.call_date).format("YYYY-MM-DD")
              : form.call_date
          }
          required
        />
      </FormGroup>
      <Button>Submit</Button>
    </Form>
  );
}

export default AddEditForm;
