import React, { Component } from "react";
import { Breadcrumb } from "matx";
import TaskEX from './taskEx'
import {SimpleCard} from 'matx'

class taskExchange extends Component {
  render() {
    return (
      <div className="m-sm-30">
        <div  className="mb-sm-30">
          <Breadcrumb
            routeSegments={[
              { name: "Task", path: "/task/exchange" },
              { name: "Task Exchange" }
            ]}
          />
        </div>
        
      <SimpleCard title="">
        <TaskEX />
      </SimpleCard>
    </div>
    // </div>
    );
  }
}

export default taskExchange;
