import React, { Component } from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import axios from 'axios'
import myApi from '../../auth/api'
import Swal from 'sweetalert2'

import {
  Button,
  Icon,
  Grid,

} from "@material-ui/core";

import "date-fns";

import history from '../../../history'
class CreateForm extends Component {

  constructor() {
    super();
    this.state = {
      oldPassword: "",
      firstName: "",
      email: "",
      date: new Date(),
      creditCard: "",
      phone: "",
      password: "",
      confirmPassword: "",
      gender: "",
      agreement: ""
    };
  }

  componentDidMount() {
    // custom rule will have name 'isPasswordMatch'
    ValidatorForm.addValidationRule("isPasswordMatch", value => {
      if (value !== this.state.password) {
        return false;
      }
      return true;
    });
  }

  componentWillUnmount() {
    // remove rule when it is not needed
    ValidatorForm.removeValidationRule("isPasswordMatch");
  }

  handleSubmit = event => {
    axios.post(myApi + '/intraco/lpg/admin/superAdmin/profile/passwordChange',
      { 'id': JSON.parse(localStorage.getItem("auth_user")).userId, 'old_password': this.state.oldPassword, 'new_password': this.state.password },
      {
        headers: {

          'x-access-token': localStorage.getItem("jwt_token")
        },
      }
    )
      .then(res => {

        if (res.data.message) {
          if (res.data.error) {
            Swal.fire({
              icon: "error",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
            // throw new Error(res.data.message)
          }
          else {
            Swal.fire({
              icon: "success",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          }

          if (res.data.error === false) {
            this.setState({
              oldPassword: "",
              firstName: "",
              email: "",
              date: new Date(),
              creditCard: "",
              phone: "",
              password: "",
              confirmPassword: "",
              gender: "",
              agreement: ""
            })
          }
        }

      })
  }

  handleChange = event => {
    event.persist();
    this.setState({ [event.target.name]: event.target.value });
  };

  handleDateChange = date => {
    // //console.log(date);

    this.setState({ date });
  };

  render() {
    let {
      oldPassword,
      password,
      confirmPassword,
    } = this.state;
    return (
      <div>
        <ValidatorForm
          ref="form"
          onSubmit={this.handleSubmit}
          onError={errors => null}
        >
          <Grid container spacing={6}>
            <Grid item lg={6} md={6} sm={12} xs={12}>
              <TextValidator
                autocomplete='off'
                className="mb-4 w-full"
                label="Current Password"
                onChange={this.handleChange}
                type="password"
                name="oldPassword"
                value={oldPassword}
                errorMessages={["this field is required"]}
              />


              <TextValidator
                autocomplete='password'
                className="mb-4 w-full"
                label="Password (Min Length : 6)"
                onChange={this.handleChange}
                name="password"
                type="password"
                value={password}
                validators={["required", "isPasswordMatch"]}
                errorMessages={["this field is required"]}
              />
              <TextValidator
                autoComplete='confirmPassword'
                className="mb-4 w-full"
                label="Confirm Password"
                onChange={this.handleChange}
                name="confirmPassword"
                type="password"
                value={confirmPassword}
                validators={["required", "isPasswordMatch"]}
                errorMessages={[
                  "this field is required",
                  "password didn't match"
                ]}
              />

            </Grid>
          </Grid>
          <Button color="primary" variant="contained" type="submit">
            <Icon>send</Icon>
            <span className="pl-2 capitalize">Update</span>
          </Button>
        </ValidatorForm>
      </div>
    );
  }
}

export default CreateForm;
