/* eslint-disable no-dupe-keys */
import React from "react";
import { authRoles } from "../../auth/authRoles";

const changePassword = [
  {
    path: "/password/edit",
    component: React.lazy(() => import("./createChangePassword")),
    auth: authRoles.admin
  },
  {
    path: "/subAdmin/passwordChange",
    component: React.lazy(() => import("./subAdminPasswordChanged")),
    auth: authRoles.editor
  },
  {
    path: "/salesperson/passwordChange",
    component: React.lazy(() => import("./salespersonPasswordChange")),
    auth: authRoles.admin,
    auth: authRoles.guest
  }
];

export default changePassword;
