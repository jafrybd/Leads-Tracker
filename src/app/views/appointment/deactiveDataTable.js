import React from "react";
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TablePagination,
} from "@material-ui/core";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import Moment from "moment";
import TaskModal from "../report/taskModal";

export default function BasicDatatable(props) {
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
  };

  const useStyles = makeStyles({
    table: {
      minWidth: 650,
    },
  });
  const classes = useStyles();
  return (
    <>
      <TableContainer
        component={Paper}
        style={{
          boxShadow:
            "0px 2px 1px -1px rgba(255, 255, 255, 0.06),0px 1px 1px 0px rgba(255, 255, 255, 0.04),0px 1px 3px 0px rgba(255, 255, 255, 0.03)",
          backgroundColor: "#fafafa",
        }}
      >
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "5%" }}
              >
                #
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "15%" }}
              >
                Appointment Date
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "13%" }}
              >
                Sales Person
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "13%" }}
              >
                Client
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "10%" }}
              >
                Client Type
              </TableCell>
              <TableCell
                align="center"
                className="px-0"
                style={{ width: "10%" }}
              >
                Details
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {props.items &&
              props.items
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((item, id) => (
                  <TableRow key={id}>
                    <TableCell align="center" className="px-0 capitalize">
                      {page * rowsPerPage + id + 1}
                    </TableCell>
                    <TableCell align="center" className="px-0 capitalize">
                      {Moment(item.appointment_date).format("YYYY-MM-DD")}
                    </TableCell>
                    <TableCell className="px-0 capitalize" align="center">
                      {item.sales_person}
                    </TableCell>
                    <TableCell className="px-0 capitalize" align="center">
                      {item.client_info &&
                        item.client_info.map(
                          (e) => e.name || e.company_title + "_" + e.title
                        )}
                    </TableCell>
                    <TableCell className="px-0 capitalize" align="center">
                      {item.client_type_id === 0 ? "Corporate" : "Individual"}
                    </TableCell>
                    <TableCell className="px-0 capitalize" align="center">
                      <TaskModal
                        style={{ maxWidth: "700px!important" }}
                        buttonLabel="View"
                        item={item}
                      />
                    </TableCell>
                  </TableRow>
                ))}
          </TableBody>
        </Table>
      </TableContainer>

      <TablePagination
        style={{
          maxWidth: 400,
          overflowX: "hidden",
          padding: "0px!important",
          display: "contents",
        }}
        className="px-4"
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={props.items && props.items.length}
        rowsPerPage={rowsPerPage}
        page={page}
        backIconButtonProps={{
          "aria-label": "Previous Page",
        }}
        nextIconButtonProps={{
          "aria-label": "Next Page",
        }}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </>
  );
}
