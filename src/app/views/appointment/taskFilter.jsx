import React from "react";
import "react-dates/initialize";
import { DateRangePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";
import MUIDataTable from "mui-datatables";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import Loader from 'react-loader-spinner'
import {

  Button,
  Icon,
  Grid,
  Radio,
  RadioGroup,
  FormControlLabel,
  Checkbox
} from "@material-ui/core";
import axios from "axios";
import myApi from '../../auth/api'
import localStorageService from 'app/services/localStorageService'
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import DeactiveDataTable from "./deactiveDataTable";

class TaskFilter extends React.Component {
  constructor(props) {
    super(props)
    const tableData = this.state.tableData

  }
  state = {
    clients: [],
    selectedClient: "",
    discussionTypes: [],
    selectedDiscussionType: "",
    CallTimeTypes: [],
    selectedCallTimeType: "",
    VehicleTypes: [],
    selectedVehicleType: "",
    Leads: [],
    selectedLead: "",
    // config : 
    columns: ['discussion_type', 'vehicle_type', 'sales_person', 'call_at', 'leads', 'call_date'],
    options: {
      filter: true,
      filterType: "dropdown",
      // responsive,
      // tableBodyHeight,
      // tableBodyMaxHeight
    },
    tableData: [],
    SalesPersons: [],
    selectedSalesPerson: '',
    startDate: null,
    endDate: null,
    endDateFormatted: null,
    startDateFormatted: null,
    isSubmitted: false
  };

  componentDidMount() {
    axios({
      "method": "GET",
      "url": myApi + "/intraco/lpg/appointment/deActiveList",
      "headers": {
        'x-access-token': localStorageService.getItem('auth_user').token
      }
    })
      .then((response) => {
        // console.log(token)
        console.log(response.data.data)
        // this.setItems(response.data.data)
        this.setState({
          isSubmitted: true,
          tableData: response.data.data
        })

      })
      .catch((error) => {
        console.log(error)
      })


    // getting clientInformation
    axios.get(
      myApi + "/intraco/lpg/client/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(client => {
          return {
            id: client.id,
            name: client.name
          };
        });

        this.setState({
          clients: [
            {
              id: '',
              name:
                "Select Client"
            }
          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting clientInformation
    axios.get(
      myApi + "/intraco/lpg/salesPerson/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(client => {
          return {
            id: client.id,
            name: client.name
          };
        });

        this.setState({
          SalesPersons: [
            {
              id: '',
              name:
                "Select Salesperson"
            }
          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });


  }
  handleChange = event => {
    event.persist();
    this.setState({ [event.target.name]: event.target.value });
  };

  hundleDateChange(startDate, endDate) {
    // this.setTableData(() => {
    //   tableData
    // })
    this.setState(() => ({

      endDate,
      startDate,
    }));
    if (startDate != null) {
      this.setState(() => ({
        startDateFormatted: startDate.format("YYYY-MM-DD"),
      }));
    }
    if (startDate == null) {
      this.setState(() => ({
        startDateFormatted: '',
      }));
    }
    if (endDate != null) {
      this.setState(() => ({
        endDateFormatted: endDate.format("YYYY-MM-DD"),
      }));
    }
    if (endDate == null) {
      this.setState(() => ({
        endDateFormatted: '',
      }));
    }
  }

  handleSubmit = () => {
    let reqData = {
      "client_id": this.state.selectedClient,
      "sales_person_id": this.state.selectedSalesPerson,
      "from_date": this.state.startDateFormatted,
      "to_date": this.state.endDateFormatted
    }
    if (reqData.to_date === '' || reqData.to_date === null || reqData.to_date === 'null') {
      delete reqData.to_date
    }
    if (reqData.from_date === '' || reqData.from_date === null || reqData.from_date === 'null') {
      delete reqData.from_date
    }
    axios.post(myApi + '/intraco/lpg/appointment/searchDeactive', reqData, {
      headers: {
        'x-access-token': localStorageService.getItem('auth_user').token // override instance defaults
      },
    })
      .then(res => {
        console.log(res.data)
        if (res.data.data) {
          this.setState({
            isSubmitted: true,
            tableData: res.data.data
          })

          console.log(this.state.tableData)
        }
      })
  }

  render() {
    return (
      <>
        <ValidatorForm
          ref="form"
          onSubmit={this.handleSubmit}
        // onError={errors => null}
        >
          <Grid container spacing={6} >
            <Grid item lg={6} md={6} sm={12} xs={12} style={{ maxWidth: '33%' }}>
              {/* <style dangerouslySetInnerHTML={{__html: "\nselect::-ms-expand {\n    display: none;\n}\n" }} /> */}
              <select style={{ width: '100%', padding: '5px', backgroundColor: 'white', color: '#444', borderBottomColor: '#949494', textAlign: 'left', marginTop: '20px' }}
                className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
                name="selectedClient"
                value={this.state.selectedClient}
                onChange={e =>
                  this.setState({
                    selectedClient: e.target.value,
                    validationError:
                      e.target.value === ""
                        ? "You must select Client type"
                        : undefined
                  })
                }
              >
                {this.state.clients.map(client => (
                  <option
                    key={client.id}
                    value={client.id}
                  >
                    {client.name}
                  </option>
                ))}
              </select>


              <select style={{ width: '100%', padding: '5px', backgroundColor: 'white', color: '#444', borderBottomColor: '#949494', textAlign: 'left', marginTop: '20px' }}
                className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
                name="selectedSalesPerson"
                value={this.state.selectedSalesPerson}
                onChange={e =>
                  this.setState({
                    selectedSalesPerson: e.target.value,
                    validationError:
                      e.target.value === ""
                        ? "selectedSalesPerson"
                        : ""
                  })
                }
              >
                {this.state.SalesPersons.map(salesperson => (
                  <option
                    key={salesperson.id}
                    value={salesperson.id}
                  >
                    {salesperson.name}
                  </option>
                ))}
              </select>


            </Grid>

            <Grid item lg={6} md={6} sm={12} xs={12} style={{ maxWidth: '33%' }}>
              <div style={{ marginTop: '20px' }}>
                <DateRangePicker
                  isOutsideRange={() => false}
                  startDate={this.state.startDate}
                  startDateId="start_date_id"
                  endDate={this.state.endDate}
                  endDateId="end_date_id"
                  onDatesChange={({ startDate, endDate }) =>
                    this.hundleDateChange(startDate, endDate)
                  }
                  focusedInput={this.state.focusedInput}
                  onFocusChange={(focusedInput) => this.setState({ focusedInput })}
                />


              </div>
              <Button onSubmit={this.handleSubmit} color="primary" variant="contained" type="submit"
                style={{ height: '30px', marginTop: '20px' }}>
                <Icon>send</Icon>
                <span className="pl-2 capitalize">Search</span>
              </Button>
            </Grid>



          </Grid>

        </ValidatorForm>

        {

          this.state.isSubmitted && this.state.tableData &&
          <DeactiveDataTable items={this.state.tableData} />

        }
      </>
    );
  }
}
export default TaskFilter;