/* eslint-disable no-dupe-keys */
import { auth } from "firebase";
import React from "react";
import { authRoles } from "../../auth/authRoles";

const AppointmentRoutes = [
  {
    path: "/appointment/create",
    component: React.lazy(() => import("./createAppointment")),
    auth: authRoles.guest
  },
  {
    path: "/appointment/deactive",
    component: React.lazy(() => import("./Filter")),
    auth: authRoles.admin,
    auth: authRoles.editor
  }

  ,
  {
    path: "/appointment/search/active",
    component: React.lazy(() => import("./FilterActive")),
    auth: authRoles.admin,
    auth: authRoles.editor,
    
  },
  {
    path: "/appointment/manage",
    component: React.lazy(() => import("./update/corporateListSaler")),
    auth: authRoles.guest
  },
  {
    path: "/appointment/edit/:id",
    component: React.lazy(() => import("./edit/createClient")),
    auth: authRoles.guest,
  },
];

export default AppointmentRoutes;
