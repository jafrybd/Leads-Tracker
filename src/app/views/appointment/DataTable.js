import React, { useEffect, useState, useCallback } from "react";
import { Table, Button } from "reactstrap";
import ModalForm from "./Modal";
import axios from "axios";
import myApi from "../../auth/api";
import Swal from "sweetalert2";
import localStorageService from "app/services/localStorageService";
import Moment from "moment";
function DataTable(props) {
  const [clientTypes, setClientTypes] = useState({});
  const [discussionTypes, setDiscussionTypes] = useState({});
  const [clients_corporate, setClientCorporateTypes] = useState({});


  useEffect(
    useCallback(() => {

      // getting Sister Concern List
      axios.get(
        myApi + "/intraco/lpg/appointment/create/data",
        {
          headers: {
            'x-access-token': localStorageService.getItem('auth_user').token
          }
        }
      )
        .then(res => {
          console.log(res)
          let clientsFromApi = res.data.data && res.data.data.corporateClient.map(cl => {
            return {
              id: cl.id,
              name: cl.company_title + '_' + cl.company_sister_concerns_title
            };
          });

          setClientCorporateTypes(
            [
              {
                id: "",
                name: "Select if you want to change",
              },
            ]
              .concat(clientsFromApi)
          );
          console.log(clients_corporate)
        })
        .catch(error => {
          console.log(error);
        });
      // getting discussiontype
      axios
        .get(myApi + "/intraco/lpg/discussion_type/allList", {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token,
          },
        })
        .then((res) => {
          let clientsFromApi = res.data.data.map((client) => {
            return {
              id: client.id,
              type: client.type,
            };
          });

          setDiscussionTypes(
            [
              {
                id: "",
                type: "Select if you want to change",
              },
            ].concat(clientsFromApi)
          );
          //console.log(clientTypes);
        })
        .catch((error) => {
        });

      // getting clientType
      axios
        .get(myApi + "/intraco/lpg/client/list", {
          headers: {
            "x-access-token": localStorageService.getItem("auth_user").token,
          },
        })
        .then((res) => {
          let clientsFromApi = res.data.data.map((client) => {
            return {
              id: client.id,
              name: client.name,
            };
          });

          setClientTypes(
            [
              {
                id: "",
                name: "Select if you want to change",
              },
            ].concat(clientsFromApi)
          );
        })
        .catch((error) => {
        });
    }, [clientTypes]),
    []
  );

  const resetPassword = (id) => {
    let confirmReset = window.confirm("Reset Account Password?");
    if (confirmReset) {
      axios
        .post(
          myApi + "/intraco/lpg/salesPerson/profile/resetPassword",
          { id: id },
          {
            headers: {
              "x-access-token": localStorage.getItem("jwt_token"),
            },
          }
        )
        .then((response) => {

          Swal.fire({
            icon: "success",
            title: response.data.message,
            showConfirmButton: false,
            timer: 1500
          });
        })

        .catch((err) => console.log(err));
    }
  };
  const deleteItem = (id) => {

    Swal.fire({
      title: "Warning!",
      text: "Give a reason , why you want to delete !",
      input: "text",
      showCancelButton: true,
    }).then((result) => {
      if (result.value) {
        axios
          .post(
            myApi + "/intraco/lpg/appointment/delete",
            { id: id, reason_for_delete: result.value },
            {
              headers: {
                "x-access-token": localStorageService.getItem("auth_user")
                  .token,
              },
            }
          )
          .then((response) => {

          })
          .then((item) => {
            props.deleteItemFromState(id);
            Swal.fire("Deleted Successfully !");
          })
          .catch((err) => console.log(err));
      }
    });
  };

  const locateValueById = (types, id) => {
    let item = types && types.find((it) => it.id === Number(id));
    return item;
  };

  const items = props.items && props.items.map((item, i) => {
    console.log(item.client_info, '1')
    return (
      <tr key={item.id}>
        <th scope="row">{i + 1}</th>
        <td>{Moment(item.appointment_date).format("YYYY-MM-DD")}</td>
        <td>
          {item.client_info && (item.client_info.map(e => e.name)).toString() ?
            (locateValueById(item.client_info, item.client_info && Number(item.client_info.map((e) => e.id).toString()))
              && locateValueById(item.client_info, item.client_info && Number(item.client_info.map((e) => e.id).toString())).name)
            :
            ((locateValueById(clients_corporate, item.client_info && Number(item.client_info.map((e) => e.id).toString()))
              && locateValueById(clients_corporate, item.client_info && Number(item.client_info.map((e) => e.id).toString())).name))
          }


        </td>
        <td>{locateValueById(discussionTypes, item.discussion_type_id) &&
          locateValueById(discussionTypes, item.discussion_type_id).type}</td>

        <td>{item.car_number}</td>
        <td>{item.driver_name}</td>

        <td>{item.service_details}</td>
        <td>
          {" "}
          {item.status && item.status == "complete" ? (
            <small className="border-radius-4 bg-secondary text-white px-2 py-2px ">
              Completed
            </small>
          ) : (
              <small className="border-radius-4 bg-primary text-white px-2 py-2px ">
                Incomplete
              </small>
            )}
        </td>

        <td>
          <div style={{ width: "150px" }}>
            <ModalForm
              buttonLabel="Edit"
              item={item}
              updateState={props.updateState}
            />{" "}
            <Button color="danger" onClick={() => deleteItem(item.id)}>
              Delete
            </Button>
          </div>
        </td>
      </tr>
    );
  });

  return (
    <Table responsive hover>
      <thead>
        <tr>
          <th>ID</th>
          <th>Appointment Date</th>
          <th>Client</th>
          <th>Discussion Type</th>
          <th>Car Number</th>

          <th>Driver Name</th>
          <th>Service</th>
          <th>Status</th>

          <th>Action</th>
        </tr>
      </thead>
      <tbody>{items}</tbody>
    </Table>
  );
}

export default DataTable;
