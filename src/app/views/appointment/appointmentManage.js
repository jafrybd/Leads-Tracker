import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "reactstrap";
import ModalForm from "./Modal";
import DataTable from "./DataTable";
import { CSVLink } from "react-csv";
import axios from "axios";
import { Breadcrumb } from "matx";
import myApi from "../../auth/api";
import localStorageService from "app/services/localStorageService";

function AppointmentManage(props) {
  const [items, setItems] = useState([]);

  const getItems = () => {
    axios({
      method: "POST",
      url: myApi + "/intraco/lpg/appointment/list",
      headers: {
        "x-access-token": localStorageService.getItem("auth_user").token,
      },
    })
      .then((response) => {
        console.log(response.data.data);
        setItems(Array.isArray(response.data.data) ? response.data.data : []);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const addItemToState = (item) => {
    setItems([...items, item]);
  };

  const updateState = (item) => {
    const itemIndex = items.findIndex((data) => data.id === item.id);
    const newArray = [
      ...items.slice(0, itemIndex),
      item,
      ...items.slice(itemIndex + 1),
    ];
    setItems(newArray);
  };

  const deleteItemFromState = (id) => {
    const updatedItems = items.filter((item) => item.id !== id);
    setItems(updatedItems);
  };

  useEffect(() => {
    getItems();
  }, []);

  return (
    <div className="m-sm-30">
      <div className="mb-sm-30">
        <Container className="App">
          <Row>
            <Col>
              <Breadcrumb
                routeSegments={[
                  { name: "Appointment", path: "/appointment/edit" },
                  { name: "Manage" },
                ]}
              />
              <h1 style={{ margin: "20px 0" }}></h1>
            </Col>
          </Row>
          {items && (
            <Row>
              <Col>
               
               </Col>
            </Row>
          )}
          <br></br>
          {items && (
            <Row>
              <Col>
                {items && (
                  <DataTable
                    items={items}
                    updateState={updateState}
                    deleteItemFromState={deleteItemFromState}
                  />
                )}{" "}
              </Col>
            </Row>
          )}
        </Container>
      </div>
    </div>
  );
}

export default AppointmentManage;
