import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "reactstrap";
import ModalForm from "./Modal";
import DeactiveDataTable from "./deactiveDataTable";
import { CSVLink } from "react-csv";
import axios from "axios";
import { Breadcrumb } from "matx";
import myApi from "../../auth/api";
import localStorageService from "app/services/localStorageService";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import {
  Button,
  Icon,
  Grid,
  Radio,
  RadioGroup,
  FormControlLabel,
  Checkbox,
} from "@material-ui/core";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
function DeactiveAppointmentManage(props) {
  const [items, setItems] = useState([]);

  const getItems = () => {
    axios({
      method: "POST",
      url: myApi + "/intraco/lpg/appointment/deactiveList",
      headers: {
        "x-access-token": localStorageService.getItem("auth_user").token,
      },
    })
      .then((response) => {
        setItems(Array.isArray(response.data.data) ? response.data.data : []);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const addItemToState = (item) => {
    setItems([...items, item]);
  };

  const updateState = (item) => {
    const itemIndex = items.findIndex((data) => data.id === item.id);
    const newArray = [
      ...items.slice(0, itemIndex),
      item,
      ...items.slice(itemIndex + 1),
    ];
    setItems(newArray);
  };

  const deleteItemFromState = (id) => {
    const updatedItems = items.filter((item) => item.id !== id);
    setItems(updatedItems);
  };

  useEffect(() => {
    getItems();
  }, []);

  return (
    <div className="m-sm-30">
      <div className="mb-sm-30">
        <Container className="App">
          <Row>
            <Col>
              <Breadcrumb
                routeSegments={[
                  { name: "Appointment", path: "/appointment/deactive" },
                  { name: "Manage Deactive Appointments" },
                ]}
              />
              <h1 style={{ margin: "20px 0" }}></h1>
            </Col>
          </Row>
          <ValidatorForm
            ref="form"
            onSubmit={this.handleSubmit}
            onError={(errors) => null}
          >
            <Grid container spacing={6}>
              <Grid item lg={6} md={6} sm={12} xs={12}>
                <TextValidator
                autoComplete='off'
                  className="mb-4 w-full"
                  label="Driver Name (Max length : 25)"
                  onChange={this.handleChange}
                  type="text"
                  name="driverName"
                  value={driverName}
                  validators={["required", "maxStringLength: 25"]}
                  errorMessages={["this field is required"]}
                />
                <TextValidator
                autoComplete='off'
                  className="mb-4 w-full"
                  label="Car Number"
                  onChange={this.handleChange}
                  type="text"
                  name="carNumber"
                  value={carNumber}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />

                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    className="mb-4 w-full"
                    margin="none"
                    id="mui-pickers-date"
                    label="Appointment Date"
                    inputVariant="standard"
                    type="text"
                    autoOk={true}
                    value={date}
                    onChange={this.handleDateChange}
                    KeyboardButtonProps={{
                      "aria-label": "Appointment Date",
                    }}
                  />
                </MuiPickersUtilsProvider>
              </Grid>

              <Grid item lg={6} md={6} sm={12} xs={12}>
                <TextValidator
                autoComplete='off'
                  className="mb-4 w-full"
                  label="Car Service"
                  onChange={this.handleChange}
                  type="text"
                  name="serviceDetails"
                  value={serviceDetails}
                  validators={["required"]}
                  errorMessages={["this field is required"]}
                />
                <select
                  style={{
                    width: "100%",
                    padding: "5px",
                    backgroundColor: "white",
                    color: "#444",
                    borderBottomColor: "#949494",
                    textAlign: "left",
                    marginTop: "20px",
                  }}
                  className="mb-4 w-full btn btn-secondaryy dropdown-toggle"
                  name="selectedClient"
                  value={this.state.selectedClient}
                  onChange={(e) =>
                    this.setState({
                      selectedClient: e.target.value,
                      validationError:
                        e.target.value === ""
                          ? "You must select Client "
                          : undefined,
                    })
                  }
                >
                  {this.state.clients.map((client) => (
                    <option key={client.id} value={client.id}>
                      {client.name}
                    </option>
                  ))}
                </select>
              </Grid>
            </Grid>
            <Button color="primary" variant="contained" type="submit">
              <Icon>send</Icon>
              <span className="pl-2 capitalize">Submit</span>
            </Button>
          </ValidatorForm>
          <Row>
            <Col>
              <CSVLink
                filename={"db.csv"}
                color="primary"
                style={{ float: "left", marginRight: "10px" }}
                className="btn btn-primary"
                data={items}
              >
                Download CSV
              </CSVLink>
            </Col>
          </Row>
          <Row>
            <Col>
              {items && (
                <DeactiveDataTable
                  items={items}
                  updateState={updateState}
                  deleteItemFromState={deleteItemFromState}
                />
              )}{" "}
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
}

export default DeactiveAppointmentManage;
