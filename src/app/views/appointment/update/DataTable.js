import React, { useState } from "react";
import { Button } from "reactstrap";
import ModalForm from "./Modal";
import axios from "axios";
import myApi from "../../../auth/api";
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TablePagination,
} from "@material-ui/core";
import Swal from "sweetalert2";
import ModalHistory from "../../sisterConcern/ModalHistory";
import history from "../../../../history";

const Datatable = (props) => {
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
  };

  const deleteItem = (id, title, active_status) => {
    axios
      .post(
        myApi + "/intraco/lpg/company/delete",
        { id: id },
        {
          headers: {
            "x-access-token": localStorage.getItem("jwt_token"),
          },
        }
      )
      .then((res) => {
        Swal.fire({
          icon: "success",
          title: "Deactivated",
          showConfirmButton: false
        });
      })
      .then((item) => {
        props.updateState({
          id,
          title,
          active_status: 1,
        });
      })
      .catch((err) => console.log(err));
  };

  const activeItem = (id, title, active_status) => {
    axios
      .post(
        myApi + "/intraco/lpg/company/active",
        { id: id },
        {
          headers: {
            "x-access-token": localStorage.getItem("jwt_token"),
          },
        }
      )
      .then((res) => {
        Swal.fire({
          icon: "success",
          title: "Activated",
          showConfirmButton: false
        });
      })
      .then((item) => {
        props.updateState({
          id,
          title,
          active_status: 0,
        });
      });
  };

  const details = (id) => {

  };

  return (
    <div className="w-full overflow-auto">
      <Table className="whitespace-pre">
        <TableHead>
          <TableRow>
            <TableCell className="px-0" style={{ width: "5%" }}>
              SL.
            </TableCell>
            <TableCell className="px-0" style={{ width: "35%" }}>
              Title
            </TableCell>
            <TableCell className="px-0" style={{ width: "35%" }}>
              Sales Person
            </TableCell>
            <TableCell className="px-0" style={{ width: "15%" }}>
              Contact
            </TableCell>
          
          </TableRow>
        </TableHead>
        <TableBody>
          {props.items &&
            props.items
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((item, id) => (
                <TableRow key={id}>
                  <TableCell
                    className="px-0 capitalize"
                    align="left"
                    style={{ width: "5%" }}
                  >
                   {page * rowsPerPage + id + 1}
                  </TableCell>
                  <TableCell
                    className="px-0 capitalize"
                    align="left"
                    style={{ width: "35%" }}
                  >
                    {item.title}
                  </TableCell>
                  {item.sales_person_name === null ? (
                    <TableCell
                      className="px-0 capitalize"
                      align="left"
                      style={{ width: "35%" }}
                    >
                      <font color="red">Not Assigned</font>
                    </TableCell>
                  ) : (
                    <TableCell
                      className="px-0 capitalize"
                      align="left"
                      style={{ width: "35%" }}
                    >
                      {item.sales_person_name}
                    </TableCell>
                  )}

                  {item.contact_number === null ? (
                    <TableCell
                      className="px-0 capitalize"
                      align="left"
                      style={{ width: "15%" }}
                    >
                      <font color="red">Not Assigned</font>
                    </TableCell>
                  ) : (
                    <TableCell
                      className="px-0 capitalize"
                      align="left"
                      style={{ width: "15%" }}
                    >
                      {item.contact_number}
                    </TableCell>
                  )}

                </TableRow>
              ))}
        </TableBody>
      </Table>

      <TablePagination
        style={{
          maxWidth: 400,
          overflowX: "hidden",
          padding: "0px!important",
          display: "contents",
        }}
        className="px-4"
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={props.items.length}
        rowsPerPage={rowsPerPage}
        page={page}
        backIconButtonProps={{
          "aria-label": "Previous Page",
        }}
        nextIconButtonProps={{
          "aria-label": "Next Page",
        }}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </div>
  );
};

export default Datatable;
