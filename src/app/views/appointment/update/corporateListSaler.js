/* eslint-disable jsx-a11y/heading-has-content */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "reactstrap";
import ActiveDataTable from "../activeDataTable";
import axios from "axios";
import { Breadcrumb } from "matx";
import myApi from "../../../auth/api";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import { Button, Icon, Grid } from "@material-ui/core";
import { ValidatorForm } from "react-material-ui-form-validator";
import localStorageService from "app/services/localStorageService";
import jsPDF from "jspdf";
import "jspdf-autotable";
import Moment from "moment";
import Select from "react-select";
import Swal from "sweetalert2";
import { DateRangePicker } from "react-dates";

function Sa(props) {
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [focusedInput, setFocusedInput] = useState(null);

  const [name, setNames] = useState("");
  const [selectedCompany, setSelectedCompany] = useState("");
  const [selectedSisterConcern, setSelectedSisterConcern] = useState("");
  const [company, setCompany] = useState([]);
  const [sisterConcernList, setSisterConcernList] = useState([]);
  const [items, setItems] = useState([]);
  const [client_type_array, setClientType] = useState([]);
  const [client_type_arrayy, setClientTypeArray] = useState([]);
  const [clients, setClients] = useState([]);
  const [clients_corporate, setClientCorporate] = useState([]);
  const [prospectTypes, setProspectTypes] = useState([]);
  const [discussionTypes, setDiscussionTypes] = useState([]);
  const [sales, setSales] = useState([]);
  const [VehicleTypes, setVehicleTypes] = useState([]);
  const [CallTimeTypes, setCallTimeTypes] = useState([]);
  const [Leads, setLeads] = useState([]);
  const [form, setValues] = useState({
    selectedClientIndividual: "",
    selectedClientCorporate: "",
    selectedClientType: "",
    selectedProspectType: "",
    selectedClient: "",
    selectedSales: "",
    selectedDiscussionType: "",
    selectedCallTimeType: "",
    selectedVehicleType: "",
    selectedLead: "",
    columns: [
      "discussion_type",
      "vehicle_type",
      "sales_person",
      "call_at",
      "leads",
      "call_date",
    ],
    options: {
      filter: true,
      filterType: "dropdown",
    },
    tableData: [],
    startDate: null,
    endDate: null,
    endDateFormatted: null,
    startDateFormatted: null,
    isSubmitted: false,
    focusedInput: "",
    selectedVehicleIndividualNew: [],
    selectedDiscussionCorporateNew: [],
  });

  const getItems = () => {
    axios({
      method: "POST",
      url: myApi + "/intraco/lpg/appointment/activeList",
      headers: {
        "x-access-token": localStorageService.getItem("auth_user").token,
      },
    })
      .then((response) => {
        // console.log(response)
        if (response.data.error) {
          setValues({ ...form, isSubmitted: false })
          console.log(form.isSubmitted)
        } else {
          setValues({ ...form, isSubmitted: true })
        }
        setItems(Array.isArray(response.data.data) ? response.data.data : []);
      })
      .catch((error) => {
        console.log(error);
      });

    // getting clientType
    axios
      .get(myApi + "/intraco/lpg/client_type/list", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        let clientsFromApi = res.data.data.map((client) => {
          return {
            value: client.id,
            label: client.type,
            id: client.id,
            type: client.type,
          };
        });

        setClientType(
          [].concat(clientsFromApi)
        );
        setClientTypeArray(
          [
            {
              value: 0,
              label: "Corporate Client",
              id: 0,
              type: "Corporate Client",
            },
          ].concat(clientsFromApi)
        );
      })
      .catch((error) => {
        console.log(error);
      });
    // getting clientInformation
    axios
      .get(myApi + "/intraco/lpg/client/list", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        let clientsFromApi = res.data.data.map((client) => {
          return {
            value: client.id,
            label: client.name,
            id: client.id,
            name: client.name,
          };
        });

        setClients([].concat(clientsFromApi));
      })
      .catch((error) => {
        console.log(error);
      });

    // getting Sister Concern List
    axios
      .get(myApi + "/intraco/lpg/company_sister_concerns/allList", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        // console.log(res);
        let clientsFromApi =
          res.data.data &&
          res.data.data.map((cl) => {
            return {
              value: cl.id,
              label: cl.company_title + "_" + cl.title,
              id: cl.id,
              name: cl.company_title + "_" + cl.title,
            };
          });

        setClientCorporate([].concat(clientsFromApi));
      })
      .catch((error) => {
        console.log(error);
      });
    // getting prospectType
    axios
      .get(myApi + "/intraco/lpg/prospect_type/list", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        let clientsFromApi = res.data.data.map((propspectType) => {
          return {
            value: propspectType.id,
            label: propspectType.type,
            id: propspectType.id,
            type: propspectType.type,
          };
        });

        setProspectTypes([].concat(clientsFromApi));
      })
      .catch((error) => {
        console.log(error);
      });

    // getting clientInformation
    axios
      .get(myApi + "/intraco/lpg/salesPerson/list", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        let clientsFromApi = res.data.data.map((client) => {
          return {
            value: client.id,
            label: client.name,
            id: client.id,
            name: client.name,
          };
        });

        setSales([].concat(clientsFromApi));
      })
      .catch((error) => {
        console.log(error);
      });

    // getting discussionType
    axios
      .get(myApi + "/intraco/lpg/discussion_type/list", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        let clientsFromApi = res.data.data.map((discussionType) => {
          return {
            value: discussionType.id,
            label: discussionType.type,
            id: discussionType.id,
            type: discussionType.type,
          };
        });

        setDiscussionTypes([].concat(clientsFromApi));
      })
      .catch((error) => {
        console.log(error);
      });

    // getting vehicleType
    axios
      .get(myApi + "/intraco/lpg/vehicle_type/list", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        let clientsFromApi = res.data.data.map((VehicleType) => {
          return {
            value: VehicleType.id,
            label: VehicleType.type,
            id: VehicleType.id,
            type: VehicleType.type,
          };
        });

        setVehicleTypes([].concat(clientsFromApi));
      })
      .catch((error) => {
        console.log(error);
      });

    // getting call type
    axios
      .get(myApi + "/intraco/lpg/call_time/list", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        let clientsFromApi = res.data.data.map((CallTimeType) => {
          return {
            id: CallTimeType.id,
            type: CallTimeType.type,

          };
        });

        setCallTimeTypes(
          [
            {
              id: "",
              type: "Select Call Time Type",
            },
          ].concat(clientsFromApi)
        );
      })
      .catch((error) => {
        console.log(error);
      });

    // getting Leads
    axios
      .get(myApi + "/intraco/lpg/lead/activeList", {
        headers: {
          "x-access-token": localStorageService.getItem("auth_user").token,
        },
      })
      .then((res) => {
        //console.log(res.data.data)
        let clientsFromApi = res.data.data.map((Lead) => {
          return {
            value: Lead.id,
            label: Lead.name,
            id: Lead.id,
            name: Lead.name,
          };
        });

        setLeads([].concat(clientsFromApi));
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const addItemToState = (item) => {
    setItems([...items, item]);
    setNames([...name, name]);
  };

  const updateState = (item) => {
    const itemIndex = items.findIndex((data) => data.id === item.id);
    const newArray = [
      ...items.slice(0, itemIndex),
      item,
      ...items.slice(itemIndex + 1),
    ];
    setItems(newArray);
  };

  const deleteItemFromState = (id) => {
    const updatedItems = items.filter((item) => item.id !== id);
    setItems(updatedItems);
  };

  useEffect(() => {
    getItems();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onSelectChangeCompany = (label) => {
    if (label !== null) {
      setSelectedCompany(label.label);
    }
    if (label === null) {
      setSelectedCompany("");
    }
  };

  const onSelectChangeSisterConcern = (label) => {
    if (label !== null) {
      setSelectedSisterConcern(label.label);
    }
    if (label === null) {
      setSelectedSisterConcern("");
    }
  };

  const handleSubmit = () => {
    console.log(client_type_array)
    let reqData = {
      client_type_id: form.selectedClientType,
      client_id: form.selectedClientIndividual || form.selectedClientCorporate,
      sales_person_id: form.selectedSales || (localStorageService.getItem('auth_user') && localStorageService.getItem('auth_user').userId),
      discussion_type_id: form.selectedDiscussionType,
      from_date: Moment(startDate).format("YYYY-MM-DD"),
      to_date: Moment(endDate).format("YYYY-MM-DD")
    }
    if (reqData.to_date === ('' || "Invalid date") && reqData.from_date !== ('' || "Invalid date")) {
      delete reqData.to_date
    }
    if (reqData.from_date === ('' || "Invalid date") && reqData.to_date !== ('' || "Invalid date")) {
      delete reqData.from_date
    }
    if (reqData.from_date === ('' || "Invalid date") && reqData.to_date === ('' || "Invalid date")) {
      axios
        .post(
          myApi + "/intraco/lpg/appointment/searchActive", {
          client_type_id: form.selectedClientType,
          client_id: form.selectedClientIndividual || form.selectedClientCorporate,
          sales_person_id: form.selectedSales,
          discussion_type_id: form.selectedDiscussionType,
          from_date: Moment(new Date()).format("YYYY-MM-DD"),
          to_date: Moment(new Date()).format("YYYY-MM-DD")
        },
          {
            headers: {
              "x-access-token": localStorageService.getItem("auth_user").token, // override instance defaults
            },
          }
        )
        .then((res) => {
          if (res.data.data) {
            if (res.data.count === 0) {
              Swal.fire({
                icon: "error",
                title: "No Data Found",
                showConfirmButton: false,
                timer: 1000,
              });
            }
            setItems(Array.isArray(res.data.data) ? res.data.data : []);
          }
        });
    } else {
      axios
        .post(
          myApi + "/intraco/lpg/appointment/searchActive", reqData,
          {
            headers: {
              "x-access-token": localStorageService.getItem("auth_user").token, // override instance defaults
            },
          }
        )
        .then((res) => {
          if (res.data.data) {
            if (res.data.count === 0) {
              Swal.fire({
                icon: "error",
                title: "No Data Found",
                showConfirmButton: false,
                timer: 1000,
              });
            }
            setItems(Array.isArray(res.data.data) ? res.data.data : []);
          }
        });
    }
  };

  const exportPDF = () => {
    const filterPosition = [90, 103, 116, 129, 142, 155];
    let filterPositionPointer = 0;
    const locateValueById = (types, id) => {
      let item = types.find((it) => it.id === Number(id));
      return item;
    };
    const addFooters = (doc) => {
      const pageCount = doc.internal.getNumberOfPages();
      var footer = new Image();
      footer.src = "/assets/footerPdf.png";

      doc.setFont("helvetica", "italic");
      doc.setFontSize(8);
      for (var i = 1; i <= pageCount; i++) {
        doc.setPage(i);

        doc.addImage(image, "JPEG", pageWidth - 110, 0, 100, 100);
        doc.addImage(leftBar, "JPEG", 0, 0, 16, 270);
        doc.addImage(footer, "JPEG", 0, pageHeight - 60, pageWidth, 60);
      }
    };

    const unit = "pt";
    const size = "A4"; // Use A1, A2, A3 or A4
    const orientation = "portrait"; // portrait or landscape

    const marginLeft = 40;
    const doc = new jsPDF(orientation, unit, size);
    var pageHeight =
      doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
    var pageWidth =
      doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
    doc.setFontSize(15);

    var leftBar = new Image();
    leftBar.src = "/assets/leftBar.png";
    var image = new Image();
    image.src = "/assets/logoNew.png";
    const title = "Appointment Report";
    const headers = [
      [
        "SL",
        "Date",
        "Sales Person",
        "Client Type",
        "Client",
        "Car Number",
        "Driver Name",
        "Service Details",
        "Status",
      ],
    ];

    const data = items.map((ap, i) => [i + 1, Moment(ap.appointment_date).format("YYYY-MM-DD"), ap.sales_person, locateValueById(client_type_arrayy, ap.client_type_id) &&
      locateValueById(client_type_arrayy, ap.client_type_id).type, ap.client_info.map(e => e.name || e.company_title + "_" + e.title),
    ap.car_number, ap.driver_name, ap.service_details, ap.status]);

    doc.setFont('helvetica', 'bold')
    doc.setFontSize(15)
    doc.text(title, marginLeft, 35);
    doc.setFont('helvetica', 'normal')
    doc.setFontSize(10)

    const filterData =
      form.startDate &&
      "Date : " +
      Moment(form.startDate).format("YYYY-MM-DD") +
      " > " +
      Moment(form.endDate).format("YYYY-MM-DD");

    const startDateFilter = "Date : " +
      Moment(form.startDate).format("YYYY-MM-DD");

    const endDateFilter = "Date : " +
      Moment(form.endDate).format("YYYY-MM-DD");

    const todayFilter = "Date : " +
      Moment(new Date()).format("YYYY-MM-DD");

    form.startDate && form.endDate && doc.text(filterData, marginLeft, 85);
    form.startDate && !form.endDate && doc.text(startDateFilter, marginLeft, 85);
    !form.startDate && form.endDate && doc.text(endDateFilter, marginLeft, 85);
    !form.startDate && !form.endDate && doc.text(todayFilter, marginLeft, 85);
    //edit

    const filterDiscussion = form.selectedDiscussionType !== '' ? locateValueById(discussionTypes, Number(form.selectedDiscussionType)) && locateValueById(discussionTypes, Number(form.selectedDiscussionType)).type : ''
    const filterClientIndividual = form.selectedClientIndividual !== '' && form.selectedClientCorporate === '' ? locateValueById(clients, form.selectedClientIndividual) && locateValueById(clients, Number(form.selectedClientIndividual)).name : ''
    const filterClientCorporate = form.selectedClientIndividual === '' && form.selectedClientCorporate !== '' ? locateValueById(clients_corporate, Number(form.selectedClientCorporate)) && locateValueById(clients_corporate, Number(form.selectedClientCorporate)).name : ''
    const filterClientType = form.selectedClientType !== '' ? locateValueById(client_type_array, Number(form.selectedClientType)) && locateValueById(client_type_array, Number(form.selectedClientType)).type : ''

    doc.setFont("helvetica", "strong");
    doc.setFontSize(11);
    doc.text('Search By : ', marginLeft, 70);
    doc.setFont("helvetica", "regular");
    doc.setFontSize(9);
    doc.text('Generated On : ' + Moment(new Date()).format("YYYY-MM-DD"), marginLeft, 50);

    doc.setFont("helvetica", "normal");
    doc.setFontSize(10);

    form.selectedDiscussionType !== '' && doc.text('\r\nDiscussion type : ' + filterDiscussion, marginLeft, filterPosition[filterPositionPointer++])
    form.selectedClientIndividual !== '' && form.selectedClientCorporate === '' && doc.text('\r\nClient : ' + filterClientIndividual, marginLeft, filterPosition[filterPositionPointer++])
    form.selectedClientIndividual === '' && form.selectedClientCorporate !== '' && doc.text('\r\nClient : ' + filterClientCorporate, marginLeft, filterPosition[filterPositionPointer++])
    form.selectedClientType !== '' && doc.text('\r\nClient Type : ' + filterClientType, marginLeft, filterPosition[filterPositionPointer++])

    //end
    let content = {
      startY: 30 + filterPosition[filterPositionPointer++],
      head: headers,
      body: data,
      margin: {
        bottom: 120,
        top: 150
      }
    };
    doc.autoTable(content);
    doc.setTextColor(100);
    doc.setFontSize(10);

    addFooters(doc);

    doc.save(
      "appointment_report(" + Moment(new Date()).format("YYYY-MM-DD") + ").pdf"
    );
  };

  const onSelectChangeIndividualClient = (value) => {
    if (value !== null) {
      setValues({ ...form, selectedClientIndividual: value.value });
      console.log(form.selectedClientIndividual);
    }
    if (value === null) {
      setValues({ ...form, selectedClientIndividual: "" });
      console.log(form.selectedClientIndividual);
    }
  };
  const onSelectChange = (value) => {
    // toggleOpen();
    if (value !== null) {
      setValues({ ...form, selectedSalesNew: value.value });
      console.log(form.selectedSalesNew);
    }
    if (value === null) {
      setValues({ ...form, selectedSalesNew: "" });
      console.log(form.selectedSalesNew);
    }
  };

  const onSelectChangeDiscussionType = (value) => {
    if (value !== null) {
      setValues({ ...form, selectedDiscussionType: value.value });
    }
    if (value === null) {
      setValues({ ...form, selectedDiscussionType: "" });
      console.log(form.selectedDiscussionType);
    }
  };

  const onSelectChangeVehicleType = (value) => {
    if (value !== null) {
      setValues({
        ...form,
        selectedVehicleType: Array.isArray(value)
          ? value.map((x) => x.value)
          : [],

        selectedVehicleIndividualNew: Array.isArray(value)
          ? value.map((x) => ({ value: x.value, label: x.label }))
          : [],
      });

      console.log(
        Array.isArray(value)
          ? value.map((x) => ({ value: x.value, label: x.label }))
          : []
      );
      console.log(form.selectedVehicleIndividualNew);
    }
    if (value === null) {
      setValues({ ...form, selectedVehicleType: "" });
      console.log(form.selectedVehicleTypeIndividual);
    }
  };

  const onSelectChangeIndividualLeads = (value) => {
    if (value !== null) {
      setValues({ ...form, selectedLead: value.value });
      console.log(form.selectedLead);
    }
    if (value === null) {
      setValues({ ...form, selectedLead: "" });
      console.log(form.selectedLead);
    }
  };
  const onSelectChangeIndividualProspectype = (value) => {
    if (value !== null) {
      setValues({ ...form, selectedProspectType: value.value });
      console.log(form.selectedProspectType);
    }
    if (value === null) {
      setValues({ ...form, selectedProspectType: "" });
      console.log(form.selectedProspectType);
    }
  };
  const onSelectChangeClientType = (value) => {
    // toggleOpen();
    if (value !== null) {
      setValues({ ...form, selectedClientType: value.value });
      console.log(form.selectedClientType);
    }
    if (value === null) {
      setValues({ ...form, selectedClientType: "" });
      console.log(form.selectedClientType);
    }
  };
  const onSelectChangeSaler = (value) => {
    if (value !== null) {
      setValues({ ...form, selectedSales: value.value });
      console.log(form.selectedSales);
    }
    if (value === null) {
      setValues({ ...form, selectedSales: "" });
      console.log(form.selectedSales);
    }
  };
  const onSelectChangeCorporateClient = (value) => {
    if (value !== null) {
      setValues({ ...form, selectedClientCorporate: value.value });
      console.log(form.selectedClientCorporate);
    }
    if (value === null) {
      setValues({ ...form, selectedClientCorporate: "" });
      console.log(form.selectedClientCorporate);
    }
  };

  const handleChange = (event) => {
    event.persist();
    setValues({ ...form, [event.target.name]: event.target.value });
  };

  const hundleDateChange = (startDate, endDate) => {
    setValues(() => ({
      endDate,
      startDate,
    }));
    if (startDate != null) {
      setValues(() => ({
        startDateFormatted: startDate.format("YYYY-MM-DD"),
      }));
    }
    if (endDate != null) {
      setValues(() => ({
        endDateFormatted: endDate.format("YYYY-MM-DD"),
      }));
    }
  };
  return (
    <div className="m-sm-30">
      <div className="mb-sm-30">
        <Container className="App">
          <Row>
            <Col>
              <Breadcrumb
                routeSegments={[
                  { name: "Appointment List", path: "/appointment/manage" },
                  { name: "Filter" },
                ]}
              />
              <h1 style={{ margin: "20px 0" }}></h1>
            </Col>
          </Row>

          <br></br>
          <Row>
            <Col>
              <div>
                <ValidatorForm onSubmit={handleSubmit}>
                  {localStorageService.getItem("auth_user").role ===
                    "SALES-PERSON" && (
                      <Grid container spacing={6}>
                        <Grid
                          item
                          lg={6}
                          md={6}
                          sm={12}
                          xs={12}

                        >
                          <div
                            style={{ marginBottom: "15px", marginTop: "-10px" }}
                          >
                            <strong>
                              <font color="black">Select Client Type </font>
                            </strong>
                          </div>
                          <Select
                            style={{
                              width: "100%",
                              backgroundColor: "white",
                              color: "#444",
                              borderBottomColor: "#000000",
                              textAlign: "left",
                              marginTop: "40px",
                            }}
                            value={client_type_array.filter(
                              (option) => option.value === form.selectedClientType
                            )}
                            isClearable="true"
                            name="selectedClientType"
                            options={client_type_array}
                            className="basic-multi-select"
                            classNamePrefix="select"
                            onChange={onSelectChangeClientType}
                          />
                          {/* <Grid item lg={6} md={6} sm={12} xs={12} style={{ maxWidth: '25%', }}> */}
                          <div
                            style={{ marginBottom: "15px", marginTop: "20px" }}
                          >
                            <strong>
                              <font color="black">Select Discussion Type </font>
                            </strong>
                          </div>

                          <Select
                            style={{
                              width: "100%",
                              backgroundColor: "white",
                              color: "#444",
                              borderBottomColor: "#000000",
                              textAlign: "left",
                              marginTop: "40px",
                            }}
                            // isMulti
                            isClearable="true"
                            name="selectedDiscussionType"
                            options={discussionTypes}
                            className="basic-multi-select"
                            classNamePrefix="select"
                            onChange={onSelectChangeDiscussionType}
                          />

                          {/* </Grid> */}
                        </Grid>

                        <Grid
                          item
                          lg={6}
                          md={6}
                          sm={12}
                          xs={12}

                        >
                          <div
                            style={{ marginBottom: "15px", marginTop: "-10px" }}
                          >
                            <strong>
                              <font color="black">Select Client </font>
                            </strong>
                          </div>
                          {form.selectedClientType === "" ? (
                            <Select
                              style={{
                                width: "100%",
                                backgroundColor: "white",
                                color: "#444",
                                borderBottomColor: "#000000",
                                textAlign: "left",
                                marginTop: "40px",
                              }}
                              isClearable="true"
                              name="selectedClientIndividual"
                              className="basic-multi-select"
                              classNamePrefix="select"
                            />
                          ) : form.selectedClientType === 0 &&
                            form.selectedClientType !== "" ? (
                                <Select
                                  style={{
                                    width: "100%",
                                    backgroundColor: "white",
                                    color: "#444",
                                    borderBottomColor: "#000000",
                                    textAlign: "left",
                                    marginTop: "40px",
                                  }}
                                  isClearable="true"
                                  name="selectedClientIndividual"
                                  options={clients_corporate}
                                  className="basic-multi-select"
                                  classNamePrefix="select"
                                  onChange={onSelectChangeCorporateClient}
                                />
                              ) : (
                                <Select
                                  style={{
                                    width: "100%",
                                    backgroundColor: "white",
                                    color: "#444",
                                    borderBottomColor: "#000000",
                                    textAlign: "left",
                                    marginTop: "40px",
                                  }}
                                  isClearable="true"
                                  name="selectedClientIndividual"
                                  options={clients}
                                  className="basic-multi-select"
                                  classNamePrefix="select"
                                  onChange={onSelectChangeIndividualClient}
                                />
                              )}

                          <div
                            style={{ marginBottom: "15px", marginTop: "20px" }}
                          >
                            <strong>
                              <font color="black">Select Date </font>
                            </strong>
                          </div>

                          <DateRangePicker
                            isOutsideRange={() => false}
                            startDate={startDate}
                            startDateId="s_id"
                            endDate={endDate}
                            endDateId="e_id"
                            onDatesChange={({ startDate, endDate }) => {
                              setStartDate(startDate);
                              setEndDate(endDate);
                            }}
                            focusedInput={focusedInput}
                            onFocusChange={(e) => setFocusedInput(e)}
                            displayFormat="DD/MM/YYYY"
                          />
                        </Grid>
                      </Grid>
                    )}

                  <Button
                    onSubmit={handleSubmit}
                    color="primary"
                    variant="contained"
                    type="submit"
                    style={{ marginTop: "25px" }}
                  >
                    <Icon>send</Icon>
                    <span className="pl-2 capitalize">Search</span>
                  </Button>
                  {"   "}

                </ValidatorForm>
                {items.length !== 0 && (
                  <button
                    className="btn btn-primary"
                    style={{ marginTop: "-27px", marginLeft: '85%' }}
                    onClick={() => exportPDF()}
                  >
                    Generate Report
                  </button>
                )}
              </div>
            </Col>
          </Row>
          <br></br>
          <br></br>
          <Row>
            <Col>
              <ActiveDataTable
                items={items}
                updateState={updateState}
                deleteItemFromState={deleteItemFromState}
              />
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
}

export default Sa;
