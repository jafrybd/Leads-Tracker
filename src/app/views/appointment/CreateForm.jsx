import React, { Component } from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import localStorageService from "./../../services/localStorageService"
import axios from 'axios'
import myApi from '../../auth/api'
import Swal from 'sweetalert2'
import moment from 'moment'
import {
  Button,
  Icon,
  Grid,

} from "@material-ui/core";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from "@material-ui/pickers";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import Select from 'react-select';

class CreateForm extends Component {

  constructor() {
    super();
    this.state = {
      discussionTypes: [],
      clients_corporate: [],
      selectedClientType: '',
      selectedClientIndividual: '',
      selectedClientCorporate: '',
      selectedDiscussionType: '',
      client_type_array: [],

      clients: [],

      driverName: '',
      carNumber: '',
      serviceDetails: ''
    }
  }


  componentDidMount() {
    // custom rule will have name 'isPasswordMatch'
    ValidatorForm.addValidationRule("isPasswordMatch", value => {
      if (value !== this.state.password) {
        return false;
      }
      return true;
    });

    // getting clientType
    axios.get(
      myApi + "/intraco/lpg/client_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(client => {
          return {
            value: client.id,
            label: client.type
          };
        });

        this.setState({
          client_type_array: []
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // custom rule will have name 'isPasswordMatch'
    ValidatorForm.addValidationRule("isPasswordMatch", value => {
      if (value !== this.state.password) {
        return false;
      }
      return true;
    });

    // getting clientInformation
    axios.get(
      myApi + "/intraco/lpg/client/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        // let clientsFromApi = res.data.data.map(client => {
        //   return {
        //     value: client.id,
        //     label: client.name
        //   };
        // });

        // this.setState({
        //   clients: [

        //   ]
        //     .concat(clientsFromApi)
        // });
      })
      .catch(error => {
        console.log(error);
      });

    // getting Sister Concern List
    axios.get(
      myApi + "/intraco/lpg/appointment/create/data",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {
        let clientsFromApi = res.data.data && res.data.data.corporateClient.map(cl => {
          return {
            value: cl.id,
            label: cl.company_title + '_' + cl.company_sister_concerns_title
          };
        });

        this.setState({
          clients_corporate: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        console.log(error);
      });

    // getting discussionType
    axios.get(
      myApi + "/intraco/lpg/discussion_type/list",
      {
        headers: {
          'x-access-token': localStorageService.getItem('auth_user').token
        }
      }
    )
      .then(res => {

        let clientsFromApi = res.data.data.map(discussionType => {
          return {
            value: discussionType.id,
            label: discussionType.type
          };
        });

        this.setState({
          discussionTypes: [

          ]
            .concat(clientsFromApi)
        });
      })
      .catch(error => {
        //console.log(error);
      });

  }

  componentWillUnmount() {
    // remove rule when it is not needed
    ValidatorForm.removeValidationRule("isPasswordMatch");
  }

  handleSubmit = event => {


    axios.post(myApi + '/intraco/lpg/appointment/add', {
      'client_type_id': this.state.selectedClientType,
      "client_id": this.state.selectedClientIndividual || this.state.selectedClientCorporate,
      "appointment_date": moment(this.state.date).format('YYYY-MM-DD'),
      "car_number": this.state.carNumber,
      "driver_name": this.state.driverName,
      "service_details": this.state.serviceDetails,
      "discussion_type_id": this.state.selectedDiscussionType,
      // "sales_person_id": localStorageService.getItem('auth_user').userId
    }, {

      "headers": {
        "x-access-token": localStorageService.getItem("auth_user").token
      }
    })

      .then(res => {

        if (!res.data.error) {
          Swal.fire({
            icon: "success",
            title: res.data.message,
            showConfirmButton: false,
            timer: 1000
          });
        } else {
          Swal.fire({
            icon: "error",
            title: res.data.message,
            showConfirmButton: false,
            timer: 1000
          });
        }
        if (res.data.error === false) {
          this.setState({
            selectedClient: "",
            selectedClientType: 1,
            selectedClientCorporate: '',
            selectedDiscussionType: '',
            selectedClientIndividual: '',

            driverName: '',
            carNumber: '',
            serviceDetails: ''
          })
        }

      })
  }

  handleChange = event => {
    event.persist();
    this.setState({ [event.target.name]: event.target.value });
  };

  handleDateChange = date => {

    this.setState({ date });
  };
  onSelectChangeDays = value => {
    if (value !== null) {
      this.setState({ selectedClientType: value.value });
      // if (this.state.selectedClientType !== '') {
      // getting clientInformation
      axios.get(
        myApi + "/intraco/lpg/client/list",
        {
          headers: {
            'x-access-token': localStorageService.getItem('auth_user').token
          }
        }
      )
        .then(res => {
          let clientsFromApi = res.data.data &&

            res.data.data
              .filter(x => Number(x.client_type_id) === this.state.selectedClientType)
              .map(client => {
                console.log(client)
                return {
                  value: client.id,
                  label: client.name,
                }
              })

          this.setState({
            clients: []
              .concat(clientsFromApi)
          });
          console.log(this.state.clients)
        })
        .catch(error => {
          // console.log(error);
        });
      // }
    }
    if (value === null) {
      this.setState({ selectedClientType: "" });
      console.log(this.state.selectedClientType)
    }
  };
  onSelectChangeCorporateClient = value => {
    if (value !== null) {
      this.setState({ selectedClientCorporate: value.value });
      console.log(this.state.selectedClientCorporate)
    }
    if (value === null) {
      this.setState({ selectedClientCorporate: "" });
      console.log(this.state.selectedClientCorporate)
    }
  };
  onSelectChangeIndividualClient = value => {
    if (value !== null) {
      this.setState({ selectedClientIndividual: value.value });
      console.log(this.state.selectedClientIndividual)
    }
    if (value === null) {
      this.setState({ selectedClientIndividual: "" });
      console.log(this.state.selectedClientIndividual)
    }
  };

  onSelectChangeDiscussion = value => {
    if (value !== null) {
      this.setState({ selectedDiscussionType: value.value });
      console.log(this.state.selectedDiscussionType)
    }
    if (value === null) {
      this.setState({ selectedDiscussionType: "" });
      console.log(this.state.selectedDiscussionType)
    }
  };
  render() {
    let {

      date,
      driverName,
      carNumber,
      serviceDetails
    } = this.state;
    return (
      <div>
        <ValidatorForm
          ref="form"
          onSubmit={this.handleSubmit}
          onError={errors => null}
        >

          <Grid container spacing={6}>
            <Grid item lg={12} md={6} sm={12} xs={12} style={{ maxWidth: '100%' }}>
              <div style={{ marginBottom: '15px', marginTop: '10px' }}><strong> <font color='black'>Select Client Type</font></strong></div>
              <Select
                style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                value={this.state.client_type_array.filter(option => option.value === this.state.selectedClientType)}
                isClearable="true"
                name="selectedClientType"
                options={this.state.client_type_array}
                className="basic-multi-select"
                classNamePrefix="select"
                onChange={this.onSelectChangeDays}
              />

            </Grid>
            <Grid item lg={6} md={6} sm={12} xs={12} style={{ maxWidth: '100%' }}>
              <div style={{ marginBottom: '15px', marginTop: '10px' }}><strong>
                <font color='black'>Select Client</font>
              </strong></div>
              {this.state.selectedClientType === '' ?
                <Select
                  style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                  isClearable="true"
                  name="selectedClientIndividual"
                  className="basic-multi-select"
                  classNamePrefix="select"
                />
                :
                this.state.selectedClientType === 0 && this.state.selectedClientType !== '' ?
                  <Select
                    style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                    value={this.state.clients_corporate.filter(option => option.value === this.state.selectedClientCorporate)}
                    isClearable="true"
                    name="selectedClientIndividual"
                    options={this.state.clients_corporate}
                    className="basic-multi-select"
                    classNamePrefix="select"
                    onChange={this.onSelectChangeCorporateClient}
                  />

                  :
                  // }
                  // {this.state.selectedClientType === 0 &&
                  <Select
                    style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                    value={this.state.clients.filter(option => option.value === this.state.selectedClientIndividual)}

                    isClearable="true"
                    name="selectedClientIndividual"
                    options={this.state.clients}
                    className="basic-multi-select"
                    classNamePrefix="select"
                    onChange={this.onSelectChangeIndividualClient}
                  />
              }
              {/* } */}
            </Grid>

            <Grid item lg={6} md={6} sm={12} xs={12}>
              <div style={{ marginBottom: '15px', marginTop: '10px' }}><strong>
                <font color='black'>Select Discussion Type</font>
              </strong></div>
              <Select
                style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                value={this.state.discussionTypes.filter(option => option.value === this.state.selectedDiscussionType)}

                isClearable="true"
                name="selectedClientCorporate"
                options={this.state.discussionTypes}
                className="basic-multi-select"
                classNamePrefix="select"
                onChange={this.onSelectChangeDiscussion}
              />
            </Grid>
          </Grid>
          <Grid container spacing={6}>
            <Grid item lg={6} md={6} sm={12} xs={12}>
              <TextValidator
                autoComplete='off'
                className="mb-4 w-full"
                label="Driver Name (Max length : 25)"
                onChange={this.handleChange}
                type="text"
                name="driverName"
                value={driverName}
                validators={[
                  // "required",

                  // "maxStringLength: 25"
                ]}
                errorMessages={["this field is required"]}
              />
              <TextValidator
                autoComplete='off'
                className="mb-4 w-full"
                label="Car Number"
                onChange={this.handleChange}
                type="text"
                name="carNumber"
                value={carNumber}
                // validators={["required"]}
                errorMessages={["this field is required"]}
              />




            </Grid>

            <Grid item lg={6} md={6} sm={12} xs={12}>
              <TextValidator
                autoComplete='off'
                className="mb-4 w-full"
                label="Car Service"
                onChange={this.handleChange}
                type="text"
                name="serviceDetails"
                value={serviceDetails}
                // validators={["required"]}
                errorMessages={["this field is required"]}
              />

              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDatePicker
                  className="mb-4 w-full"
                  margin="none"
                  id="mui-pickers-date"
                  label="Appointment Date"
                  inputVariant="standard"
                  type="text"
                  autoOk={true}
                  value={date}
                  onChange={this.handleDateChange}
                  KeyboardButtonProps={{
                    "aria-label": "Appointment Date"
                  }}
                />
              </MuiPickersUtilsProvider>
            </Grid>
          </Grid>
          <Button style={{ backgroundColor: '#007bff' }} color="primary" variant="contained" type="submit">
            <Icon>send</Icon>
            <span className="pl-2 capitalize">Submit</span>
          </Button>
        </ValidatorForm>
      </div>
    );
  }
}

export default CreateForm;
