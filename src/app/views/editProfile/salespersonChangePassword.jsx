/* eslint-disable no-unused-vars */
import React, { Component } from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import localStorageService from "./../../services/localStorageService"
import axios from 'axios'
import myApi from '../../auth/api'
import Swal from 'sweetalert2'
import { updateUserProfile } from 'app/redux/actions/UserActions'
import { connect } from "react-redux";
import PropTypes from "prop-types";

import {
  Button,
  Icon,
  Grid,
  Radio,
  RadioGroup,
  FormControlLabel,
  Checkbox
} from "@material-ui/core";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from "@material-ui/pickers";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import themeOptions from "app/MatxLayout/MatxTheme/themeOptions";

class SalespersonPasswordChange extends Component {

  constructor(props) {
    super(props);
    let { user } = this.props

    this.state = {

      oldPass: "",
      name: user.displayName,
      email: user.email ,
      date: new Date(),
      creditCard: "",
      phone: "",
      password: "",
      confirmPassword: "",
      gender: "",
      agreement: ""
    };
  }

  componentDidMount() {
    // custom rule will have name 'isPasswordMatch'
    ValidatorForm.addValidationRule("isPasswordMatch", value => {
      if (value !== this.state.password) {
        return false;
      }
      return true;
    });
    axios.get(myApi + '/intraco/lpg/salesPerson/profile/view/'+parseInt(localStorageService.getItem('auth_user').userId), {
      headers: {
        'x-access-token': localStorageService.getItem('auth_user').token
      }
    }).then(res => {
      this.setState({ name : res.data.data.name, email: res.data.email })
    })

    let { user } = this.props
  }

  componentWillUnmount() {
    // remove rule when it is not needed
    ValidatorForm.removeValidationRule("isPasswordMatch");
  }

  handleSubmit = event => {
    let baseURL;
    let { user } = this.props
    axios.post(myApi + '/intraco/lpg/salesPerson/profile/update',
      {
        id: this.props.user.userId,
        name: this.state.name,
        email: this.state.email ? this.state.email : this.props.user.email,
        phone_number: this.props.user.phone_number
      },
      {
        headers: {
          'x-access-token': localStorage.getItem("jwt_token")
        },
      }
    )
      .then(res => {
        if (res.data.error) {
          Swal.fire({
            icon: "error",
            title: res.data.message,
            showConfirmButton: false,
            timer: 1000
          });
          throw new Error(res.data.message)
        }
        else {
          Swal.fire({
            icon: "success",
            title: res.data.message,
            showConfirmButton: false,
            timer: 1000
          });
        }

        this.props.updateUserProfile(this.state.name,this.state.email)
        this.setState({
          name: this.state.name,
          email: this.state.email
        });
      })
      .catch((e) => {
        //console.log(e)
      })


  }

  handleChange = event => {
    event.persist();
    this.setState({ [event.target.name]: event.target.value });
  };

  handleDateChange = date => {
    // //console.log(date);

    this.setState({ date });
  };

  render() {
    let { user } = this.props
    let {
      oldPass,
      name,
      creditCard,
      phone,
      password,
      confirmPassword,
      gender,
      date,
      email
    } = this.state;
    return (
      <div>
        <ValidatorForm
          ref="form"
          onSubmit={this.handleSubmit}
          onError={errors => null}
        >
          <Grid container spacing={6}>
            <Grid item lg={6} md={6} sm={12} xs={12}>

              <TextValidator
                autoComplete='off'
                className="mb-4 w-full"
                // label='Full Name'
                onChange={this.handleChange}
                type="text"
                placeholder='Enter Name'
                // placeholder={this.state.name|| 'Enter your Full Name'}
                name="name"
                value={name}
                validators={["required"]}
                errorMessages={["this field is required"]}
              />
              <TextValidator
                autoComplete='off'
                className="mb-4 w-full"
                placeholder='Enter Email'
                onChange={this.handleChange}
                type="email"
                name="email"
                value={email}

              />


            </Grid>
          </Grid>
          <Button color="primary" variant="contained" type="submit">
            <Icon>send</Icon>
            <span className="pl-2 capitalize">Update</span>
          </Button>
        </ValidatorForm>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  updateUserProfile: PropTypes.func.isRequired,
  user: state.user
});

export default connect(mapStateToProps, { updateUserProfile })(SalespersonPasswordChange);

