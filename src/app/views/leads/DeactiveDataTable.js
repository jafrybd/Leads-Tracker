import React from "react";
import { Button } from "reactstrap";
import ModalForm from "./Modal";
import axios from "axios";
import myApi from "../../auth/api";
import localStorageService from "app/services/localStorageService";
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TablePagination,
} from "@material-ui/core";
function DataTable(props) {
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [page, setPage] = React.useState(0);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
  };
  const deleteItem = (id) => {
    let confirmDelete = window.confirm("Do you want this to Active ?");
    if (confirmDelete) {
      axios
        .post(
          myApi + "/intraco/lpg/lead/active",
          { id: id },
          {
            headers: {
              "x-access-token": localStorageService.getItem("auth_user").token,
            },
          }
        )
        .then((response) => {
          // response.json()
          //console.log(response);
        })
        .then((item) => {
          props.deleteItemFromState(id);
        })
        .catch((err) => console.log(err));
    }
  };

  // const items = props.items && props.items.map((item, i) => {
  //   return (
  //     <tr key={item.id}>
  //       <th scope="row">{i + 1}</th>
  //       <td>{item.name}</td>
  //       {/* <td>{item.phone_number}</td>
  //         <td>{item.email}</td> */}

  //       <td>
  //         <div style={{ width: "150px" }}>
  //           <ModalForm
  //             buttonLabel="Edit"
  //             item={item}
  //             updateState={props.updateState}
  //           />{" "}
  //           <Button
  //             color="green"
  //             className="btn btn-primary"
  //             onClick={() => deleteItem(item.id)}
  //           >
  //             Active
  //           </Button>
  //         </div>
  //       </td>
  //     </tr>
  //   );
  // });

  return (
    <div className="w-full overflow-auto">
      <Table className="whitespace-pre">
        <TableHead>
          <TableRow>
            <TableCell className="px-0" style={{ width: "100px" }}>
              SL.
            </TableCell>
            <TableCell className="px-0" style={{ width: "70%" }}>
              Type
            </TableCell>
            <TableCell className="px-0">Edit</TableCell>
            <TableCell className="px-0">Active</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.items && props.items
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((item, id) => (
              <TableRow key={id}>
                <TableCell
                  className="px-0 capitalize"
                  align="left"
                  style={{ width: "100px" }}
                >
                 {page * rowsPerPage + id + 1}
                </TableCell>
                <TableCell
                  className="px-0 capitalize"
                  align="left"
                  style={{ width: "70%" }}
                >
                  {item.name}
                </TableCell>
                <TableCell className="px-0 capitalize" align="left">
                  <ModalForm
                    buttonLabel="Edit"
                    item={item}
                    updateState={props.updateState}
                  />
                </TableCell>
                <TableCell
                  className="px-0 capitalize"
                  align="left"
                  style={{ width: "250px" }}
                >
                  <Button
              color="green"
              className="btn btn-primary"
              onClick={() => deleteItem(item.id)}
            >
              Active
            </Button>
                </TableCell>
              </TableRow>
            ))}
        </TableBody>
      </Table>

      <TablePagination
        style={{
          maxWidth: 400,
          overflowX: "hidden",
          padding: "0px!important",
          display: "contents",
        }}
        className="px-4"
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={props.items.length}
        rowsPerPage={rowsPerPage}
        page={page}
        backIconButtonProps={{
          "aria-label": "Previous Page",
        }}
        nextIconButtonProps={{
          "aria-label": "Next Page",
        }}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </div>
  );
}
export default DataTable;
