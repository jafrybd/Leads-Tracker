import React, { useState, useEffect } from 'react'
import { Container, Row, Col } from 'reactstrap'
import ModalForm from './Modal'
import DataTable from './DeactiveDataTable'
import { CSVLink } from "react-csv"
import axios from 'axios'
import { Breadcrumb } from "matx";
import myApi from '../../auth/api'
import localStorageService from "../../services/localStorageService";
// import PropTypes from "prop-types";
// import { setUserData } from "../../redux/actions/UserActions";
// import jwtAuthService from "../services/jwtAuthService";
// import history from "history.js";



function DeactiveLeads(props) {
  //console.log(localStorage.getItem("jwt_token"))
  // const checkJwtAuth = async setUserData => {
  //   // You need to send token to your server to check token is valid
  //   // modify loginWithToken method in jwtService
  //   let user = await jwtAuthService.loginWithToken();
  //   if (user) setUserData(user);
  //   else
  //     history.push({
  //       pathname: "/signin"
  //     });
  //   return user;
  // };
  // //console.log(user)
  // //console.log(checkJwtAuth(setUserData))
  const [items, setItems] = useState([])
  const user = JSON.parse(localStorageService.getItem(props.user));
  //console.log(user)
  const getItems = () => {
    axios({
      "method": "GET",
      "url": myApi + "/intraco/lpg/lead/deactiveList",
      "headers": {

        "x-access-token": localStorageService.getItem("auth_user").token
      }
    })
      .then((response) => {
        // //console.log(token)
        //console.log(response.data.data)
        setItems(Array.isArray(response.data.data)?response.data.data:[])
      })
      .catch((error) => {
        //console.log(error)
      })
    // .then(response => response.json())
    // .then(items => setItems(items))
    // .catch(err => console.log(err))
  }
  //console.log(items)
  const addItemToState = (item) => {
    setItems([...items, item])
  }

  const updateState = (item) => {
    const itemIndex = items.findIndex(data => data.id === item.id)
    const newArray = [...items.slice(0, itemIndex), item, ...items.slice(itemIndex + 1)]
    setItems(newArray)
  }
  // const updateStateChange =(id,name) =>{
  //   const updatedItems = items.updatedItems(item => item.id !== id, item.name !== name)
  //   setItems(updatedItems)
  // }

  const deleteItemFromState = (id) => {
    const updatedItems = items.filter(item => item.id !== id)
    setItems(updatedItems)
  }

  useEffect(() => {
    getItems()
  }, []);

  return (
    <div className="m-sm-30">
      <div className="mb-sm-30">
        <Container className="App">

          <Row>
            <Col>
              <Breadcrumb
                routeSegments={[
                  { name: "Leads", path: "/lead/deactive" },
                  { name: "Deactive Leads" }
                ]}
              />
              <h1 style={{ margin: "20px 0" }}></h1>
            </Col>
          </Row>
          <Row>
            <Col>
              {/* <CSVLink
                filename={"db.csv"}
                color="primary"
                style={{ float: "left", marginRight: "10px" }}
                className="btn btn-primary"
                data={items}>
                Download CSV
            </CSVLink> */}
              {/* <ModalForm buttonLabel="Add Item" addItemToState={addItemToState} /> */}
            </Col>
          </Row>
          <br></br>
          <Row>
            <Col>
              <DataTable items={items} updateState={updateState} deleteItemFromState={deleteItemFromState} />
            </Col>
          </Row>
         
        </Container>
      </div>
    </div>
  )
}

export default DeactiveLeads