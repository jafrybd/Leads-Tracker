import React, { Component } from "react";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import axios from 'axios'
import Swal from 'sweetalert2'
import {
  Button,
  Icon,
  Grid,

} from "@material-ui/core";

import "date-fns";
import myApi from "app/auth/api";
import Select from 'react-select';

class CreateForm extends Component {


  constructor() {
    super();
    this.state = {
      name: "",
      selectedServiceYear: "",
      selectedServiceDays: '',
      selectedServiceMonth: '',
      selectedReminderYear: '',
      selectedReminderMonth: '',
      selectedReminderDays: '',
      years: [
        { value: 0, label: "0" },
        { value: 1, label: "1" },
        { value: 2, label: "2" },
        { value: 3, label: "3" },
        { value: 4, label: "4" },
        { value: 5, label: "5" },
        { value: 6, label: "6" },
        { value: 7, label: "7" },
      ],
      months: [
        { value: 0, label: "0" },
        { value: 1, label: "1" },
        { value: 2, label: "2" },
        { value: 3, label: "3" },
        { value: 4, label: "4" },
        { value: 5, label: "5" },
        { value: 6, label: "6" },
        { value: 7, label: "7" },
        { value: 8, label: "8" },
        { value: 9, label: "9" },
        { value: 10, label: "10" },
        { value: 11, label: "11" },
      ],
      days: [
        { value: 0, label: "0" },
        { value: 1, label: "1" },
        { value: 2, label: "2" },
        { value: 3, label: "3" },
        { value: 4, label: "4" },
        { value: 5, label: "5" },
        { value: 6, label: "6" },
        { value: 7, label: "7" },
        { value: 8, label: "8" },
        { value: 9, label: "9" },
        { value: 10, label: "10" },
        { value: 11, label: "11" },
        { value: 12, label: "12" },
        { value: 13, label: "13" },
        { value: 14, label: "14" },
        { value: 15, label: "15" },
        { value: 16, label: "16" },
        { value: 17, label: "17" },
        { value: 18, label: "18" },
        { value: 19, label: "19" },
        { value: 20, label: "20" },
        { value: 21, label: "21" },
        { value: 22, label: "22" },
        { value: 23, label: "23" },
        { value: 24, label: "24" },
        { value: 25, label: "25" },
        { value: 26, label: "26" },
        { value: 27, label: "27" },
        { value: 28, label: "28" },
        { value: 29, label: "29" },
        { value: 30, label: "30" },
      ]
    };
  }


  componentDidMount() {
    // custom rule will have name 'isPasswordMatch'
    ValidatorForm.addValidationRule("isPasswordMatch", value => {
      if (value !== this.state.password) {
        return false;
      }
      return true;
    });

  }

  componentWillUnmount() {
    // remove rule when it is not needed
    ValidatorForm.removeValidationRule("isPasswordMatch");
  }

  handleSubmit = event => {

    axios.post(myApi + '/intraco/lpg/discussion_type/add', {
      "type": this.state.name,
      "service_year": this.state.selectedServiceYear,
      "service_month": this.state.selectedServiceMonth,
      "service_day": this.state.selectedServiceDays,
      "reminder_year": this.state.selectedReminderYear,
      "reminder_month": this.state.selectedReminderMonth,
      "reminder_day": this.state.selectedReminderDays
    }, {

      "headers": {
        "x-access-token": localStorage.getItem("jwt_token")
      }
    })

      .then(res => {
        //console.log(res.data.error)

        if (res.data.message) {
          if (!res.data.error) {
            Swal.fire({
              icon: "success",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          } else {
            Swal.fire({
              icon: "error",
              title: res.data.message,
              showConfirmButton: false,
              timer: 1000
            });
          }

          if (res.data.error === false) {
            this.setState({
              name: '',
              selectedServiceYear: "",
              selectedServiceDays: '',
              selectedServiceMonth: '',
              selectedReminderYear: '',
              selectedReminderMonth: '',
              selectedReminderDays: '',
            })
          }
        }
      })
  }

  handleChange = event => {
    event.persist();
    this.setState({ [event.target.name]: event.target.value });
  };

  handleDateChange = date => {

    this.setState({ date });
  };
  onSelectChangeYear = value => {
    // this.toggleOpen();
    if (value !== null) {
      this.setState({ selectedServiceYear: value.value });
      console.log(this.state.selectedServiceYear)
    }
    if (value === null) {
      this.setState({ selectedServiceYear: "" });
      console.log(this.state.selectedServiceYear)
    }
  };

  onSelectChangeMonth = value => {
    // this.toggleOpen();
    if (value !== null) {
      this.setState({ selectedServiceMonth: value.value });
      console.log(this.state.selectedServiceMonth)
    }
    if (value === null) {
      this.setState({ selectedServiceMonth: "" });
      console.log(this.state.selectedServiceMonth)
    }
  };

  onSelectChangeDays = value => {
    // this.toggleOpen();
    if (value !== null) {
      this.setState({ selectedServiceDays: value.value });
      console.log(this.state.selectedServiceDays)
    }
    if (value === null) {
      this.setState({ selectedServiceDays: "" });
      console.log(this.state.selectedServiceDays)
    }
  };

  onSelectChangeReminderYears = value => {
    // this.toggleOpen();
    if (value !== null) {
      this.setState({ selectedReminderYear: value.value });
      console.log(this.state.selectedReminderYear)
    }
    if (value === null) {
      this.setState({ selectedReminderYear: "" });
      console.log(this.state.selectedReminderYear)
    }
  };

  onSelectChangeReminderMonths = value => {
    // this.toggleOpen();
    if (value !== null) {
      this.setState({ selectedReminderMonth: value.value });
      console.log(this.state.selectedReminderMonth)
    }
    if (value === null) {
      this.setState({ selectedReminderMonth: "" });
      console.log(this.state.selectedReminderMonth)
    }
  };

  onSelectChangeReminderDays = value => {
    // this.toggleOpen();
    if (value !== null) {
      this.setState({ selectedReminderDays: value.value });
      console.log(this.state.selectedReminderDays)
    }
    if (value === null) {
      this.setState({ selectedReminderDays: "" });
      console.log(this.state.selectedReminderDays)
    }
  };

  render() {
    let {
      name,
    } = this.state;
    return (
      <div>
        <ValidatorForm
          ref="form"
          onSubmit={this.handleSubmit}
          onError={errors => null}
        >
          {/* <SimpleCard title="Discussion Type Name" style={{marginTop : '10px', marginBottom : '10px', height: "100px"}}> */}
          <Grid container spacing={6}>
            <Grid item lg={12} md={6} sm={12} xs={12}>
              <TextValidator
                autoComplete='off'
                className="mb-4 w-full"
                label="Discussion Type (Max length : 25)"
                onChange={this.handleChange}
                type="text"
                name="name"
                value={name}
                validators={[
                  "required",
                  "maxStringLength: 25"
                ]}
                errorMessages={["this field is required"]}
              />

            </Grid>
          </Grid>
          <div style={{ fontWeight: '500', fontSize: '15px', marginTop: '40px', marginBottom: '15px' }}>
            <font color="black">
              Service Period
            </font>
          </div>
          <Grid container spacing={6}>
            <Grid item lg={6} md={6} sm={12} xs={12} style={{ maxWidth: '25%' }}>
              <font color='grey'>Day</font>
              <Select
                style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                value={this.state.days.filter(option => option.value === this.state.selectedServiceDays)}
                isClearable="true"
                name="selectedServiceDays"
                options={this.state.days}
                className="basic-multi-select"
                classNamePrefix="select"
                onChange={this.onSelectChangeDays}
              />

            </Grid>
            <Grid item lg={6} md={6} sm={12} xs={12} style={{ maxWidth: '35%' }}>
              <font color='grey'>Month</font>
              <Select
                style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                value={this.state.months.filter(option => option.value === this.state.selectedServiceMonth)}
                isClearable="true"
                name="selectedServiceMonth"
                options={this.state.months}
                className="basic-multi-select"
                classNamePrefix="select"
                onChange={this.onSelectChangeMonth}
              />

            </Grid>
            <Grid item lg={6} md={6} sm={12} xs={12} style={{ maxWidth: '40%' }}>
              <font color='grey'>Year</font>
              <Select
                style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                value={this.state.years.filter(option => option.value === this.state.selectedServiceYear)}
                isClearable="true"
                name="selectedServiceYear"
                options={this.state.years}
                className="basic-multi-select"
                classNamePrefix="select"
                onChange={this.onSelectChangeYear}
              />

            </Grid>
          </Grid>

          <div style={{ fontWeight: '500', fontSize: '15px', marginTop: '40px', marginBottom: '15px' }}>
            <font color="black">
              Reminder Period
            </font>
          </div>

          <Grid container spacing={6}>

            <Grid item lg={6} md={6} sm={12} xs={12} style={{ maxWidth: '25%' }}>

              <font color='grey'>Day</font>
              <Select
                style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                value={this.state.days.filter(option => option.value === this.state.selectedReminderDays)}
                isClearable="true"
                name="selectedReminderDays"
                options={this.state.days}
                className="basic-multi-select"
                classNamePrefix="select"
                onChange={this.onSelectChangeReminderDays}
              />

            </Grid>
            <Grid item lg={6} md={6} sm={12} xs={12} style={{ maxWidth: '35%' }}>
              <font color='grey'>Month</font>
              <Select
                style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                value={this.state.months.filter(option => option.value === this.state.selectedReminderMonth)}
                isClearable="true"
                name="selectedReminderMonth"
                options={this.state.months}
                className="basic-multi-select"
                classNamePrefix="select"
                onChange={this.onSelectChangeReminderMonths}
              />

            </Grid>
            <Grid item lg={6} md={6} sm={12} xs={12} style={{ maxWidth: '40%' }}>
              <font color='grey'>Year</font>
              <Select
                style={{ width: '100%', backgroundColor: 'white', color: '#444', borderBottomColor: '#000000', textAlign: 'left', marginTop: '40px' }}
                value={this.state.years.filter(option => option.value === this.state.selectedReminderYear)}
                isClearable="true"
                name="selectedReminderYear"
                options={this.state.years}
                className="basic-multi-select"
                classNamePrefix="select"
                onChange={this.onSelectChangeReminderYears}
              />

            </Grid>
          </Grid>
          <Button color="primary" variant="contained" type="submit" style={{ marginTop: '30px' }}>
            <Icon>send</Icon>
            <span className="pl-2 capitalize">Submit</span>
          </Button>
        </ValidatorForm>
      </div>
    );
  }
}

export default CreateForm;
