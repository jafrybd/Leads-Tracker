export const MDNavigation = [
  {
    name: "Dashboard",
    path: "/dashboard/md",
    icon: "dashboard",
  },
  {
    name: "Appointment Report",
    path: "/report/appointment",
    icon: "dashboard",
  },
  {
    name: "Task Report",
    path: "/report/task",
    icon: "dashboard",
  },
];
