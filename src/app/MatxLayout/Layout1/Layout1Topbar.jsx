/* eslint-disable no-unused-vars */
import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { Icon, IconButton, MenuItem } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { setLayoutSettings } from "app/redux/actions/LayoutActions";
import { logoutUser } from "app/redux/actions/UserActions";
import PropTypes from "prop-types";
import { MatxMenu, MatxSearchBox } from "matx";
import { isMdScreen, classList } from "utils";
import NotificationBar from "../SharedCompoents/NotificationBar";
import { Link } from "react-router-dom";
import ShoppingCart from "../SharedCompoents/ShoppingCart";
import history from '../../../history'
import Swal from "sweetalert2";
import localStorageService from "app/services/localStorageService";
// import a from '../iltManual.pdf'
const styles = theme => ({
  topbar: {
    "& .topbar-hold": {
      backgroundColor: "#0956A4",
      // theme.palette.primary.main,
      height: "80px",
      "&.fixed": {
        boxShadow: theme.shadows[8],
        height: "64px"
      }
    }
  },
  menuItem: {
    display: "flex",
    alignItems: "center",
    minWidth: 185
  }
});

class Layout1Topbar extends Component {
  state = {};

  updateSidebarMode = sidebarSettings => {
    let { settings, setLayoutSettings } = this.props;

    setLayoutSettings({
      ...settings,
      layout1Settings: {
        ...settings.layout1Settings,
        leftSidebar: {
          ...settings.layout1Settings.leftSidebar,
          ...sidebarSettings
        }
      }
    });
  };

  handleSidebarToggle = () => {
    let { settings } = this.props;
    let { layout1Settings } = settings;

    let mode;
    if (isMdScreen()) {
      mode = layout1Settings.leftSidebar.mode === "close" ? "mobile" : "close";
    } else {
      mode = layout1Settings.leftSidebar.mode === "full" ? "close" : "full";
    }
    this.updateSidebarMode({ mode });
  };

  handleSignOut = () => {
    Swal.fire({
      title: "Are you sure?",
      // text: "You will be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, proceed!",
      allowOutsideClick: false,
    }).then((result) => {
      if (result.isConfirmed) {
        this.props.logoutUser();
      }
    });
  };
  handleHelpLine = () => {
    Swal.fire({
      icon: "info",
      title: 'Available Soon',
      showConfirmButton: false,
    });
  };

  handleAdminProfile = () => {
    history.push({
      pathname: "/profile/admin"
    });
  };
  handleAdminUpdateProfile = () => {
    history.push({
      pathname: "/profile/admin/edit"
    });
  };
  handleAdminChangePass = () => {
    history.push({
      pathname: "/password/edit"
    });
  };


  handleSalesProfile = () => {
    history.push({
      pathname: "/account/profile"
    });
  };
  handleSalesUpdateProfile = () => {
    history.push({
      pathname: "/profile/salesperson/edit"
    });
  };
  handleSalesForgetPass = () => {
    history.push({
      pathname: "/setting/forgetpassword"
    });
  };
  handleSalesPassChange = () => {
    history.push({
      pathname: "/salesperson/passwordChange"
    });
  };

  handleSubAdminProfile = () => {
    history.push({
      pathname: "/profile/subadmin"
    });
  };
  handleSubAdminUpdateProfile = () => {
    history.push({
      pathname: "/profile/subadmin/edit"
    });
  };
  handleSubAdminChangePass = () => {
    history.push({
      pathname: "/subAdmin/passwordChange"
    });
  };

  render() {
    let { classes, fixed, user } = this.props;

    return (
      <div className={`topbar ${classes.topbar}`}>
        <div className={classList({ "topbar-hold": true, fixed: fixed })}>
          <div className="flex justify-between items-center h-full">
            <div className="flex">
              <IconButton
                onClick={this.handleSidebarToggle}
                className="hide-on-pc"
              >
                <Icon>menu</Icon>
              </IconButton>

              <div className="hide-on-mobile">
                {/* <IconButton>
                  <Icon>mail_outline</Icon>
                </IconButton>

                <IconButton>
                  <Icon>web_asset</Icon>
                </IconButton>

                <IconButton>
                  <Icon>star_outline</Icon>
                </IconButton> */}
              </div>
            </div>
            <div className="flex items-center">
              {/* <MatxSearchBox />

              <NotificationBar />

              <ShoppingCart></ShoppingCart> */}

              {/* Admin profile icon */}
              {localStorageService.getItem('auth_user') && localStorageService.getItem('auth_user').role === 'ADMIN' &&
                <Link
                  onClick={this.handleAdminProfile}
                  to="/profile/admin">
                  <img
                    className="mx-2 align-middle circular-image-small cursor-pointer"
                    src={user.photoURL}
                    alt="user"
                  />
                </Link>
              }

              {/* Sub Admin profile icon */}
              {localStorageService.getItem('auth_user') && localStorageService.getItem('auth_user').role === 'SUB-ADMIN' &&
                <Link
                  onClick={this.handleSubAdminProfile}
                  to="/profile/subadmin">
                  <img
                    className="mx-2 align-middle circular-image-small cursor-pointer"
                    src={user.photoURL}
                    alt="user"
                  />
                </Link>
              }

              {/* salesperson profile icon */}
              {localStorageService.getItem('auth_user') && localStorageService.getItem('auth_user').role === 'SALES-PERSON' &&
                <Link
                  onClick={this.handleSalesProfile}
                  to="/account/profile">
                  <img
                    className="mx-2 align-middle circular-image-small cursor-pointer"
                    src={user.photoURL}
                    alt="user"
                  />
                </Link>
              }

              <MatxMenu
                menuButton={
                  <span style={{ color: 'white' }}
                    className="material-icons MuiIcon-root MuiIcon-fontSizeLarge"
                    aria-hidden="true"
                    title="more_vert">more_vert</span>

                }
              >
                {localStorageService.getItem('auth_user') && localStorageService.getItem('auth_user').role === 'ADMIN' &&
                  <>
                    <MenuItem
                      onClick={this.handleAdminProfile}
                      className={classes.menuItem} to="/profile/admin">
                      <Icon> person </Icon>
                      <span className="pl-4"> View Profile </span>
                    </MenuItem>
                    <MenuItem
                      onClick={this.handleAdminUpdateProfile}
                      className={classes.menuItem}
                      to="/profile/admin/edit">
                      <Icon> settings </Icon>
                      <span className="pl-4"> Update Profile </span>
                    </MenuItem>
                    <MenuItem
                      onClick={this.handleAdminChangePass}
                      className={classes.menuItem}
                      to="/password/edit">
                      <Icon> fingerprint </Icon>
                      <span className="pl-4"> Change Password </span>
                    </MenuItem>
                  </>
                }
                {localStorageService.getItem('auth_user') && localStorageService.getItem('auth_user').role === 'SUB-ADMIN' &&
                  <>
                    <MenuItem
                      onClick={this.handleSubAdminProfile}
                      className={classes.menuItem} to="/profile/subadmin">
                      <Icon> person </Icon>
                      <span className="pl-4"> View Profile </span>
                    </MenuItem>
                    <MenuItem
                      onClick={this.handleSubAdminUpdateProfile}
                      className={classes.menuItem}
                      to="/profile/subadmin/edit">
                      <Icon> settings </Icon>
                      <span className="pl-4"> Update Profile </span>
                    </MenuItem>
                    <MenuItem
                      onClick={this.handleSubAdminChangePass}
                      className={classes.menuItem}
                      to="/subAdmin/passwordChange">
                      <Icon> fingerprint </Icon>
                      <span className="pl-4"> Change Password </span>
                    </MenuItem>
                  </>
                }
                {localStorageService.getItem('auth_user') && localStorageService.getItem('auth_user').role === 'SALES-PERSON' &&
                  <>
                    <MenuItem
                      onClick={this.handleSalesProfile}
                      className={classes.menuItem} to="/account/profile">
                      <Icon> person </Icon>
                      <span className="pl-4"> View Profile </span>
                    </MenuItem>
                    <MenuItem
                      onClick={this.handleSalesUpdateProfile}
                      className={classes.menuItem}
                      to="/profile/salesperson/edit">
                      <Icon> settings </Icon>
                      <span className="pl-4"> Update Profile </span>
                    </MenuItem>
                    <MenuItem
                      onClick={this.handleSalesPassChange}
                      className={classes.menuItem}
                      to="/salesperson/passwordChange">
                      <Icon> fingerprint </Icon>
                      <span className="pl-4"> Change Password </span>
                    </MenuItem>
                    <MenuItem
                      onClick={this.handleSalesForgetPass}
                      className={classes.menuItem}
                      to="/setting/forgetpassword">
                      <Icon> settings </Icon>
                      <span className="pl-4"> Forget Password Setting</span>
                    </MenuItem>
                  </>
                }
                <MenuItem
                  // onClick={this.handleHelpLine}
                  className={classes.menuItem}
                >
                  <Icon> help </Icon>
                  <span className="pl-4"> <a href='../iltManual.pdf' target='_blank'>Support/Helpline</a> </span>
                </MenuItem>
                <MenuItem
                  onClick={this.handleSignOut}
                  className={classes.menuItem}
                >
                  <Icon> power_settings_new </Icon>
                  <span className="pl-4"> Logout </span>
                </MenuItem>
              </MatxMenu>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Layout1Topbar.propTypes = {
  setLayoutSettings: PropTypes.func.isRequired,
  logoutUser: PropTypes.func.isRequired,
  settings: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  user: state.user,
  setLayoutSettings: PropTypes.func.isRequired,
  logoutUser: PropTypes.func.isRequired,
  settings: state.layout.settings
});

export default withStyles(styles, { withTheme: true })(
  withRouter(
    connect(mapStateToProps, { setLayoutSettings, logoutUser })(Layout1Topbar)
  )
);
